#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  # fix permissions in case files have been uploaded using sftp
  chmod u+x $ARTIFACT_ROOT/use_defaults_nvidia_a100.sh
  chmod u+x $ARTIFACT_ROOT/use_defaults_nvidia_v100.sh
  chmod u+x $ARTIFACT_ROOT/use_defaults_intel_gold_6140.sh
  chmod u+x $ARTIFACT_ROOT/use_defaults_intel_e5_2683.sh
  chmod u+x $ARTIFACT_ROOT/tune.sh
  chmod u+x $ARTIFACT_ROOT/bench.sh
  chmod u+x $ARTIFACT_ROOT/clean.sh

  if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
      # MDH
      if [[ -n "$ENABLE_MDH" && "$ENABLE_MDH" -eq 1 ]]; then
          (
            mkdir -p $ARTIFACT_ROOT/mdh/gpu/build &> /dev/null
            cd $ARTIFACT_ROOT/mdh/gpu/build &&
            cmake -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/mdh/gpu/cmake/Modules ${@:1} .. &&
            make -j `nproc`
          ) || { printf "\n\nBuild failed!\n"; exit 1; }
      fi

      # TVM
      if [[ -n "$ENABLE_TVM" && "$ENABLE_TVM" -eq 1 ]]; then
          (
            mkdir -p $ARTIFACT_ROOT/tvm/cuda_profiler/build &> /dev/null
            cd $ARTIFACT_ROOT/tvm/cuda_profiler/build &&
            cmake -DCMAKE_MODULE_PATH=${NVHPC_DIR}/cmake ${@:1} .. &&
            make -j `nproc`
          ) || { printf "\n\nBuild failed!\n"; exit 1; }
      fi

      # cuBLAS
      if [[ -n "$ENABLE_CUBLAS" && "$ENABLE_CUBLAS" -eq 1 ]]; then
          (
            mkdir -p $ARTIFACT_ROOT/cublas/matmul/build &> /dev/null
            cd $ARTIFACT_ROOT/cublas/matmul/build &&
            export PATH=${NVHPC_DIR}/compilers/bin:$PATH &&
            cmake .. -DCMAKE_CXX_COMPILER=nvc++ -DCMAKE_C_COMPILER=nvc -DNVHPC_DIR=${NVHPC_DIR}/cmake -DCMAKE_BUILD_TYPE=Release &&
            make -j `nproc`
          ) || { printf "\n\nBuild failed!\n"; exit 1; }
      fi

      # cuDNN
      if [[ -n "$ENABLE_CUBLAS" && "$ENABLE_CUBLAS" -eq 1 ]]; then
          (
            mkdir -p $ARTIFACT_ROOT/cudnn/mcc/build &> /dev/null
            cd $ARTIFACT_ROOT/cudnn/mcc/build &&
            export PATH=${NVHPC_DIR}/compilers/bin:$PATH &&
            cmake .. -DCMAKE_CXX_COMPILER=nvc++ -DCMAKE_C_COMPILER=nvc -DNVHPC_DIR=${NVHPC_DIR}/cmake -DCMAKE_BUILD_TYPE=Release &&
            make -j `nproc`
          ) || { printf "\n\nBuild failed!\n"; exit 1; }
      fi
  fi

  if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
      # MDH
      if [[ -n "$ENABLE_MDH" && "$ENABLE_MDH" -eq 1 ]]; then
          (
            mkdir -p $ARTIFACT_ROOT/mdh/cpu/build &> /dev/null
            cd $ARTIFACT_ROOT/mdh/cpu/build &&
            cmake -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/mdh/cpu/cmake/Modules ${@:1} .. &&
            make -j `nproc`
          ) || { printf "\n\nBuild failed!\n"; exit 1; }
      fi

      # TVM
      if [[ -n "$ENABLE_TVM" && "$ENABLE_TVM" -eq 1 ]]; then
          (
            mkdir -p $ARTIFACT_ROOT/tvm/opencl_profiler/build &> /dev/null
            cd $ARTIFACT_ROOT/tvm/opencl_profiler/build &&
            cmake ${@:1} .. &&
            make -j `nproc`
          ) || { printf "\n\nBuild failed!\n"; exit 1; }
      fi

      # oneMKL
      if [[ -n "$ENABLE_ONEMKL" && "$ENABLE_ONEMKL" -eq 1 ]]; then
          (
            mkdir -p $ARTIFACT_ROOT/onemkl/matmul/build &> /dev/null
            cd $ARTIFACT_ROOT/onemkl/matmul/build &&
            source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
            source ${ONEAPI_DIR}/mkl/latest/env/vars.sh &&
            cmake .. -DCMAKE_CXX_COMPILER=icpx -DCMAKE_C_COMPILER=icx -DCMAKE_BUILD_TYPE=Release &&
            make -j `nproc`
          ) || { printf "\n\nBuild failed!\n"; exit 1; }
      fi

      # oneDNN
      if [[ -n "$ENABLE_ONEDNN" && "$ENABLE_ONEDNN" -eq 1 ]]; then
          (
            mkdir -p $ARTIFACT_ROOT/onednn/mcc/build &> /dev/null
            cd $ARTIFACT_ROOT/onednn/mcc/build &&
            source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
            source ${ONEAPI_DIR}/dnnl/latest/env/vars.sh --dnnl-configuration=cpu_iomp &&
            export CPATH=$DNNLROOT/include:$CPATH &&
            cmake .. -DCMAKE_CXX_COMPILER=icpx -DCMAKE_C_COMPILER=icx -DDNNL_CONFIGURATION=cpu_iomp -DCMAKE_BUILD_TYPE=Release &&
            make -j `nproc`
          ) || { printf "\n\nBuild failed!\n"; exit 1; }
      fi
  fi

  printf "\n\nBuild successful!\n"
}