std::vector<float> a;
std::vector<float> b;
std::vector<float> c;

void init_data() {
    a = std::vector<float>(m_arg.getValue() * k_arg.getValue()); for (int i = 0; i < a.size(); ++i) a[i] = data_sequence<float>(i);
    b = std::vector<float>(k_arg.getValue() * n_arg.getValue()); for (int i = 0; i < b.size(); ++i) b[i] = data_sequence<float>(i);
    c = std::vector<float>(m_arg.getValue() * n_arg.getValue()); for (int i = 0; i < c.size(); ++i) c[i] = data_sequence<float>(i);
}