void prepare_api_call() {

}

void exec_api_call() {
    cblas_sgemm(format_arg.getValue() == "row-major" ? CblasRowMajor : CblasColMajor,
                transpose_a_arg.getValue() ? CblasTrans : CblasNoTrans,
                transpose_b_arg.getValue() ? CblasTrans : CblasNoTrans,
                m_arg.getValue(), n_arg.getValue(), k_arg.getValue(),
                alpha_arg.getValue(),
                a.data(), ((format_arg.getValue() == "row-major" && transpose_a_arg.getValue()) || (format_arg.getValue() == "column-major" && !transpose_a_arg.getValue())) ? m_arg.getValue() : k_arg.getValue(),
                b.data(), ((format_arg.getValue() == "row-major" && transpose_b_arg.getValue()) || (format_arg.getValue() == "column-major" && !transpose_b_arg.getValue())) ? k_arg.getValue() : n_arg.getValue(),
                beta_arg.getValue(),
                c.data(), format_arg.getValue() == "row-major" ? n_arg.getValue() : m_arg.getValue());
}

bool result_correct(const std::string &gold_file) {
    std::ifstream is(gold_file);
    std::istream_iterator<float> start(is), end;
    std::vector<float> c_gold(start, end);

    return !memcmp(c.data(), c_gold.data(), c_gold.size() * sizeof(float));
}