TCLAP::ValueArg<int> m_arg("m", "m", "Input size M (number of rows of first input matrix and result matrix).", true, 0, "int");
TCLAP::ValueArg<int> n_arg("n", "n", "Input size N (number of columns of second input matrix and result matrix).", true, 0, "int");
TCLAP::ValueArg<int> k_arg("k", "k", "Input size K (number of columns of first input matrix and number of rows of second input matrix).", true, 0, "int");
TCLAP::ValueArg<float> alpha_arg("a", "alpha", "Alpha value.", false, 1.0f, "float");
TCLAP::ValueArg<float> beta_arg("b", "beta", "Beta value.", false, 0.0f, "float");
TCLAP::SwitchArg transpose_a_arg("", "transpose-a", "Transpose first input matrix.", false);
TCLAP::SwitchArg transpose_b_arg("", "transpose-b", "Transpose second input matrix.", false);
std::vector<std::string> formats({
                                         "row-major",
                                         "column-major"
                                 });
TCLAP::ValueArg<std::string> format_arg("f", "format", "The format (row- or column-major) of the matrices.", false, "row-major",
                                        new TCLAP::ValuesConstraint<std::string>(formats));

void add_arguments(TCLAP::CmdLine &cmd) {
    cmd.add(m_arg);
    cmd.add(n_arg);
    cmd.add(k_arg);
    cmd.add(alpha_arg);
    cmd.add(beta_arg);
    cmd.add(transpose_a_arg);
    cmd.add(transpose_b_arg);
    cmd.add(format_arg);
}

void benchmark_data_path(std::vector<std::string> &path) {
    path.push_back(format_arg.getValue());
    path.push_back(
            std::to_string(m_arg.getValue()) + "x" + std::to_string(n_arg.getValue()) + "x" + std::to_string(k_arg.getValue()) + "_" +
            (alpha_arg.getValue() != 1 ? std::to_string(alpha_arg.getValue()) + "_" : "") + (beta_arg.getValue() != 0 ? std::to_string(beta_arg.getValue()) + "_" : "") +
            (transpose_a_arg.getValue() ? "t" : "n") + (transpose_b_arg.getValue() ? "t" : "n")
    );
}

void gold_data_path(std::vector<std::string> &path) {
    path.push_back(format_arg.getValue());
    path.push_back(
            std::to_string(m_arg.getValue()) + "x" + std::to_string(n_arg.getValue()) + "x" + std::to_string(k_arg.getValue()) + "_" +
            (alpha_arg.getValue() != 1 ? std::to_string(alpha_arg.getValue()) + "_" : "") + (beta_arg.getValue() != 0 ? std::to_string(beta_arg.getValue()) + "_" : "") +
            (transpose_a_arg.getValue() ? "t" : "n") + (transpose_b_arg.getValue() ? "t" : "n")
    );
}