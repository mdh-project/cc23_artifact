void calculate_gold(const std::string &data_dir) {
    std::ofstream gold_file(data_dir + "/gold.tsv", std::ios::out | std::ios::trunc);

    auto a_mat = [&] (int i, int k) {
        if ((format_arg.getValue() == "row-major" && !transpose_a_arg.getValue()) || (format_arg.getValue() == "column-major" && transpose_a_arg.getValue())) {
            return a[i * k_arg.getValue() + k];
        } else {
            return a[k * m_arg.getValue() + i];
        }
    };
    auto b_mat = [&] (int k, int j) {
        if ((format_arg.getValue() == "row-major" && !transpose_b_arg.getValue()) || (format_arg.getValue() == "column-major" && transpose_b_arg.getValue())) {
            return b[k * n_arg.getValue() + j];
        } else {
            return b[j * k_arg.getValue() + k];
        }
    };
    auto c_mat = [&] (int i, int j) {
        if (format_arg.getValue() == "row-major") {
            return b[i * n_arg.getValue() + j];
        } else {
            return b[j * m_arg.getValue() + i];
        }
    };

    for (size_t l1 = 0; l1 < (format_arg.getValue() == "row-major" ? m_arg.getValue() : n_arg.getValue()); ++l1) {
        for (size_t l2 = 0; l2 < (format_arg.getValue() == "row-major" ? n_arg.getValue() : m_arg.getValue()); ++l2) {
            auto i = format_arg.getValue() == "row-major" ? l1 : l2;
            auto j = format_arg.getValue() == "row-major" ? l2 : l1;
            float acc = 0;
            for (size_t k = 0; k < k_arg.getValue(); ++k) {
                acc += a_mat(i, k) * b_mat(k, j);
            }
            gold_file << (alpha_arg.getValue() * acc + beta_arg.getValue() * c_mat(i, j)) << "\t";
        }
        gold_file << std::endl;
    }
    gold_file.close();
}
