#include "data_util/include/data_util.hpp"
#include "tclap/CmdLine.h"
#include "json/json.hpp"
using json = nlohmann::json;

#include <mkl.h>

#include <algorithm>
#include <utility>
#include <fstream>
#include <chrono>
#include <regex>

#include "meta_info.cpp"
#include "data_info.cpp"
#include "bench_info.cpp"

int main(int argc, const char **argv) {
    TCLAP::CmdLine cmd("Benchmark oneMKL routines.", ' ', "none", false);

    TCLAP::ValueArg<int> warm_ups_arg("", "warm-ups", "Number of warm ups.", false, 10, "int");
    cmd.add(warm_ups_arg);

    TCLAP::ValueArg<int> evaluations_arg("", "evaluations", "Number of evaluations.", false, 200, "int");
    cmd.add(evaluations_arg);

    TCLAP::ValueArg<std::string> gold_file_arg("g", "gold-file", "Gold file for result check.", false, "", "string");
    cmd.add(gold_file_arg);

    TCLAP::ValueArg<std::string> path_suffix_arg("x", "suffix", "Data path suffix.", false, "", "string");
    cmd.add(path_suffix_arg);

    TCLAP::ValueArg<std::string> full_path_arg("", "path", "Data path.", false, "", "string");
    cmd.add(full_path_arg);

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    // prepare directory for tuning results
    std::ifstream cpu_info_file;
    cpu_info_file.open("/proc/cpuinfo", std::ifstream::in);
    std::string cpu_info((std::istreambuf_iterator<char>(cpu_info_file)),
                         std::istreambuf_iterator<char>());
    cpu_info_file.close();
    std::regex rgx(R"(model name.*: (.+))");
    std::sregex_iterator rgx_begin(cpu_info.begin(), cpu_info.end(), rgx);
    std::sregex_iterator rgx_end;
    std::string dev_name;
    if (rgx_begin != rgx_end) {
        dev_name = rgx_begin->str(1);
    } else {
        std::cerr << "cpu name not found" << std::endl;
        exit(EXIT_FAILURE);
    }
    char buf[198];
    mkl_get_version_string(buf, 198);
    std::string version(buf);
    std::string application = "oneMKL";
    std::string routine = "sgemm";
    std::string data_dir = full_path_arg.getValue();
    if (data_dir.empty()) {
        std::vector<std::string> path = {dev_name, application, version, routine};
        benchmark_data_path(path);
        if (!path_suffix_arg.getValue().empty()) path.emplace_back(path_suffix_arg.getValue());
        data_dir = "../" + data_directory(path);
    }
    prepare_data_directory(data_dir, true);

    // initialize data
    init_data();
    prepare_api_call();

    // warm ups
    std::vector<long long> warm_ups;
    for (int i = 0; i < warm_ups_arg.getValue(); ++i) {
        auto start = std::chrono::high_resolution_clock::now();
        exec_api_call();
        auto end = std::chrono::high_resolution_clock::now();
        warm_ups.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count());
    }

    // evaluations
    std::vector<long long> evaluations;
    for (int i = 0; i < evaluations_arg.getValue(); ++i) {
        auto start = std::chrono::high_resolution_clock::now();
        exec_api_call();
        auto end = std::chrono::high_resolution_clock::now();
        evaluations.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count());
    }

    // check results
    std::string error_status = "not checked";
    if (!gold_file_arg.getValue().empty()) {
        // check result
        if (result_correct(gold_file_arg.getValue())) {
            error_status = "no errors found";
        } else {
            error_status = "errors found";
        }
    }

    // write profiling data to files
    json meta_json;
    meta_json["warm ups"] = std::to_string(warm_ups_arg.getValue());
    meta_json["evaluations"] = std::to_string(evaluations_arg.getValue());
    meta_json["gold file"] = gold_file_arg.getValue();
    meta_json["error status"] = error_status;
    meta_json["oneMKL version"] = version;
    std::ofstream meta_file(data_dir + "/benchmark_meta", std::ios::out | std::ios::trunc);
    meta_file << std::setw(4) << meta_json;
    meta_file.close();
    json runtimes_json;
    runtimes_json["warm ups"] = warm_ups;
    runtimes_json["evaluations"] = evaluations;
    long long runtimes_min = *std::min_element(evaluations.begin(), evaluations.end());
    std::nth_element(evaluations.begin(),
                     evaluations.begin() + (evaluations.size() / 2),
                     evaluations.end());
    long long runtimes_median = evaluations[evaluations.size() / 2];
    long long runtimes_average = std::accumulate(evaluations.begin(), evaluations.end(), 0ll) / evaluations.size();
    long long runtimes_max = *std::max_element(evaluations.begin(), evaluations.end());
    std::ofstream runtimes_file(data_dir + "/runtimes", std::ios::out | std::ios::trunc);
    runtimes_file << std::setw(4) << json(runtimes_json);
    runtimes_file.close();
    std::ofstream runtimes_min_file(data_dir + "/runtimes_min", std::ios::out | std::ios::trunc);
    runtimes_min_file << runtimes_min;
    runtimes_min_file.close();
    std::ofstream runtimes_median_file(data_dir + "/runtimes_median", std::ios::out | std::ios::trunc);
    runtimes_median_file << runtimes_median;
    runtimes_median_file.close();
    std::ofstream runtimes_average_file(data_dir + "/runtimes_average", std::ios::out | std::ios::trunc);
    runtimes_average_file << runtimes_average;
    runtimes_average_file.close();
    std::ofstream runtimes_max_file(data_dir + "/runtimes_max", std::ios::out | std::ios::trunc);
    runtimes_max_file << runtimes_max;
    runtimes_max_file.close();
}

