#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }

mkdir $ARTIFACT_ROOT/results &> /dev/null
rm -rf $ARTIFACT_ROOT/results/cpu &> /dev/null
cp -r defaults/skylake results/cpu &&
echo &&
echo &&
echo "defaults for Intel Skylake CPU now in use"
