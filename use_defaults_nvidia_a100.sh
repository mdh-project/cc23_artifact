#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }

mkdir $ARTIFACT_ROOT/results &> /dev/null
rm -rf $ARTIFACT_ROOT/results/gpu &> /dev/null
cp -r defaults/ampere results/gpu &&
echo &&
echo &&
echo "defaults for NVIDIA Ampere GPU now in use"
