# (De/Re)-Compositions Expressed Systematically via MDH-Based Schedules

This artifact contains the workflow to reproduce the experiments presented in the paper *(De/Re)-Compositions Expressed Systematically via MDH-Based Schedules* accepted for publication at the [ACM SIGPLAN 2023 International Conference on Compiler Construction](https://conf.researchr.org/home/CC-2023). The user is invited to perform the steps described below.

In case of **any problems**, please feel free to **open an issue** to get in touch with the authors.

## Hardware Dependencies

All experiments in our paper have been conducted on:
- NVIDIA Ampere GPU A100-PCIE-40GB
- NVIDIA Volta GPU V100-SXM2-16GB
- Intel Xeon Skylake CPU Gold-6140 (Dual-Socket)
- Intel Xeon Broadwell CPU E5-2683 v4 (Dual-Socket)

Our experiments can also be conducted on other OpenCL and/or CUDA-capable devices, but then require auto-tuning (see step [Auto-Tuning](https://gitlab.com/mdh-project/artifacts/cc23_artifact#optional-auto-tuning) below).

## Software Dependencies

We provide two [Docker images](https://hub.docker.com/_/docker) that contain all required dependencies (see step [1)](https://gitlab.com/mdh-project/artifacts/cc23_artifact#1-loadingbuilding-the-artifact) below for details).

If the user does not want to use Docker, the following dependencies are required to build the artifact from source: 
 
- [CentOS Linux release 7.9.2009](https://wiki.centos.org/Manuals/ReleaseNotes/CentOS7.2009)
- [Linux kernel version 3.10.0-1160.80.1.el7.x86_64](http://rpmfind.net/linux/RPM/centos/updates/7.9.2009/x86_64/Packages/kernel-3.10.0-1160.80.1.el7.x86_64.html)
- [C++17 compiler](https://en.cppreference.com/w/cpp/compiler_support)
- [CMake 3.18](https://cmake.org/)
- [CUDA 11.4](https://developer.nvidia.com/cuda-toolkit) (for experiments on CUDA devices) 
- [OpenCL 1.2](https://www.khronos.org/opencl/) (for experiments on OpenCL devices)
- [Python 2.7](https://www.python.org) with packages: [OpenTuner 0.8.0](http://opentuner.org/) and [`tabulate`](https://pypi.org/project/tabulate/)

Further requirements for benchmarking the related approaches:

- [Apache TVM 0.8.0](https://tvm.apache.org/) (using [LLVM 11.0.0](https://llvm.org))
- [NVIDIA HPC SDK 22.1](https://developer.nvidia.com/hpc-sdk)
- [Intel oneAPI Base Toolkit 2022.0.0](https://www.intel.com/content/www/us/en/developer/tools/oneapi/base-toolkit.html)

## Workflow

The workflow of this artifact is divided into three main steps: **1) Loading/Building the artifact**, **2) Loading pre-tuned kernels for one CPU and/or GPU**, **3) Benchmarking MDH and its competitors**. After step [3)](https://gitlab.com/mdh-project/artifacts/cc23_artifact#3-benchmarking-mdh-and-the-competitors), the benchmarking results will be printed to the screen and can also be found in file `results/summary.txt`. 

### 1) **Loading/Building the artifact**

The artifact can be *either* loaded as a Docker image (described in subsection **a)**) or built from source (subsection **b)**):

   **a) Loading the artifact as a Docker image:**

   We provide two Docker images, containing the experiments for CPU and GPU, respectively. After executing the steps below for starting the Docker image, continue with step [2)](https://gitlab.com/mdh-project/artifacts/cc23_artifact#2-loading-pre-tuned-kernels-for-one-cpu-andor-gpu).
   
   For GPU (requires the [NVIDIA Container Toolkit](https://github.com/NVIDIA/nvidia-docker)):

    docker build -t cc23_artifact_gpu -f docker/Dockerfile_gpu .
    docker run --gpus all -it cc23_artifact_gpu bash
    source environment.sh

   For CPU:

    docker build -t cc23_artifact_cpu -f docker/Dockerfile_cpu .
    docker run -it cc23_artifact_cpu bash
    source environment.sh

   **b) Building the artifact from source:**

   Clone the repository:
   
   `$ git clone https://gitlab.com/mdh-project/cc23_artifact.git`
   
   Change into the artifact directory:
   
   `$ cd cc23_artifact`
   
   Initialize required environment variables:
   
   `$ source environment.sh`

   The `environment.sh` file can be adapted to the user's environment:

- lines 6-7 enable/disable evaluation on CPU/GPU
- lines 12-17 enable/disable evaluation for each competitor
- lines 22-33 enable/disable evaluation for each experiment
- lines 39-47 set custom paths to competitors' installation locations

Compile artifact's source code:
   
   `$ ./build.sh`
   
   All arguments provided to script `build.sh` will be directly forwarded to CMake when building the binaries. For example, forwarding arguments can be required if some dependencies cannot be found automatically by CMake (e.g., when dependencies are not installed in their default locations). 

### 2) **Loading pre-tuned kernels for one CPU and/or GPU**

Per default, our artifact provides pre-tuned kernels for our specific GPUs and CPUs when using NVIDIA CUDA 11.4, Intel OpenCL 18.1.0.0920, and LLVM 11.0.0:

- NVIDIA Ampere GPU A100-PCIE-40GB
- NVIDIA Volta GPU V100-SMX2-16GB
- Intel Xeon Skylake CPU Gold-6140 (Dual-Socket)
- Intel Xeon Broadwell CPU E5-2683 v4 (Dual-Socket)

To use our pre-tuned kernels, please execute one CPU and/or one GPU script of the following list (note that at most one GPU script and one CPU script can be loaded at the same time):

- `use_defaults_nvidia_a100.sh`
- `use_defaults_nvidia_v100.sh`
- `use_defaults_intel_gold_6140.sh`
- `use_defaults_intel_e5_2683.sh`

For example, to load pre-tuned kernels for the NVIDIA Ampere GPU and Intel Skylake CPU, execute:

    $ ./use_defaults_nvidia_a100.sh
    $ ./use_defaults_intel_gold_6140.sh

Alternatively, if the user intends to only execute the artifact on the NVIDIA Volta GPU, execute:

    $ ./use_defaults_nvidia_v100.sh

#### Optional: Auto-Tuning

For devices other than the ones listed above, auto-tuning needs to be run before step [3)](https://gitlab.com/mdh-project/artifacts/cc23_artifact#3-benchmarking-mdh-and-the-competitors) via:

`$ ./tune.sh`

Note that auto-tuning may take a long time, because for each routine both TVM+Ansor and MDH are auto-tuned for several hours (in total up to >15 days per device to tune for). Consequently, we recommend to disable experiments in file `environment.sh` to reduce the artifact's runtime. Also, the user may edit the variable `TUNING_TIME_IN_MINUTES` in file `tune.sh` to decrease the overall auto-tuning time (note that low tuning times might affect performance!).
The tuning time time can further reduced by running the tuning in parallel using the SLURM workload manager. After step [1)](https://gitlab.com/mdh-project/artifacts/cc23_artifact#1-loadingbuilding-the-artifact), execute file `tune.sh` using SLURM's `sbatch`:

    sbatch <sbatch flags for partition selection, resource allocation, etc.> --array=0-49 ./tune.sh

When tuning in fully parallel (i.e. all 50 jobs are running simultaneously), tuning time is reduced to the value used in `TUNING_TIME_IN_MINUTES` (12h by default).

### 3) **Benchmarking MDH and the competitors**

   The experiments are started by executing:

   `$ ./bench.sh`
   
   The benchmarking results will be printed to the screen and can also be found in file `results/summary.txt`. 