//
//  abstract_local_buffer.hpp
//  ocal
//
//  Created by Ari Rasch on 18.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef abstract_local_buffer_h
#define abstract_local_buffer_h

// TODO: local_buffer in ::ocal::common verschieben, da auch von ocal::device genutzt

namespace ocal
{

namespace core
{

namespace abstract
{


template< typename T >
class local_or_shared_buffer
{
  public:
    local_or_shared_buffer( size_t size )
      : _size( size * sizeof(T) )
    {}
  
  
    ~local_or_shared_buffer() = default;
  
  
    auto size() const
    {
      return _size;
    }
    
  
  private:
    size_t _size;
};


} // namespace "abstract"

} // namespace "core"

} // namespace "ocal"


#endif /* abstract_local_buffer_h */
