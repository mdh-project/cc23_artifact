//
//  ocl.hpp
//  
//
//  Created by Ari Rasch on 17/10/16.
//
//

#ifndef ocl_hpp
#define ocl_hpp

#include <vector>
#include <array>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <iostream>
#include <regex>
#include <sstream>
#include <fstream>
#include <cmath>
#include <numeric>
#include <cstdlib>
#include <memory> 

#include "assert.h"

#define __CL_ENABLE_EXCEPTIONS
#include "cl.hpp"

#ifndef NUM_OPENCL_COMMAND_QUEUES_PER_DEVICE
  #define NUM_OPENCL_COMMAND_QUEUES_PER_DEVICE  3 // number of OpenCL queues for overlapping computations with communications
#endif 


// global definitions
namespace ocal
{

namespace core
{

namespace ocl
{


// helper type
using command_queue_map = std::unordered_map< int, cl::CommandQueue >;

std::unordered_map< cl_uint, cl::Context >   opencl_platform_id_to_context;                       // 1:1 correspondence
std::unordered_map< int,     cl_uint >       unique_device_id_to_opencl_platform_id;              // n:1 correspondence
std::unordered_map< int,     cl_uint >       unique_device_id_to_opencl_device_id;                // 1:1 correspondence

std::unordered_map< int, cl::Device>         unique_device_id_to_opencl_device;                   // 1:1 correspondence

std::unordered_map< int, command_queue_map > unique_device_id_to_opencl_command_queues;           // 1:1 correspondence
std::unordered_map< int, int >               unique_device_id_to_actual_opencl_command_queue_id;  // 1:1 correspondence


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"


// common classes
#include "../common/helper.hpp"
#include "../common/event_manager.hpp"
#include "../common/source_to_source_translation.hpp"


#include "../../common/helper.hpp"
#include "../../common/kernel_argument_specifiers.hpp"
#include "../../common/return_proxy.hpp"
#include "../../common/static_parameters.hpp"

// ocal::core::common helper
#include "../common/helper.hpp"

// ocal::core::ocl helper
#include "ocl_helper.hpp"


// global definitions
namespace ocal
{

namespace core
{

namespace ocl
{

cl_uint PLATFORM_ID_OF_HOST_DEVICE = get_platform_id_of_host_platform();

using ocl_event_manager = common::event_manager< clEvent_wrapper >;


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"


// abstract classes
#include "../abstract/abstract_buffer.hpp"
#include "../abstract/abstract_host_buffer.hpp"
#include "../abstract/abstract_unified_buffer.hpp"
#include "../abstract/abstract_local_or_shared_buffer.hpp"
#include "../abstract/abstract_thread_configuration.hpp"
#include "../abstract/abstract_kernel.hpp"
#include "../abstract/abstract_device.hpp"


// ocal::core::ocl header
#include "ocl_buffer.hpp"
#include "ocl_host_buffer.hpp"
#include "ocl_unified_buffer.hpp"
#include "ocl_local_buffer.hpp"
#include "ocl_nd_range.hpp"
#include "ocl_kernel.hpp"
#include "ocl_device.hpp"


// global definitions
namespace ocal
{

namespace core
{

namespace ocl
{

class ocl_init_dummy_class
{
  public:
    // OCL "init"
    ocl_init_dummy_class()
    {
      ocal::core::ocl::device{ ocl::PLATFORM_ID_OF_HOST_DEVICE }; // Note: required for OpenCL host/unified buffer (buffer operations require host device's command queue)
    }
  
  
    // OCL "free"
    ~ocl_init_dummy_class() = default; // Note: OpenCL object are free via RAII
};

// init/free OCL
ocl_init_dummy_class ocl_init_dummy = ocl_init_dummy_class{};


//[]()
//{
//  ocal::core::ocl::device{ ocl::PLATFORM_ID_OF_HOST_DEVICE }; // Note: required for OpenCL host/unified buffer (buffer operations require host device's command queue)
//
//  return 0;
//}();


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"


#endif /* ocl_hpp */
