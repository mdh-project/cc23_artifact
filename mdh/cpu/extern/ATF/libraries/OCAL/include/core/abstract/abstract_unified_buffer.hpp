//
//  abstract_unified_buffer.hpp
//  ocal
//
//  Created by Ari Rasch on 13.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef abstract_unified_buffer_h
#define abstract_unified_buffer_h


namespace ocal
{

namespace core
{

namespace abstract
{


template< typename child_t, typename buffer_t, typename event_t, typename host_memory_ptr_t, typename T >
class abstract_unified_buffer : public common::event_manager< event_t >
{
  friend class ::ocal::common::return_proxy< abstract_unified_buffer< child_t, buffer_t, event_t, host_memory_ptr_t, T >, T >;
  
  public:
    template< typename callable_t >
    abstract_unified_buffer( size_t size, callable_t create_host_buffer, int platform_id_of_host )
      : _size( size ), _platform_id_of_host( platform_id_of_host ), _platform_or_stream_id_to_opencl_or_cuda_buffer(), _host_memory( create_host_buffer() ), _ptr_to_host_memory( size, _host_memory ), _platform_or_stream_id_to_dirty_flag(), _host_is_dirty( false )
    {
      // set buffer as up to date
      _platform_or_stream_id_to_dirty_flag[ _platform_id_of_host ] = false;
    }
  
  
    abstract_unified_buffer()                                  = delete;  // Note: buffer are initialized with input size
    abstract_unified_buffer( const abstract_unified_buffer&  ) = default; // Note: required to put buffers in std::vector
    abstract_unified_buffer(       abstract_unified_buffer&& ) = default;

    abstract_unified_buffer& operator=( const abstract_unified_buffer&  ) = default;
    abstract_unified_buffer& operator=(       abstract_unified_buffer&& ) = default;

  
    virtual ~abstract_unified_buffer() = default;
//    {
//      for( auto& ptr : _pointer_to_delete )
//        delete[] ptr;
//    }


    // returns a proxy which enables differentiating betweeen read and write accesses
    auto operator[]( size_t i )
    {
      assert( i < _size );
      
      return ::ocal::common::return_proxy< abstract_unified_buffer< child_t, buffer_t, event_t, host_memory_ptr_t, T >, T >( *this, i );
    }

  
    // ----------------------------------------------------------
    //   set/get host memory (static polymorphism)
    // ----------------------------------------------------------

    void set_content( T* new_content )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->set( new_content ); 
    }
  

    std::vector<T> get_content()
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      return me_as_child->get();
    }
  
  
    // ----------------------------------------------------------
    //   buffer access functions
    // ----------------------------------------------------------
  
    // returns the device buffer
    buffer_t& get_device_buffer_with_read_access( int unique_device_id, int ) // Note: command queue id not required for unified buffers
    {
      // get platform id
      auto platform_id = this->get_platform_id( unique_device_id );
    
      // create device buffer if it does not already exist
      this->create_platform_buffer_if_not_existent( platform_id );
    
      // refresh data in buffer
      this->update_platform_buffer( platform_id );
      
      return this->_platform_or_stream_id_to_opencl_or_cuda_buffer.at( platform_id );
    }
  
  
    // returns the device buffer and sets dirty flags
    buffer_t& get_device_buffer_with_write_access( int unique_device_id, int ) // Note: command queue id not required for unified buffers
    {
      // get platform id
      auto platform_id = this->get_platform_id( unique_device_id );
    
      // create device buffer if it does not already exist
      this->create_platform_buffer_if_not_existent( platform_id );
      
      // set dirty flags
      this->set_all_platform_buffers_as_dirty_except( platform_id );
      
      return this->_platform_or_stream_id_to_opencl_or_cuda_buffer.at( platform_id );
    }
  
  
    // returns the device buffer and sets dirty flags
    buffer_t& get_device_buffer_with_read_write_access( int unique_device_id, int ) // Note: command queue id not required for unified buffers
    {
      // return OpenCL device buffer
      auto platform_id = this->get_platform_id( unique_device_id );
    
      // create device buffer if it does not already exist
      this->create_platform_buffer_if_not_existent( platform_id );

      // refresh data in buffer
      this->update_platform_buffer( platform_id );
      
      // set dirty flags
      this->set_all_platform_buffers_as_dirty_except( platform_id );
      
      return this->_platform_or_stream_id_to_opencl_or_cuda_buffer.at( platform_id );
    }
  
  
  protected:

    // ---------------------------------------------------------------------------------------
    //   native Buffer management: creation and H2D/D2H data transfers (static polymorphism)
    // ---------------------------------------------------------------------------------------

    void create_platform_buffer_if_not_existent( int platform_id )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->create_platform_buffer_if_not_existent( platform_id );
    }
    

    void copy_data_from_platform_buffer_to_platform_buffer( int platform_id_of_source, int platform_id_of_destination )
    {
      // flush host memory
      this->flush_host_memory();

      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->copy_data_from_platform_buffer_to_platform_buffer( platform_id_of_source, platform_id_of_destination );
      
      // flush host memory
      this->flush_host_memory();
    }


    void copy_data_from_host_memory_to_platform_buffer( int platform_id )
    {
      // flush host memory
      this->flush_host_memory();

      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->copy_data_from_host_memory_to_platform_buffer( platform_id );

      // flush host memory
      this->flush_host_memory();
    }


    void copy_data_from_platform_buffer_to_host_memory( int platform_id )
    {
      // flush host memory
      this->flush_host_memory();

      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->copy_data_from_platform_buffer_to_host_memory( platform_id );
      
      // flush host memory
      this->flush_host_memory();
    }

  
  
    // ----------------------------------------------------------
    //   flag management
    // ----------------------------------------------------------
  
    // setter
    void set_all_platform_buffers_as_dirty_except( int platform_id )
    {
      for( auto& flag : _platform_or_stream_id_to_dirty_flag )
        flag.second = true;
      
      _platform_or_stream_id_to_dirty_flag.at( platform_id ) = false;
      
      // manage host flag
      if( platform_id == _platform_id_of_host )
        _host_is_dirty = false;
      else
        _host_is_dirty = true;
    }
  
  
    void set_all_platform_buffers_as_dirty()
    {
      for( auto& flag : _platform_or_stream_id_to_dirty_flag )
        flag.second = true;
      
      _host_is_dirty = true; // Note: host memory is in case of unified_buffers also a platform buffer
    }
  
  
    void set_platform_buffer_as_up_to_date( int platform_id )
    {
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( platform_id) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );
      
      _platform_or_stream_id_to_dirty_flag.at( platform_id ) = false;
    }
  
  
    void set_host_memory_as_dirty()
    {
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( _platform_id_of_host) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );
      
      _platform_or_stream_id_to_dirty_flag.at( _platform_id_of_host ) = true;
    }


    void set_host_memory_as_up_to_date()
    {
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( _platform_id_of_host) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );
      
      // if host memory is up to date do nothing ( avoids costly operation "_platform_or_stream_id_to_dirty_flag.at(...)" below)
      if( this->is_host_memory_up_to_date() )
        return;
      
      _platform_or_stream_id_to_dirty_flag.at( _platform_id_of_host ) = false;
      _host_is_dirty                                                  = false; // Note: _host_is_dirty is redundant flag for high performance
    }
  
  
    // getter
    bool is_platform_buffer_up_to_date( int platform_id ) const
    {
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( platform_id) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );
      
      return _platform_or_stream_id_to_dirty_flag.find( platform_id ) != _platform_or_stream_id_to_dirty_flag.end() && // device exists?
             _platform_or_stream_id_to_dirty_flag.at( platform_id ) == false;                                          // device up to date?
    }

  
    bool is_host_memory_up_to_date() const
    {
      return _host_is_dirty == false;
    }


    // ----------------------------------------------------------
    //   buffer refreshing functions (manage dirty flags)
    // ----------------------------------------------------------

    void update_host_memory()
    {
      // if host is already up to date then do nothing
      if( this->is_host_memory_up_to_date() )
      {
        // wait for critical events to finish
        this->sync_write_events_on_host_memory();        

        return;
      }
      
      this->update_platform_buffer( _platform_id_of_host );
      
      // update dirty flag
      this->set_host_memory_as_up_to_date();
    }


    virtual void update_platform_buffer( int platform_id )  // TODO: static polymorphism
    {}
  
  
    // ----------------------------------------------------------
    //   host memory access functions (manage dirty flags)
    // ----------------------------------------------------------

    // scalar read access of host memory
    void read_from_host_memory( size_t i, T& val )
    {
      assert( i < _size );
      
      this->update_host_memory();
      
      val = _ptr_to_host_memory[ i ];
    }

  
    // scalar write access of host memory
    void write_in_host_memory( size_t i, const T& val )
    {
      assert( i < _size );
      
      this->update_host_memory();

      _ptr_to_host_memory[ i ] = val;

      // set dirty flags
      this->set_all_platform_buffers_as_dirty_except( _platform_id_of_host ); // Note: do not set the device buffer as dirty that corresponds to the host buffer
    }

  
    // ----------------------------------------------------------
    //   helper
    // ----------------------------------------------------------
  
    // find up-to-date device buffer and return platform_id
    int get_platform_id_of_clean_platform_buffer() const
    {
      // iterate over flags
      for( const auto& flag : _platform_or_stream_id_to_dirty_flag )
        // if flag is clean
        if( flag.second == false )
          // return platform id
          return flag.first;
      
      assert( false && "should never be reached" );
      return -1;
    }
  

    // required to minimize map/unmap operations of OpenCL host_buffers
    void flush_host_memory()
    {
      _ptr_to_host_memory.flush();
      //_ptr_to_host_memory = host_memory_ptr_t( _ptr_to_host_memory );  // Note: assigning the same object causes flushing the buffer in case of OpenCL host/unified buffer and nothing otherwise
    }
    
  
    // (static polymorphism)
    int get_platform_id( int unique_device_id )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      return me_as_child->get_platform_id( unique_device_id );
    }
  
  
  protected:
    size_t                            _size;
    int                               _platform_id_of_host;
    std::unordered_map<int, buffer_t> _platform_or_stream_id_to_opencl_or_cuda_buffer; // Note: has to be declared before "_host_memory" due to host buffer creation of unified buffers
    buffer_t                          _host_memory;
    host_memory_ptr_t                 _ptr_to_host_memory;
    std::unordered_map<int, bool>     _platform_or_stream_id_to_dirty_flag;
    bool                              _host_is_dirty;
};


} // namespace "abstract"

} // namespace "core"

} // namespace "ocal"


#endif /* abstract_unified_buffer_h */
