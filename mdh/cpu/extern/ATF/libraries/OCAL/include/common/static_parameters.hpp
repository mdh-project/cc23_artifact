//
//  static_parameters.hpp
//  ocal
//
//  Created by Ari Rasch on 07.11.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef static_parameters_hpp
#define static_parameters_hpp


namespace ocal
{

namespace common
{


class static_parameters
{
  public:
    template< typename... Ts >
    explicit
    static_parameters( Ts... sps )
      : _sps_as_string()
    {
      transform_sps_to_strings( sps... );
    }
  
    auto get_sps_as_string( const std::string& seperating_symbol ) const
    {
      //assert( !_sps_as_string.empty() );
      
      if( _sps_as_string.empty() )
        return std::string( "" );
      
      // concatenate all the sps and use seperating_symbol for separating the sps
      std::string sp_string = _sps_as_string[ 0 ];
      
      for( size_t i = 1 ; i < _sps_as_string.size() ; ++i )
        sp_string += seperating_symbol + _sps_as_string[ i ];
      
      return sp_string;
    }
  
  
    bool empty() const
    {
      return _sps_as_string.empty();
    }
  
  
  private:
  
    // IS
    template< typename T, typename... Ts >
    void transform_sps_to_strings( T sp, Ts... sps )
    {
      // sp to std::string
      std::stringstream ss;
      ss << sp;
      
      _sps_as_string.push_back( ss.str() );
      
      transform_sps_to_strings( sps... );
    }
  
  
    // IA
    void transform_sps_to_strings()
    {}
  
  
  private:
    std::vector< std::string > _sps_as_string;
};


} // namespace "common"

} // namespace "ocal"



#endif /* static_parameters_hpp */
