//
//  ocal_buffer.hpp
//  ocal_ocal
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocal_buffer_hpp
#define ocal_buffer_hpp


namespace ocal
{


// device buffer
template< typename T >
class buffer //: public ::ocl::buffer< T,N >, public ::cuda::buffer< T,N >
{

  friend class ::ocal::common::return_proxy< buffer, T >;
  
  public:
    buffer( size_t size = 0 )
      : _size( size ), _host_memory( new T[ size ] ), _ocl_buffer( size, _host_memory.get() ), _cuda_buffer( size, _host_memory.get() ), _is_dirty_host_memory( false ), _is_dirty_ocl_buffer( false ), _is_dirty_cuda_buffer( false )
    {}


    buffer( const std::vector<T>& vec )
      : _size( vec.size() ), _host_memory( new T[ vec.size() ] ), _ocl_buffer( vec.size(), _host_memory ), _cuda_buffer( vec.size(), _host_memory ), _is_dirty_host_memory( false ), _is_dirty_ocl_buffer( false ), _is_dirty_cuda_buffer( false )
    {
      this->set_content( vec.data() );
    }

  
    buffer( const buffer<T>&  ) = default;
    buffer(       buffer<T>&& ) = default;

    buffer& operator=( const buffer&  ) = default;
    buffer& operator=(       buffer&& ) = default;
  
    ~buffer()                   = default;


    // returns a proxy which enables differentiating betweeen read and write accesses
    auto operator[]( int i )
    {
      if( i >= _size )
      {
        std::wcerr << "invalid buffer index: " << i << std::endl;
        
        assert( false );
        exit( EXIT_FAILURE );
      }
      
      return ::ocal::common::return_proxy< buffer<T>, T >( *this, i );
    }


    T* get_host_memory_ptr()
    {
      this->update_host_memory();
      
      return _host_memory.get();
    }

    size_t size()
    {
        return _size;
    }
  
  
  
    // ----------------------------------------------------------
    //   buffer access functions
    // ----------------------------------------------------------
  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_read_access()
    {
      this->update_ocl_buffer();
      
      return _ocl_buffer;
    }
  
  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_write_access()
    {
      //this->update_ocl_buffer();
      
      // manage flags
      this->set_host_memory_as_dirty();
      this->set_cuda_buffer_as_dirty();
      this->set_ocl_buffer_as_up_to_date();
      
      return _ocl_buffer;
    }

  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_read_write_access()
    {
      this->update_ocl_buffer();
      
      // manage flags
      this->set_host_memory_as_dirty();
      this->set_cuda_buffer_as_dirty();
      this->set_ocl_buffer_as_up_to_date();
      
      return _ocl_buffer;
    }
  

    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_read_access()
    {
      this->update_cuda_buffer();
      
      return _cuda_buffer;
    }

  
    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_write_access()
    {
      //this->update_cuda_buffer();
      
      // manage flags
      this->set_host_memory_as_dirty();
      this->set_ocl_buffer_as_dirty();
      this->set_cuda_buffer_as_up_to_date();
      
      return _cuda_buffer;
    }
  
  
    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_read_write_access()
    {
      this->update_cuda_buffer();
      
      // manage flags
      this->set_host_memory_as_dirty();
      this->set_ocl_buffer_as_dirty();
      this->set_cuda_buffer_as_up_to_date();
      
      return _cuda_buffer;
    }
  

    // ----------------------------------------------------------
    //   set/get host memory
    // ----------------------------------------------------------
  
    void set_content( T* new_content )
    {
      // copy data
      std::memcpy( _host_memory, new_content, _size * sizeof( T ) );
      
      // set only host buffer as up to date
      this->set_ocl_buffer_as_dirty();
      this->set_cuda_buffer_as_dirty();
      this->set_host_memory_as_up_to_date();
    }


    std::vector<T> get_content()
    {
      this->update_host_memory();
    
      // declare result vector
      std::vector<T> content;
      content.reserve( _size );
      
      // copy data
      content.insert( content.end(), &_host_memory[ 0 ], &_host_memory[ _size ] );
  
      // set only host buffer as up to date
      this->set_ocl_buffer_as_dirty();
      this->set_cuda_buffer_as_dirty();
      this->set_host_memory_as_up_to_date();

      return content;
    }


  private:

  
    // ----------------------------------------------------------
    //   host memory access functions (manage dirty flags)
    // ----------------------------------------------------------
  
    // reads from host memory at position i and writes in val
    void read_from_host_memory( int position, T& val )
    {
      this->update_host_memory();
      
      val = _host_memory.get()[ position ];
    }
  

    // writes val in host memory at position i
    void write_in_host_memory( int position, const T& val )
    {
      this->update_host_memory();
          
      _host_memory.get()[ position ] = val;
    }
  

    // ----------------------------------------------------------
    //   flag management
    // ----------------------------------------------------------
  
    void set_host_memory_as_up_to_date()
    {
      _is_dirty_host_memory = false;
    }
  
    void set_ocl_buffer_as_up_to_date()
    {
      _is_dirty_ocl_buffer = false;
    }
  
  
    void set_cuda_buffer_as_up_to_date()
    {
      _is_dirty_cuda_buffer = false;
    }

  
    void set_host_memory_as_dirty()
    {
      _is_dirty_host_memory = true;
    }

    void set_ocl_buffer_as_dirty()
    {
      _is_dirty_ocl_buffer = true;
    }

    void set_cuda_buffer_as_dirty()
    {
      _is_dirty_cuda_buffer = true;
    }



    // ----------------------------------------------------------
    //   buffer refreshing functions (manage dirty flags)
    // ----------------------------------------------------------

    void update_host_memory()
    {
      // either all memory regions are up to date or only the ocl/cuda buffer 
      assert( (!_is_dirty_host_memory && !_is_dirty_ocl_buffer && !_is_dirty_cuda_buffer) || (_is_dirty_host_memory && !_is_dirty_ocl_buffer && _is_dirty_cuda_buffer) || (_is_dirty_host_memory && _is_dirty_ocl_buffer && !_is_dirty_cuda_buffer) );
      
      // if host memory is up to date then do nothing
      if( !_is_dirty_host_memory )
        return;
      
      // if opencl buffer is up to date
      if( !_is_dirty_ocl_buffer )
      {
        // Note: assert( _cuda_buffer.is_host_memory_up_to_date() == true );
        
        _ocl_buffer.get_host_memory_ptr(); // Note: access causes _ocl_buffer' host memory to be updated
        
        this->set_host_memory_as_up_to_date();
        this->set_cuda_buffer_as_up_to_date();  // Note: allowed because of assertion above
        
        return;
      }
      
      // if cuda buffer is up to date
      if( !_is_dirty_cuda_buffer )
      {
        // Note: assert( _ocl_buffer.is_host_memory_up_to_date() == true );

        _cuda_buffer.get_host_memory_ptr(); // Note: access causes _cuda_buffer' host memory to be updated
        
        this->set_host_memory_as_up_to_date();
        this->set_ocl_buffer_as_up_to_date();   // Note: allowed because of assertion above
        
        return;
      }
      
      assert( false && "should never be reached" );
    }
  

    void update_ocl_buffer()
    {
      // either all memory regions are up to date or only the ocl/cuda buffer
      assert( (!_is_dirty_host_memory && !_is_dirty_ocl_buffer && !_is_dirty_cuda_buffer) || (_is_dirty_host_memory && !_is_dirty_ocl_buffer && _is_dirty_cuda_buffer) || (_is_dirty_host_memory && _is_dirty_ocl_buffer && !_is_dirty_cuda_buffer) );
      
      // if ocl buffer is up to date then do nothing
      if( !_is_dirty_ocl_buffer )
        return;
      
      // if host is up to date then copy data from host to ocl buffer
      if( !_is_dirty_host_memory )
      {
        assert( false && "should never be reached" ); // Note: should never be reached due to assertion above
        exit( EXIT_FAILURE );
      }
    
      // if cuda buffer is up to date
      if( !_is_dirty_cuda_buffer )
      {
        // Note: assert( _ocl_buffer.is_host_memory_up_to_date() == true );
      
        _cuda_buffer.get_host_memory_ptr(); // Note: access causes _ocl_buffer' host memory to be updated
        
        this->set_ocl_buffer_as_up_to_date();
        this->set_host_memory_as_up_to_date();
        
        return;
      }
      
      assert( false && "should never be reached" );
    }
  
  
    void update_cuda_buffer()
    {
      // either all memory regions are up to date or only the ocl/cuda buffer 
      assert( (!_is_dirty_host_memory && !_is_dirty_ocl_buffer && !_is_dirty_cuda_buffer) || (_is_dirty_host_memory && !_is_dirty_ocl_buffer && _is_dirty_cuda_buffer) || (_is_dirty_host_memory && _is_dirty_ocl_buffer && !_is_dirty_cuda_buffer) );
    
      // if cuda buffer is up to date then do nothing
      if( !_is_dirty_cuda_buffer )
        return;
     
      // if host is up to date then copy data from host to cuda buffer
      if( !_is_dirty_host_memory )
      {
        assert( false && "should never be reached" ); // Note: should never be reached due to assertion above
        exit( EXIT_FAILURE );
      }

      // if opencl buffer is up to date
      if( !_is_dirty_ocl_buffer )
      {
        // Note: assert( _cuda_buffer.is_host_memory_up_to_date() == true );
        
        _ocl_buffer.get_host_memory_ptr(); // Note: access causes _ocl_buffer' host memory to be updated
        
        this->set_cuda_buffer_as_up_to_date();
        this->set_host_memory_as_up_to_date();
        
        return;
      }
      
      assert( false && "should never be reached" );
    }
  
  
  private:
    size_t                _size;
    std::shared_ptr<T>    _host_memory;
    core::ocl::buffer<T>  _ocl_buffer;
    core::cuda::buffer<T> _cuda_buffer;
    bool                  _is_dirty_host_memory;
    bool                  _is_dirty_ocl_buffer;
    bool                  _is_dirty_cuda_buffer;
};



template< typename T >
class host_buffer
{
  friend class ::ocal::common::return_proxy< host_buffer, T >;
  
  public:
    host_buffer( size_t size )
      : _size( size ), _ocl_buffer( size ), _cuda_buffer( size ), _is_dirty_ocl_buffer( false ), _is_dirty_cuda_buffer( false )
    {}


    host_buffer( const std::vector<T>& vec )
      : _size( vec.size() ), _ocl_buffer( vec.size() ), _cuda_buffer( vec.size() ), _is_dirty_ocl_buffer( false ), _is_dirty_cuda_buffer( false )
    {
      this->set_content( vec.data() );
    }
  
  
    ~host_buffer() = default;
  
  
    // returns a proxy which enables differentiating betweeen read and write accesses
    auto operator[]( int i )
    {
      if( i >= _size )
      {
        std::wcerr << "invalid buffer index: " << i << std::endl;
        
        assert( false );
        exit( EXIT_FAILURE );
      }
      
      return common::return_proxy< host_buffer<T>, T >( *this, i );
    }
  
  
    T* get_host_memory_ptr()
    {
      return _ocl_buffer.get_host_memory_ptr(); // Note: using ocl_buffer as host_buffer
    }

  
    // ----------------------------------------------------------
    //   buffer access functions
    // ----------------------------------------------------------
  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_read_access()
    {
      this->update_ocl_buffer();
      
      return _ocl_buffer;//.get_ocl_ocal_buffer_with_read_access( unique_device_id, command_queue_id );
    }
  
  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_write_access()
    {
      //this->update_ocl_buffer();
      
      // manage flags
      this->set_cuda_buffer_as_dirty();
      this->set_ocl_buffer_as_up_to_date();
      
      return _ocl_buffer;//.get_ocl_ocal_buffer_with_write_access( unique_device_id, command_queue_id );
    }


  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_read_write_access()
    {
      this->update_ocl_buffer();
      
      // manage flags
      this->set_cuda_buffer_as_dirty();
      this->set_ocl_buffer_as_up_to_date();
      
      return _ocl_buffer;//.get_ocl_ocal_buffer_with_read_write_access( unique_device_id, command_queue_id );
    }
  

    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_read_access()
    {
      this->update_cuda_buffer();
      
      return _cuda_buffer;//.get_cuda_ocal_buffer_with_read_write_access( unique_device_id, cuda_stream_id );
    }

  
    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_write_access()
    {
      //this->update_cuda_buffer();
      
      // manage flags
      this->set_ocl_buffer_as_dirty();
      this->set_cuda_buffer_as_up_to_date();
      
      return _cuda_buffer;//.get_cuda_ocal_buffer_with_write_access( unique_device_id, cuda_stream_id );
    }
  
  
    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_read_write_access()
    {
      this->update_cuda_buffer();
      
      // manage flags
      this->set_ocl_buffer_as_dirty();
      this->set_cuda_buffer_as_up_to_date();
      
      return _cuda_buffer;//.get_cuda_ocal_buffer_with_read_write_access( unique_device_id, cuda_stream_id );
    }


    // ----------------------------------------------------------
    //   set/get host memory
    // ----------------------------------------------------------
  
    void set_content( T* new_content )
    {
      // copy data
      _ocl_buffer.set_content( new_content ); // Note: using ocl_buffer as host_buffer
      
      // set only ocl buffer as up to date
      this->set_ocl_buffer_as_up_to_date();
      this->set_cuda_buffer_as_dirty();
    }


    std::vector<T> get_content()
    {
      this->update_ocl_buffer();
    
      return _ocl_buffer.get_content();
    }

  
  private:
  
    // ----------------------------------------------------------
    //   host memory access functions (manage dirty flags)
    // ----------------------------------------------------------
  
    // reads from host memory at position i and writes in val
    void read_from_host_memory( int i, T& val )
    {
      if( !_is_dirty_ocl_buffer )
        val = _ocl_buffer[ i ];
      
      else if( !_is_dirty_cuda_buffer )
        val = _cuda_buffer[ i ];

      else
      {
        assert( false && "should never be reached" );
        exit( EXIT_FAILURE );
      }
    }

    // writes val in host memory at position i
    void write_in_host_memory( int i, const T& val )
    {
      if( !_is_dirty_ocl_buffer )
        _ocl_buffer[ i ] = val;
      
      else if( !_is_dirty_cuda_buffer )
        _cuda_buffer[ i ] = val;

      else
      {
        assert( false && "should never be reached" );
        exit( EXIT_FAILURE );
      }
    }
  

    // ----------------------------------------------------------
    //   flag management
    // ----------------------------------------------------------
  
    void set_ocl_buffer_as_up_to_date()
    {
      _is_dirty_ocl_buffer = false;
    }
  
  
    void set_cuda_buffer_as_up_to_date()
    {
      _is_dirty_cuda_buffer = false;
    }

  
    void set_ocl_buffer_as_dirty()
    {
      _is_dirty_ocl_buffer = true;
    }

    void set_cuda_buffer_as_dirty()
    {
      _is_dirty_cuda_buffer = true;
    }


    // ----------------------------------------------------------
    //   buffer refreshing functions (manage dirty flags)
    // ----------------------------------------------------------

    void update_ocl_buffer()
    {
      // if ocl buffer is up to date then do nothing
      if( !_is_dirty_ocl_buffer )
        return;
     
      // if cuda buffer is up to date
      if( !_is_dirty_cuda_buffer )
      {
        _ocl_buffer.set_content( _cuda_buffer.get_host_memory_ptr() );
        
        this->set_ocl_buffer_as_up_to_date();
        
        return;
      }
      
      assert( false && "should never be reached" );
    }
  
  
    void update_cuda_buffer()
    {
      // if cuda buffer is up to date then do nothing
      if( !_is_dirty_cuda_buffer )
        return;
     
      // if opencl buffer is up to date
      if( !_is_dirty_ocl_buffer )
      {
        _cuda_buffer.set_content( _ocl_buffer.get_host_memory_ptr() );
        
        this->set_cuda_buffer_as_up_to_date();
        
        return;
      }
      
      assert( false && "should never be reached" );
    }
  
  
  private:
    size_t                     _size;
    core::ocl::host_buffer<T>  _ocl_buffer;
    core::cuda::host_buffer<T> _cuda_buffer;
    bool                       _is_dirty_ocl_buffer;
    bool                       _is_dirty_cuda_buffer;
};



template< typename T >
class unified_buffer
{

  friend class ::ocal::common::return_proxy< unified_buffer, T >;
  
  public:
    unified_buffer( size_t size )
      : _size( size ), _ocl_buffer( size ), _cuda_buffer( size ), _is_dirty_ocl_buffer( false ), _is_dirty_cuda_buffer( false )
    {}


    unified_buffer( const std::vector<T>& vec )
      : _size( vec.size() ), _ocl_buffer( vec.size() ), _cuda_buffer( vec.size() ), _is_dirty_ocl_buffer( false ), _is_dirty_cuda_buffer( false )
    {
      this->set_content( vec.data() );
    }


    ~unified_buffer() = default;
  
  
    // returns a proxy which enables differentiating betweeen read and write accesses
    auto operator[]( int i )
    {
      if( i >= _size )
      {
        std::wcerr << "invalid buffer index: " << i << std::endl;
        
        assert( false );
        exit( EXIT_FAILURE );
      }
      
      return common::return_proxy<unified_buffer,T>( *this, i );
    }
  
  
    T* get_host_memory_ptr()
    {
      return _ocl_buffer.get_host_memory_ptr(); // Note: using ocl_buffer as host_buffer
    }

  
    // ----------------------------------------------------------
    //   buffer access functions
    // ----------------------------------------------------------
  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_read_access()//( int unique_device_id, int command_queue_id )
    {
      this->update_ocl_buffer();
      
      return _ocl_buffer;//.get_ocl_ocal_buffer_with_read_access( unique_device_id, command_queue_id );
    }
  
  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_write_access()//( int unique_device_id, int command_queue_id )
    {
      //this->update_ocl_buffer();
      
      // manage flags
      this->set_cuda_buffer_as_dirty();
      this->set_ocl_buffer_as_up_to_date();
      
      return _ocl_buffer;//.get_ocl_ocal_buffer_with_write_access( unique_device_id, command_queue_id );
    }


  
    // returns the device buffer and also sets dirty flags
    auto& get_ocl_ocal_buffer_with_read_write_access()//( int unique_device_id, int command_queue_id )
    {
      this->update_ocl_buffer();
      
      // manage flags
      this->set_cuda_buffer_as_dirty();
      this->set_ocl_buffer_as_up_to_date();
      
      return _ocl_buffer;//.get_ocl_ocal_buffer_with_read_write_access( unique_device_id, command_queue_id );
    }
  

    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_read_access()
    {
      this->update_cuda_buffer();
      
      return _cuda_buffer;//.get_cuda_ocal_buffer_with_read_write_access( unique_device_id, cuda_stream_id );
    }

  
    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_write_access()
    {
      //this->update_cuda_buffer();
      
      // manage flags
      this->set_ocl_buffer_as_dirty();
      this->set_cuda_buffer_as_up_to_date();
      
      return _cuda_buffer;//.get_cuda_ocal_buffer_with_write_access( unique_device_id, cuda_stream_id );
    }
  
  
    // returns the device buffer and also sets dirty flags
    auto& get_cuda_ocal_buffer_with_read_write_access()
    {
      this->update_cuda_buffer();
      
      // manage flags
      this->set_ocl_buffer_as_dirty();
      this->set_cuda_buffer_as_up_to_date();
      
      return _cuda_buffer;//.get_cuda_ocal_buffer_with_read_write_access( unique_device_id, cuda_stream_id );
    }
  
  
    // ----------------------------------------------------------
    //   set/get host memory
    // ----------------------------------------------------------
  
    void set_content( T* new_content )
    {
      // copy data
      _ocl_buffer.set_content( new_content ); // Note: using ocl_buffer as host_buffer
      
      // set only ocl buffer as up to date
      this->set_ocl_buffer_as_up_to_date();
      this->set_cuda_buffer_as_dirty();
    }


    std::vector<T> get_content()
    {
      this->update_ocl_buffer();
    
      return _ocl_buffer.get_content();
    }
  
  
  private:
  
    // ----------------------------------------------------------
    //   host memory access functions (manage dirty flags)
    // ----------------------------------------------------------
  
    // reads from host memory at position i and writes in val
    void read_from_host_memory( int i, T& val )
    {
      if( !_is_dirty_ocl_buffer )
        val = _ocl_buffer[ i ];
      
      else if( !_is_dirty_cuda_buffer )
        val = _cuda_buffer[ i ];

      else
      {
        assert( false && "should never be reached" );
        exit( EXIT_FAILURE );
      }
    }

    // writes val in host memory at position i
    void write_in_host_memory( int i, const T& val )
    {
      if( !_is_dirty_ocl_buffer )
        _ocl_buffer[ i ] = val;
      
      else if( !_is_dirty_cuda_buffer )
        _cuda_buffer[ i ] = val;

      else
      {
        assert( false && "should never be reached" );
        exit( EXIT_FAILURE );
      }
    }
  

    // ----------------------------------------------------------
    //   flag management
    // ----------------------------------------------------------
  
    void set_ocl_buffer_as_up_to_date()
    {
      _is_dirty_ocl_buffer = false;
    }
  
  
    void set_cuda_buffer_as_up_to_date()
    {
      _is_dirty_cuda_buffer = false;
    }

  
    void set_ocl_buffer_as_dirty()
    {
      _is_dirty_ocl_buffer = true;
    }

    void set_cuda_buffer_as_dirty()
    {
      _is_dirty_cuda_buffer = true;
    }


    // ----------------------------------------------------------
    //   buffer refreshing functions (manage dirty flags)
    // ----------------------------------------------------------

    void update_ocl_buffer()
    {
      // if ocl buffer is up to date then do nothing
      if( !_is_dirty_ocl_buffer )
        return;
     
      // if cuda buffer is up to date
      if( !_is_dirty_cuda_buffer )
      {
        _ocl_buffer.set_content( _cuda_buffer.get_host_memory_ptr() );
        
        this->set_ocl_buffer_as_up_to_date();
        
        return;
      }
      
      assert( false && "should never be reached" );
    }
  
  
    void update_cuda_buffer()
    {
      // if cuda buffer is up to date then do nothing
      if( !_is_dirty_cuda_buffer )
        return;
     
      // if opencl buffer is up to date
      if( !_is_dirty_ocl_buffer )
      {
        _cuda_buffer.set_content( _ocl_buffer.get_host_memory_ptr() );
        
        this->set_cuda_buffer_as_up_to_date();
        
        return;
      }
      
      assert( false && "should never be reached" );
    }
  
  
  private:
    size_t                        _size;
    core::ocl::unified_buffer<T>  _ocl_buffer;
    core::cuda::unified_buffer<T> _cuda_buffer;
    bool                          _is_dirty_ocl_buffer;
    bool                          _is_dirty_cuda_buffer;
};



template< typename T >
class local_buffer : public core::abstract::local_or_shared_buffer< T > // TODO: "shared_buffer" und "local_buffer" definieren für Unterscheidung zwischen CUDA und OpenCL
{
  // friend class parent
  friend class core::abstract::local_or_shared_buffer< T >;
  
  public:
    template< typename... Ts >
    local_buffer( Ts... args )
      : core::abstract::local_or_shared_buffer< T >( args... )
    {}
};



} // namespace "ocal"


#endif /* ocal_buffer_hpp */
