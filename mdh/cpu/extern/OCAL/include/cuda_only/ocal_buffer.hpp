//
//  ocal_buffer.hpp
//  ocal_ocal
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocal_buffer_hpp
#define ocal_buffer_hpp


namespace ocal
{


// buffer
template< typename T >
using buffer = core::cuda::buffer<T>;

//template< typename T >
//class buffer : public core::cuda::buffer< T >
//{
//  public:
//    template< typename... Ts >
//    buffer( const Ts&... args )
//      : core::cuda::buffer< T >( args... )
//    {}
//
//    // delete functions that serve implementation purposes
//    buffer( size_t size, T* content               )                          = delete; // Note: this ctor is only required to implement ocal::buffer in "ocal.hpp"
////    void set_host_memory_ptr( std::shared_ptr<T> new_content ) = delete;
////    T*   get_host_memory_ptr()                                 = delete;
//};


// host buffer
template< typename T >
using host_buffer = core::cuda::host_buffer<T>;

//template< typename T >
//class host_buffer : public core::cuda::host_buffer< T >
//{
//  public:
//    template< typename... Ts >
//    host_buffer( const Ts&... args )
//      : core::cuda::host_buffer< T >( args... )
//    {}
//};


// unified buffer
template< typename T >
using unified_buffer = core::cuda::unified_buffer<T>;

//template< typename T >
//class unified_buffer : public core::cuda::unified_buffer< T >
//{
//  public:
//    template< typename... Ts >
//    unified_buffer( const Ts&... args )
//      : core::cuda::unified_buffer< T >( args... )
//    {}
//};


//local buffer
template< typename T >
using shared_buffer = core::cuda::shared_buffer<T>;


} // namespace "ocal"

#endif /* ocal_buffer_hpp */
