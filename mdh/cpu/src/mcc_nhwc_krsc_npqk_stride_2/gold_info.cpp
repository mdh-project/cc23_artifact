void calculate_gold(const std::string &data_dir, int input_size_l_1, int input_size_l_2, int input_size_l_3, int input_size_l_4, int input_size_r_1, int input_size_r_2, int input_size_r_3) {
    std::ofstream gold_file(data_dir + "/gold.tsv", std::ios::out | std::ios::trunc);
    gold_file << std::fixed << std::setprecision(0);

    auto images_ptr = images.get_host_memory_ptr();
    auto filter_ptr = filter.get_host_memory_ptr();

    for (size_t n = 0; n < input_size_l_1; ++n) {
        for (size_t p = 0; p < input_size_l_2; ++p) {
            for (size_t q = 0; q < input_size_l_3; ++q) {
                for (size_t k = 0; k < input_size_l_4; ++k) {
                    float acc = 0;
                    for (size_t c = 0; c < input_size_r_1; ++c) {
                        for (size_t r = 0; r < input_size_r_2; ++r) {
                            for (size_t s = 0; s < input_size_r_3; ++s) {
                                acc += images_ptr[n           * (2 * input_size_l_2 - 1 + input_size_r_2 - 1) * (2 * input_size_l_3 - 1 + input_size_r_3 - 1) * input_size_r_1 +
                                                  (2 * p + r) * (2 * input_size_l_3 - 1 + input_size_r_3 - 1) * input_size_r_1 +
                                                  (2 * q + s) * input_size_r_1 +
                                                  c]
                                     * filter_ptr[k * input_size_r_2 * input_size_r_3 * input_size_r_1 +
                                                  r * input_size_r_3 * input_size_r_1 +
                                                  s * input_size_r_1 +
                                                  c];
                            }
                        }
                    }
                    gold_file << acc << "\t";
                }
                gold_file << std::endl;
            }
            gold_file << std::endl;
        }
        gold_file << std::endl;
    }
    gold_file.close();
}