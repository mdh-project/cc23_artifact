ocal::buffer<float> a;
ocal::buffer<float> b;
ocal::buffer<float> c;

void init_data(const std::vector<int> &input_size_l_1, const std::vector<int> &input_size_l_2, const std::vector<int> &input_size_r_1) {
    int max_a_size = 0;
    int max_b_size = 0;
    int max_c_size = 0;
    for (int i = 0; i < input_size_l_1.size(); ++i) {
        max_a_size = std::max(max_a_size, input_size_l_1[i] * input_size_r_1[i]);
        max_b_size = std::max(max_b_size, input_size_r_1[i] * input_size_l_2[i]);
        max_c_size = std::max(max_c_size, input_size_l_1[i] * input_size_l_2[i]);
    }
    a = ocal::buffer<float>(max_a_size); for (int i = 0; i < a.size(); ++i) a[i] = data_sequence<float>(i);
    b = ocal::buffer<float>(max_b_size); for (int i = 0; i < b.size(); ++i) b[i] = data_sequence<float>(i);
    c = ocal::buffer<float>(max_c_size); for (int i = 0; i < c.size(); ++i) c[i] = 0;
}

auto inputs(int case_nr) {
    return std::make_tuple(read(a), read(b));
}

auto outputs(int case_nr) {
    return std::make_tuple(write(c));
}