ocal::buffer<float> images;
ocal::buffer<float> filter;
ocal::buffer<float> out;

void init_data(const std::vector<int> &input_size_l_1, const std::vector<int> &input_size_l_2, const std::vector<int> &input_size_l_3, const std::vector<int> &input_size_l_4, const std::vector<int> &input_size_r_1, const std::vector<int> &input_size_r_2, const std::vector<int> &input_size_r_3) {
    int max_images_size = 0;
    int max_filter_size = 0;
    int max_out_size = 0;
    for (int i = 0; i < input_size_l_1.size(); ++i) {
        max_images_size = std::max(max_images_size, input_size_l_1[i] * (1 * input_size_l_2[i] - 0 + input_size_r_2[i] - 1) * (1 * input_size_l_3[i] - 0 + input_size_r_3[i] - 1) * input_size_r_1[i]);
        max_filter_size = std::max(max_filter_size, input_size_l_4[i] * input_size_r_2[i] * input_size_r_3[i] * input_size_r_1[i]);
        max_out_size = std::max(max_out_size, input_size_l_1[i] * input_size_l_2[i] * input_size_l_3[i] * input_size_l_4[i]);
    }
    images = ocal::buffer<float>(max_images_size); for (int i = 0; i < images.size(); ++i) images[i] = data_sequence<float>(i);
    filter = ocal::buffer<float>(max_filter_size); for (int i = 0; i < filter.size(); ++i) filter[i] = data_sequence<float>(i);
    out = ocal::buffer<float>(max_out_size); for (int i = 0; i < out.size(); ++i) out[i] = 0;
}

auto inputs(int case_nr) {
    return std::make_tuple(read(images), read(filter));
}

auto outputs(int case_nr) {
    return std::make_tuple(write(out));
}