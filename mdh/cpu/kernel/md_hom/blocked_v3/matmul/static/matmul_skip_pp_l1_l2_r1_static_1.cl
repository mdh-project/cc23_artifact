#include "helper_ocl.h"

#define C_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,              \
              wg_2, glb_2, wi_2, lcl_2, prv_2)              \
        ARRAY_2D_FLAT(c_glb,                                \
                      INPUT_SIZE_1                        , \
                      wg_1 * GLB_1 * WI_1 * LCL_1 * PRV_1 + \
                             glb_1 * WI_1 * LCL_1 * PRV_1 + \
                                     wi_1 * LCL_1 * PRV_1 + \
                                            lcl_1 * PRV_1 + \
                                                    prv_1 , \
                      INPUT_SIZE_2                        , \
                      wg_2 * GLB_2 * WI_2 * LCL_2 * PRV_2 + \
                             glb_2 * WI_2 * LCL_2 * PRV_2 + \
                                     wi_2 * LCL_2 * PRV_2 + \
                                            lcl_2 * PRV_2 + \
                                                    prv_2 )

#define C_PRV(lcl_1, prv_1,           \
              lcl_2, prv_2)           \
        ARRAY_2D_FLAT(c_prv,          \
                      LCL_1 * PRV_1 , \
                      lcl_1 * PRV_1 + \
                              prv_1 , \
                      LCL_2 * PRV_2 , \
                      lcl_2 * PRV_2 + \
                              prv_2 )

#define A_GLB(wg_1,  glb_1,  wi_1,  lcl_1,  prv_1,          \
              wg_3,  glb_3,  wi_3,  lcl_3,  prv_3)          \
        ARRAY_2D_FLAT(a_glb,                                \
                      INPUT_SIZE_1                        , \
                      wg_1 * GLB_1 * WI_1 * LCL_1 * PRV_1 + \
                             glb_1 * WI_1 * LCL_1 * PRV_1 + \
                                     wi_1 * LCL_1 * PRV_1 + \
                                            lcl_1 * PRV_1 + \
                                                    prv_1 , \
                      INPUT_SIZE_3                        , \
                      wg_3 * GLB_3 * WI_3 * LCL_3 * PRV_3 + \
                             glb_3 * WI_3 * LCL_3 * PRV_3 + \
                                     wi_3 * LCL_3 * PRV_3 + \
                                            lcl_3 * PRV_3 + \
                                                    prv_3 )

#define A_LCL(wi_1,  lcl_1,  prv_1,          \
              wi_3,  lcl_3,  prv_3)          \
        ARRAY_2D_FLAT(a_lcl,                 \
                      WI_1 * LCL_1 * PRV_1 , \
                      wi_1 * LCL_1 * PRV_1 + \
                             lcl_1 * PRV_1 + \
                                     prv_1 , \
                      WI_3 * LCL_3 * PRV_3 , \
                      wi_3 * LCL_3 * PRV_3 + \
                             lcl_3 * PRV_3 + \
                                     prv_3 )

#define A_PRV(prv_1,          \
              prv_2)          \
        ARRAY_2D_FLAT(a_prv,  \
                      PRV_1 , \
                      prv_1 , \
                      PRV_3 , \
                      prv_3 )

#define B_GLB(wg_2,  glb_2,  wi_2,  lcl_2,  prv_2,          \
              wg_3,  glb_3,  wi_3,  lcl_3,  prv_3)          \
        ARRAY_2D_FLAT(b_glb,                                \
                      INPUT_SIZE_3                        , \
                      wg_3 * GLB_3 * WI_3 * LCL_3 * PRV_3 + \
                             glb_3 * WI_3 * LCL_3 * PRV_3 + \
                                     wi_3 * LCL_3 * PRV_3 + \
                                            lcl_3 * PRV_3 + \
                                                    prv_3 , \
                      INPUT_SIZE_2                        , \
                      wg_2 * GLB_2 * WI_2 * LCL_2 * PRV_2 + \
                             glb_2 * WI_2 * LCL_2 * PRV_2 + \
                                     wi_2 * LCL_2 * PRV_2 + \
                                            lcl_2 * PRV_2 + \
                                                    prv_2 )

#define B_LCL(wi_2,  lcl_2,  prv_2,          \
              wi_3,  lcl_3,  prv_3)          \
        ARRAY_2D_FLAT(b_lcl,                 \
                      WI_3 * LCL_3 * PRV_3 , \
                      wi_3 * LCL_3 * PRV_3 + \
                             lcl_3 * PRV_3 + \
                                     prv_3 , \
                      WI_2 * LCL_2 * PRV_2 , \
                      wi_2 * LCL_2 * PRV_2 + \
                             lcl_2 * PRV_2 + \
                                     prv_2 )

#define B_PRV(prv_2,          \
              prv_3)          \
        ARRAY_2D_FLAT(b_prv,  \
                      PRV_3 , \
                      prv_3 , \
                      PRV_2 , \
                      prv_2 )

KERNEL_QUALIFIER void matmul_static_1(
        GLB_MEM_QUALIFIER float const * const __restrict__ a_glb,
        GLB_MEM_QUALIFIER float const * const __restrict__ b_glb,
        GLB_MEM_QUALIFIER float * const __restrict__ _,
        GLB_MEM_QUALIFIER float * const __restrict__ c_glb) {
    #if C_CACHE_PRV == 1
    PRV_MEM_QUALIFIER float c_prv[(LCL_1 * PRV_1) *
                                  (LCL_2 * PRV_2)];
    #endif

    #if A_CACHE_LCL == 1
    LCL_MEM_QUALIFIER float a_lcl[(WI_1 * LCL_1 * PRV_1) *
                                  (WI_3 * LCL_3 * PRV_3)];
    #endif
    #if A_CACHE_PRV == 1
    PRV_MEM_QUALIFIER float a_prv[(PRV_1) *
                                  (PRV_3)];
    #endif

    #if B_CACHE_LCL == 1
    LCL_MEM_QUALIFIER float b_lcl[(WI_3 * LCL_3 * PRV_3) *
                                  (WI_2 * LCL_2 * PRV_2)];
    #endif
    #if B_CACHE_PRV == 1
    PRV_MEM_QUALIFIER float b_prv[(PRV_3) *
                                  (PRV_2)];
    #endif

    LOOP_WG_1 {
    LOOP_WG_2 {
    LOOP_WG_3 {

    LOOP_WI_1 {
    LOOP_WI_2 {
    LOOP_WI_3 {

    LOOP_GLB_1 {
    LOOP_GLB_2 {

    { // init prv
    LOOP_LCL_1 {
    LOOP_LCL_2 {
    LOOP_PRV_1 {
    LOOP_PRV_2 {
        #if C_CACHE_PRV == 1
        C_PRV(lcl_1, prv_1,
              lcl_2, prv_2)
        #else
        C_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
              wg_2, glb_2, wi_2, lcl_2, prv_2)
        #endif
            = 0.0f;
    }}}}
    }

    LOOP_GLB_3 {

        // copy wg -> wi
        #if A_CACHE_LCL == 1
        {
            BARRIER
        #if 1
            #define CACHE_BLOCK_OFFSET_1 (wg_1  * GLB_1  * WI_1  * LCL_1  * PRV_1  + glb_1  * WI_1  * LCL_1  * PRV_1 )
            #define CACHE_BLOCK_OFFSET_2 (wg_3  * GLB_3  * WI_3  * LCL_3  * PRV_3  + glb_3  * WI_3  * LCL_3  * PRV_3 )
            #define CACHE_BLOCK_SIZE_1 (WI_1  * LCL_1  * PRV_1 )
            #define CACHE_BLOCK_SIZE_2 (WI_3  * LCL_3  * PRV_3 )
            #define CACHE_BLOCK_SIZE (CACHE_BLOCK_SIZE_1 * CACHE_BLOCK_SIZE_2)
            #if INPUT_SIZE_3 % 4 == 0 && CACHE_BLOCK_OFFSET_2 % 4 == 0 && CACHE_BLOCK_SIZE_2 % 4 == 0
            #define VECTOR_LOAD_SIZE 4
            #define VECTOR_LOAD_TYPE float4
            #elif INPUT_SIZE_3 % 2 == 0 && CACHE_BLOCK_OFFSET_2 % 2 == 0 && CACHE_BLOCK_SIZE_2 % 2 == 0
            #define VECTOR_LOAD_SIZE 2
            #define VECTOR_LOAD_TYPE float2
            #else
            #define VECTOR_LOAD_SIZE 1
            #define VECTOR_LOAD_TYPE float
            #endif
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE > 0
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE == 1
            #define step 0
            #else
            #pragma unroll
            for (size_t step = 0; step < (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE; ++step)
            #endif
            {
                ((LCL_MEM_QUALIFIER VECTOR_LOAD_TYPE*)a_lcl)[step * FLAT_WI_SIZE + FLAT_WI_IDX] =
                ARRAY_2D_FLAT(((GLB_MEM_QUALIFIER VECTOR_LOAD_TYPE*)a_glb),
                              INPUT_SIZE_1,
                              CACHE_BLOCK_OFFSET_1 + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_2 / VECTOR_LOAD_SIZE) % (CACHE_BLOCK_SIZE_1)),
                              (INPUT_SIZE_3 / VECTOR_LOAD_SIZE),
                              (CACHE_BLOCK_OFFSET_2 / VECTOR_LOAD_SIZE) + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) % ((CACHE_BLOCK_SIZE_2 / VECTOR_LOAD_SIZE))));
            }
            #endif
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) % FLAT_WI_SIZE != 0
            if (FLAT_WI_IDX < ((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) % FLAT_WI_SIZE)) {
                ((LCL_MEM_QUALIFIER VECTOR_LOAD_TYPE*)a_lcl)[((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX] =
                ARRAY_2D_FLAT(((GLB_MEM_QUALIFIER VECTOR_LOAD_TYPE*)a_glb),
                              INPUT_SIZE_1,
                              CACHE_BLOCK_OFFSET_1 + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_2 / VECTOR_LOAD_SIZE) % (CACHE_BLOCK_SIZE_1)),
                              (INPUT_SIZE_3 / VECTOR_LOAD_SIZE),
                              (CACHE_BLOCK_OFFSET_2 / VECTOR_LOAD_SIZE) + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) % ((CACHE_BLOCK_SIZE_2 / VECTOR_LOAD_SIZE))));
            }
            #endif

            #ifdef step
            #undef step
            #endif
            #undef VECTOR_LOAD_TYPE
            #undef VECTOR_LOAD_SIZE
            #undef CACHE_BLOCK_SIZE
            #undef CACHE_BLOCK_SIZE_2
            #undef CACHE_BLOCK_SIZE_1
            #undef CACHE_BLOCK_OFFSET_2
            #undef CACHE_BLOCK_OFFSET_1
        #else
            LOOP_LCL_1
            LOOP_LCL_3
            LOOP_PRV_1
            LOOP_PRV_3
                A_LCL(wi_1, lcl_1, prv_1,
                      wi_3, lcl_3, prv_3) =
                A_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
                      wg_3, glb_3, wi_3, lcl_3, prv_3);
        #endif
        }
        #endif
        #if B_CACHE_LCL == 1
        {
            BARRIER
        #if 1
            #define CACHE_BLOCK_OFFSET_1 (wg_3  * GLB_3  * WI_3  * LCL_3  * PRV_3  + glb_3  * WI_3  * LCL_3  * PRV_3 )
            #define CACHE_BLOCK_OFFSET_2 (wg_2  * GLB_2  * WI_2  * LCL_2  * PRV_2  + glb_2  * WI_2  * LCL_2  * PRV_2 )
            #define CACHE_BLOCK_SIZE_1 (WI_3  * LCL_3  * PRV_3 )
            #define CACHE_BLOCK_SIZE_2 (WI_2  * LCL_2  * PRV_2 )
            #define CACHE_BLOCK_SIZE (CACHE_BLOCK_SIZE_1 * CACHE_BLOCK_SIZE_2)
            #if INPUT_SIZE_2 % 4 == 0 && CACHE_BLOCK_OFFSET_2 % 4 == 0 && CACHE_BLOCK_SIZE_2 % 4 == 0
            #define VECTOR_LOAD_SIZE 4
            #define VECTOR_LOAD_TYPE float4
            #elif INPUT_SIZE_2 % 2 == 0 && CACHE_BLOCK_OFFSET_2 % 2 == 0 && CACHE_BLOCK_SIZE_2 % 2 == 0
            #define VECTOR_LOAD_SIZE 2
            #define VECTOR_LOAD_TYPE float2
            #else
            #define VECTOR_LOAD_SIZE 1
            #define VECTOR_LOAD_TYPE float
            #endif
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE > 0
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE == 1
            #define step 0
            #else
            #pragma unroll
            for (size_t step = 0; step < (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE; ++step)
            #endif
            {
                ((LCL_MEM_QUALIFIER VECTOR_LOAD_TYPE*)b_lcl)[step * FLAT_WI_SIZE + FLAT_WI_IDX] =
                ARRAY_2D_FLAT(((GLB_MEM_QUALIFIER VECTOR_LOAD_TYPE*)b_glb),
                              INPUT_SIZE_3,
                              CACHE_BLOCK_OFFSET_1 + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_2 / VECTOR_LOAD_SIZE) % (CACHE_BLOCK_SIZE_1)),
                              (INPUT_SIZE_2 / VECTOR_LOAD_SIZE),
                              (CACHE_BLOCK_OFFSET_2 / VECTOR_LOAD_SIZE) + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) % ((CACHE_BLOCK_SIZE_2 / VECTOR_LOAD_SIZE))));
            }
            #endif
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) % FLAT_WI_SIZE != 0
            if (FLAT_WI_IDX < ((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) % FLAT_WI_SIZE)) {
                ((LCL_MEM_QUALIFIER VECTOR_LOAD_TYPE*)b_lcl)[((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX] =
                ARRAY_2D_FLAT(((GLB_MEM_QUALIFIER VECTOR_LOAD_TYPE*)b_glb),
                              INPUT_SIZE_3,
                              CACHE_BLOCK_OFFSET_1 + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_2 / VECTOR_LOAD_SIZE) % (CACHE_BLOCK_SIZE_1)),
                              (INPUT_SIZE_2 / VECTOR_LOAD_SIZE),
                              (CACHE_BLOCK_OFFSET_2 / VECTOR_LOAD_SIZE) + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) % ((CACHE_BLOCK_SIZE_2 / VECTOR_LOAD_SIZE))));
            }
            #endif

            #ifdef step
            #undef step
            #endif
            #undef VECTOR_LOAD_TYPE
            #undef VECTOR_LOAD_SIZE
            #undef CACHE_BLOCK_SIZE
            #undef CACHE_BLOCK_SIZE_2
            #undef CACHE_BLOCK_SIZE_1
            #undef CACHE_BLOCK_OFFSET_2
            #undef CACHE_BLOCK_OFFSET_1
        #else
            LOOP_LCL_2
            LOOP_LCL_3
            LOOP_PRV_2
            LOOP_PRV_3
                B_LCL(wi_2, lcl_2, prv_2,
                      wi_3, lcl_3, prv_3) =
                B_GLB(wg_2, glb_2, wi_2, lcl_2, prv_2,
                      wg_3, glb_3, wi_3, lcl_3, prv_3);
        #endif
        }
        #endif
        #if A_CACHE_LCL == 1 || B_CACHE_LCL == 1
        BARRIER
        #endif

        LOOP_LCL_3 {
        LOOP_LCL_1 {
        LOOP_LCL_2 {

            // copy wi -> prv
            #if A_CACHE_PRV == 1
            {
                LOOP_PRV_1
                LOOP_PRV_3
                    A_PRV(prv_1,
                          prv_3) =
                    #if A_CACHE_LCL == 1
                    A_LCL(wi_1, lcl_1, prv_1,
                          wi_3, lcl_3, prv_3)
                    #else
                    A_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
                          wg_3, glb_3, wi_3, lcl_3, prv_3)
                    #endif
                        ;
            }
            #endif
            #if B_CACHE_PRV == 1
            {
                LOOP_PRV_2
                LOOP_PRV_3
                    B_PRV(prv_2,
                          prv_3) =
                    #if B_CACHE_LCL == 1
                    B_LCL(wi_2, lcl_2, prv_2,
                          wi_3, lcl_3, prv_3)
                    #else
                    B_GLB(wg_2, glb_2, wi_2, lcl_2, prv_2,
                          wg_3, glb_3, wi_3, lcl_3, prv_3)
                    #endif
                        ;
            }
            #endif

            LOOP_PRV_3 {
            LOOP_PRV_1 {
            LOOP_PRV_2 {

            // scalar phase
            #if C_CACHE_PRV == 1
            C_PRV(lcl_1, prv_1,
                  lcl_2, prv_2)
            #else
            C_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
                  wg_2, glb_2, wi_2, lcl_2, prv_2)
            #endif
                +=
            #if A_CACHE_PRV == 1
            A_PRV(prv_1,
                  prv_3)
            #elif A_CACHE_LCL == 1
            A_LCL(wi_1, lcl_1, prv_1,
                  wi_3, lcl_3, prv_3)
            #else
            A_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
                  wg_3, glb_3, wi_3, lcl_3, prv_3)
            #endif
            *
            #if B_CACHE_PRV == 1
            B_PRV(prv_2,
                  prv_3)
            #elif B_CACHE_LCL == 1
            B_LCL(wi_2, lcl_2, prv_2,
                  wi_3, lcl_3, prv_3)
            #else
            B_GLB(wg_2, glb_2, wi_2, lcl_2, prv_2,
                  wg_3, glb_3, wi_3, lcl_3, prv_3)
            #endif
            ;

            }}} // PRV
        }}} // LCL
    } // GLB (+)

    #if C_CACHE_PRV == 1
    { // copy prv -> wg
    LOOP_LCL_1 {
    LOOP_LCL_2 {
    LOOP_PRV_1 {
    LOOP_PRV_2 {
        C_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
              wg_2, glb_2, wi_2, lcl_2, prv_2) =
        C_PRV(lcl_1, prv_1,
              lcl_2, prv_2);
    }}}}
    }
    #endif

    }} // GLB (++)
    }}} // WI
    }}} // WG
} // kernel