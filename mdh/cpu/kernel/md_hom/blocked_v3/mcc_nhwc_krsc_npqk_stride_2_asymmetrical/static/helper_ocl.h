#define KERNEL_QUALIFIER __kernel
#define GLB_MEM_QUALIFIER __global
#define LCL_MEM_QUALIFIER __local
#define PRV_MEM_QUALIFIER __private
#define BARRIER barrier(CLK_LOCAL_MEM_FENCE);

#define ARRAY_4D_FLAT(mem, size_1, idx_1, size_2, idx_2, size_3, idx_3, size_4, idx_4) \
                      mem[(idx_1) * (size_2) * (size_3) * (size_4) +                   \
                                     (idx_2) * (size_3) * (size_4) +                   \
                                                (idx_3) * (size_4) +                   \
                                                           (idx_4) ]

#if GLB_1 == 1
#define LOOP_GLB_1 /* #define glb_1 0 */
#define glb_1 0
#else
#define LOOP_GLB_1 for (size_t glb_1 = 0; glb_1 < GLB_1; ++glb_1)
#endif
#if GLB_2 == 1
#define LOOP_GLB_2 /* #define glb_2 0 */
#define glb_2 0
#else
#define LOOP_GLB_2 for (size_t glb_2 = 0; glb_2 < GLB_2; ++glb_2)
#endif
#if GLB_3 == 1
#define LOOP_GLB_3 /* #define glb_3 0 */
#define glb_3 0
#else
#define LOOP_GLB_3 for (size_t glb_3 = 0; glb_3 < GLB_3; ++glb_3)
#endif
#if GLB_4 == 1
#define LOOP_GLB_4 /* #define glb_4 0 */
#define glb_4 0
#else
#define LOOP_GLB_4 for (size_t glb_4 = 0; glb_4 < GLB_4; ++glb_4)
#endif
#if GLB_5 == 1
#define LOOP_GLB_5 /* #define glb_5 0 */
#define glb_5 0
#else
#define LOOP_GLB_5 for (size_t glb_5 = 0; glb_5 < GLB_5; ++glb_5)
#endif
#if GLB_6 == 1
#define LOOP_GLB_6 /* #define glb_6 0 */
#define glb_6 0
#else
#define LOOP_GLB_6 for (size_t glb_6 = 0; glb_6 < GLB_6; ++glb_6)
#endif
#if GLB_7 == 1
#define LOOP_GLB_7 /* #define glb_7 0 */
#define glb_7 0
#else
#define LOOP_GLB_7 for (size_t glb_7 = 0; glb_7 < GLB_7; ++glb_7)
#endif

#if LCL_1 == 1
#define LOOP_LCL_1 /* #define lcl_1 0 */
#define lcl_1 0
#else
#define LOOP_LCL_1 for (size_t lcl_1 = 0; lcl_1 < LCL_1; ++lcl_1)
#endif
#if LCL_2 == 1
#define LOOP_LCL_2 /* #define lcl_2 0 */
#define lcl_2 0
#else
#define LOOP_LCL_2 for (size_t lcl_2 = 0; lcl_2 < LCL_2; ++lcl_2)
#endif
#if LCL_3 == 1
#define LOOP_LCL_3 /* #define lcl_3 0 */
#define lcl_3 0
#else
#define LOOP_LCL_3 for (size_t lcl_3 = 0; lcl_3 < LCL_3; ++lcl_3)
#endif
#if LCL_4 == 1
#define LOOP_LCL_4 /* #define lcl_4 0 */
#define lcl_4 0
#else
#define LOOP_LCL_4 for (size_t lcl_4 = 0; lcl_4 < LCL_4; ++lcl_4)
#endif
#if LCL_5 == 1
#define LOOP_LCL_5 /* #define lcl_5 0 */
#define lcl_5 0
#else
#define LOOP_LCL_5 for (size_t lcl_5 = 0; lcl_5 < LCL_5; ++lcl_5)
#endif
#if LCL_6 == 1
#define LOOP_LCL_6 /* #define lcl_6 0 */
#define lcl_6 0
#else
#define LOOP_LCL_6 for (size_t lcl_6 = 0; lcl_6 < LCL_6; ++lcl_6)
#endif
#if LCL_7 == 1
#define LOOP_LCL_7 /* #define lcl_7 0 */
#define lcl_7 0
#else
#define LOOP_LCL_7 for (size_t lcl_7 = 0; lcl_7 < LCL_7; ++lcl_7)
#endif

#if PRV_1 == 1
#define LOOP_PRV_1 /* #define prv_1 0 */
#define prv_1 0
#else
#define LOOP_PRV_1 for (size_t prv_1 = 0; prv_1 < PRV_1; ++prv_1)
#endif
#if PRV_2 == 1
#define LOOP_PRV_2 /* #define prv_2 0 */
#define prv_2 0
#else
#define LOOP_PRV_2 for (size_t prv_2 = 0; prv_2 < PRV_2; ++prv_2)
#endif
#if PRV_3 == 1
#define LOOP_PRV_3 /* #define prv_3 0 */
#define prv_3 0
#else
#define LOOP_PRV_3 for (size_t prv_3 = 0; prv_3 < PRV_3; ++prv_3)
#endif
#if PRV_4 == 1
#define LOOP_PRV_4 /* #define prv_4 0 */
#define prv_4 0
#else
#define LOOP_PRV_4 for (size_t prv_4 = 0; prv_4 < PRV_4; ++prv_4)
#endif
#if PRV_5 == 1
#define LOOP_PRV_5 /* #define prv_5 0 */
#define prv_5 0
#else
#define LOOP_PRV_5 for (size_t prv_5 = 0; prv_5 < PRV_5; ++prv_5)
#endif
#if PRV_6 == 1
#define LOOP_PRV_6 /* #define prv_6 0 */
#define prv_6 0
#else
#define LOOP_PRV_6 for (size_t prv_6 = 0; prv_6 < PRV_6; ++prv_6)
#endif
#if PRV_7 == 1
#define LOOP_PRV_7 /* #define prv_7 0 */
#define prv_7 0
#else
#define LOOP_PRV_7 for (size_t prv_7 = 0; prv_7 < PRV_7; ++prv_7)
#endif

#if WG_1_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_1
#elif WG_1_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_1
#elif WG_1_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_1
#elif WG_1_OCL_DIM == 3
#define WG_OCL_DIM_3 WG_1
#elif WG_1_OCL_DIM == 4
#define WG_OCL_DIM_4 WG_1
#elif WG_1_OCL_DIM == 5
#define WG_OCL_DIM_5 WG_1
#elif WG_1_OCL_DIM == 6
#define WG_OCL_DIM_6 WG_1
#endif

#if WG_2_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_2
#elif WG_2_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_2
#elif WG_2_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_2
#elif WG_2_OCL_DIM == 3
#define WG_OCL_DIM_3 WG_2
#elif WG_2_OCL_DIM == 4
#define WG_OCL_DIM_4 WG_2
#elif WG_2_OCL_DIM == 5
#define WG_OCL_DIM_5 WG_2
#elif WG_2_OCL_DIM == 6
#define WG_OCL_DIM_6 WG_2
#endif

#if WG_3_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_3
#elif WG_3_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_3
#elif WG_3_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_3
#elif WG_3_OCL_DIM == 3
#define WG_OCL_DIM_3 WG_3
#elif WG_3_OCL_DIM == 4
#define WG_OCL_DIM_4 WG_3
#elif WG_3_OCL_DIM == 5
#define WG_OCL_DIM_5 WG_3
#elif WG_3_OCL_DIM == 6
#define WG_OCL_DIM_6 WG_3
#endif

#if WG_4_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_4
#elif WG_4_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_4
#elif WG_4_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_4
#elif WG_4_OCL_DIM == 3
#define WG_OCL_DIM_3 WG_4
#elif WG_4_OCL_DIM == 4
#define WG_OCL_DIM_4 WG_4
#elif WG_4_OCL_DIM == 5
#define WG_OCL_DIM_5 WG_4
#elif WG_4_OCL_DIM == 6
#define WG_OCL_DIM_6 WG_4
#endif

#if WG_5_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_5
#elif WG_5_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_5
#elif WG_5_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_5
#elif WG_5_OCL_DIM == 3
#define WG_OCL_DIM_3 WG_5
#elif WG_5_OCL_DIM == 4
#define WG_OCL_DIM_4 WG_5
#elif WG_5_OCL_DIM == 5
#define WG_OCL_DIM_5 WG_5
#elif WG_5_OCL_DIM == 6
#define WG_OCL_DIM_6 WG_5
#endif

#if WG_6_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_6
#elif WG_6_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_6
#elif WG_6_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_6
#elif WG_6_OCL_DIM == 3
#define WG_OCL_DIM_3 WG_6
#elif WG_6_OCL_DIM == 4
#define WG_OCL_DIM_4 WG_6
#elif WG_6_OCL_DIM == 5
#define WG_OCL_DIM_5 WG_6
#elif WG_6_OCL_DIM == 6
#define WG_OCL_DIM_6 WG_6
#endif

#if WG_7_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_7
#elif WG_7_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_7
#elif WG_7_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_7
#elif WG_7_OCL_DIM == 3
#define WG_OCL_DIM_3 WG_7
#elif WG_7_OCL_DIM == 4
#define WG_OCL_DIM_4 WG_7
#elif WG_7_OCL_DIM == 5
#define WG_OCL_DIM_5 WG_7
#elif WG_7_OCL_DIM == 6
#define WG_OCL_DIM_6 WG_7
#endif

#if WG_1 == 1
    #define LOOP_WG_1 /* #define wg_1 0 */
    #define wg_1 0
#else
    #if WG_1_OCL_DIM == 0
    #define LOOP_WG_1 const size_t wg_1 = get_group_id(0) % (WG_OCL_DIM_0);
    #elif WG_1_OCL_DIM == 1
    #define LOOP_WG_1 const size_t wg_1 = get_group_id(0) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_1_OCL_DIM == 2
    #define LOOP_WG_1 const size_t wg_1 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1) % (WG_OCL_DIM_2);
    #elif WG_1_OCL_DIM == 3
    #define LOOP_WG_1 const size_t wg_1 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2) % (WG_OCL_DIM_3);
    #elif WG_1_OCL_DIM == 4
    #define LOOP_WG_1 const size_t wg_1 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3) % (WG_OCL_DIM_4);
    #elif WG_1_OCL_DIM == 5
    #define LOOP_WG_1 const size_t wg_1 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4) % (WG_OCL_DIM_5);
    #elif WG_1_OCL_DIM == 6
    #define LOOP_WG_1 const size_t wg_1 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4 * WG_OCL_DIM_5);
    #endif
#endif

#if WG_2 == 1
    #define LOOP_WG_2 /* #define wg_2 0 */
    #define wg_2 0
#else
    #if WG_2_OCL_DIM == 0
    #define LOOP_WG_2 const size_t wg_2 = get_group_id(0) % (WG_OCL_DIM_0);
    #elif WG_2_OCL_DIM == 1
    #define LOOP_WG_2 const size_t wg_2 = get_group_id(0) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_2_OCL_DIM == 2
    #define LOOP_WG_2 const size_t wg_2 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1) % (WG_OCL_DIM_2);
    #elif WG_2_OCL_DIM == 3
    #define LOOP_WG_2 const size_t wg_2 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2) % (WG_OCL_DIM_3);
    #elif WG_2_OCL_DIM == 4
    #define LOOP_WG_2 const size_t wg_2 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3) % (WG_OCL_DIM_4);
    #elif WG_2_OCL_DIM == 5
    #define LOOP_WG_2 const size_t wg_2 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4) % (WG_OCL_DIM_5);
    #elif WG_2_OCL_DIM == 6
    #define LOOP_WG_2 const size_t wg_2 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4 * WG_OCL_DIM_5);
    #endif
#endif

#if WG_3 == 1
    #define LOOP_WG_3 /* #define wg_3 0 */
    #define wg_3 0
#else
    #if WG_3_OCL_DIM == 0
    #define LOOP_WG_3 const size_t wg_3 = get_group_id(0) % (WG_OCL_DIM_0);
    #elif WG_3_OCL_DIM == 1
    #define LOOP_WG_3 const size_t wg_3 = get_group_id(0) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_3_OCL_DIM == 2
    #define LOOP_WG_3 const size_t wg_3 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1) % (WG_OCL_DIM_2);
    #elif WG_3_OCL_DIM == 3
    #define LOOP_WG_3 const size_t wg_3 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2) % (WG_OCL_DIM_3);
    #elif WG_3_OCL_DIM == 4
    #define LOOP_WG_3 const size_t wg_3 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3) % (WG_OCL_DIM_4);
    #elif WG_3_OCL_DIM == 5
    #define LOOP_WG_3 const size_t wg_3 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4) % (WG_OCL_DIM_5);
    #elif WG_3_OCL_DIM == 6
    #define LOOP_WG_3 const size_t wg_3 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4 * WG_OCL_DIM_5);
    #endif
#endif

#if WG_4 == 1
    #define LOOP_WG_4 /* #define wg_4 0 */
    #define wg_4 0
#else
    #if WG_4_OCL_DIM == 0
    #define LOOP_WG_4 const size_t wg_4 = get_group_id(0) % (WG_OCL_DIM_0);
    #elif WG_4_OCL_DIM == 1
    #define LOOP_WG_4 const size_t wg_4 = get_group_id(0) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_4_OCL_DIM == 2
    #define LOOP_WG_4 const size_t wg_4 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1) % (WG_OCL_DIM_2);
    #elif WG_4_OCL_DIM == 3
    #define LOOP_WG_4 const size_t wg_4 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2) % (WG_OCL_DIM_3);
    #elif WG_4_OCL_DIM == 4
    #define LOOP_WG_4 const size_t wg_4 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3) % (WG_OCL_DIM_4);
    #elif WG_4_OCL_DIM == 5
    #define LOOP_WG_4 const size_t wg_4 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4) % (WG_OCL_DIM_5);
    #elif WG_4_OCL_DIM == 6
    #define LOOP_WG_4 const size_t wg_4 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4 * WG_OCL_DIM_5);
    #endif
#endif

#if WG_5 == 1
    #define LOOP_WG_5 /* #define wg_5 0 */
    #define wg_5 0
#else
    #if WG_5_OCL_DIM == 0
    #define LOOP_WG_5 const size_t wg_5 = get_group_id(0) % (WG_OCL_DIM_0);
    #elif WG_5_OCL_DIM == 1
    #define LOOP_WG_5 const size_t wg_5 = get_group_id(0) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_5_OCL_DIM == 2
    #define LOOP_WG_5 const size_t wg_5 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1) % (WG_OCL_DIM_2);
    #elif WG_5_OCL_DIM == 3
    #define LOOP_WG_5 const size_t wg_5 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2) % (WG_OCL_DIM_3);
    #elif WG_5_OCL_DIM == 4
    #define LOOP_WG_5 const size_t wg_5 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3) % (WG_OCL_DIM_4);
    #elif WG_5_OCL_DIM == 5
    #define LOOP_WG_5 const size_t wg_5 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4) % (WG_OCL_DIM_5);
    #elif WG_5_OCL_DIM == 6
    #define LOOP_WG_5 const size_t wg_5 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4 * WG_OCL_DIM_5);
    #endif
#endif

#if WG_6 == 1
    #define LOOP_WG_6 /* #define wg_6 0 */
    #define wg_6 0
#else
    #if WG_6_OCL_DIM == 0
    #define LOOP_WG_6 const size_t wg_6 = get_group_id(0) % (WG_OCL_DIM_0);
    #elif WG_6_OCL_DIM == 1
    #define LOOP_WG_6 const size_t wg_6 = get_group_id(0) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_6_OCL_DIM == 2
    #define LOOP_WG_6 const size_t wg_6 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1) % (WG_OCL_DIM_2);
    #elif WG_6_OCL_DIM == 3
    #define LOOP_WG_6 const size_t wg_6 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2) % (WG_OCL_DIM_3);
    #elif WG_6_OCL_DIM == 4
    #define LOOP_WG_6 const size_t wg_6 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3) % (WG_OCL_DIM_4);
    #elif WG_6_OCL_DIM == 5
    #define LOOP_WG_6 const size_t wg_6 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4) % (WG_OCL_DIM_5);
    #elif WG_6_OCL_DIM == 6
    #define LOOP_WG_6 const size_t wg_6 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4 * WG_OCL_DIM_5);
    #endif
#endif

#if WG_7 == 1
    #define LOOP_WG_7 /* #define wg_7 0 */
    #define wg_7 0
#else
    #if WG_7_OCL_DIM == 0
    #define LOOP_WG_7 const size_t wg_7 = get_group_id(0) % (WG_OCL_DIM_0);
    #elif WG_7_OCL_DIM == 1
    #define LOOP_WG_7 const size_t wg_7 = get_group_id(0) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_7_OCL_DIM == 2
    #define LOOP_WG_7 const size_t wg_7 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1) % (WG_OCL_DIM_2);
    #elif WG_7_OCL_DIM == 3
    #define LOOP_WG_7 const size_t wg_7 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2) % (WG_OCL_DIM_3);
    #elif WG_7_OCL_DIM == 4
    #define LOOP_WG_7 const size_t wg_7 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3) % (WG_OCL_DIM_4);
    #elif WG_7_OCL_DIM == 5
    #define LOOP_WG_7 const size_t wg_7 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4) % (WG_OCL_DIM_5);
    #elif WG_7_OCL_DIM == 6
    #define LOOP_WG_7 const size_t wg_7 = get_group_id(0) / (WG_OCL_DIM_0 * WG_OCL_DIM_1 * WG_OCL_DIM_2 * WG_OCL_DIM_3 * WG_OCL_DIM_4 * WG_OCL_DIM_5);
    #endif
#endif

#if WI_1_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_1
#elif WI_1_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_1
#elif WI_1_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_1
#elif WI_1_OCL_DIM == 3
#define WI_OCL_DIM_3 WI_1
#elif WI_1_OCL_DIM == 4
#define WI_OCL_DIM_4 WI_1
#elif WI_1_OCL_DIM == 5
#define WI_OCL_DIM_5 WI_1
#elif WI_1_OCL_DIM == 6
#define WI_OCL_DIM_6 WI_1
#endif

#if WI_2_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_2
#elif WI_2_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_2
#elif WI_2_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_2
#elif WI_2_OCL_DIM == 3
#define WI_OCL_DIM_3 WI_2
#elif WI_2_OCL_DIM == 4
#define WI_OCL_DIM_4 WI_2
#elif WI_2_OCL_DIM == 5
#define WI_OCL_DIM_5 WI_2
#elif WI_2_OCL_DIM == 6
#define WI_OCL_DIM_6 WI_2
#endif

#if WI_3_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_3
#elif WI_3_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_3
#elif WI_3_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_3
#elif WI_3_OCL_DIM == 3
#define WI_OCL_DIM_3 WI_3
#elif WI_3_OCL_DIM == 4
#define WI_OCL_DIM_4 WI_3
#elif WI_3_OCL_DIM == 5
#define WI_OCL_DIM_5 WI_3
#elif WI_3_OCL_DIM == 6
#define WI_OCL_DIM_6 WI_3
#endif

#if WI_4_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_4
#elif WI_4_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_4
#elif WI_4_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_4
#elif WI_4_OCL_DIM == 3
#define WI_OCL_DIM_3 WI_4
#elif WI_4_OCL_DIM == 4
#define WI_OCL_DIM_4 WI_4
#elif WI_4_OCL_DIM == 5
#define WI_OCL_DIM_5 WI_4
#elif WI_4_OCL_DIM == 6
#define WI_OCL_DIM_6 WI_4
#endif

#if WI_5_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_5
#elif WI_5_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_5
#elif WI_5_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_5
#elif WI_5_OCL_DIM == 3
#define WI_OCL_DIM_3 WI_5
#elif WI_5_OCL_DIM == 4
#define WI_OCL_DIM_4 WI_5
#elif WI_5_OCL_DIM == 5
#define WI_OCL_DIM_5 WI_5
#elif WI_5_OCL_DIM == 6
#define WI_OCL_DIM_6 WI_5
#endif

#if WI_6_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_6
#elif WI_6_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_6
#elif WI_6_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_6
#elif WI_6_OCL_DIM == 3
#define WI_OCL_DIM_3 WI_6
#elif WI_6_OCL_DIM == 4
#define WI_OCL_DIM_4 WI_6
#elif WI_6_OCL_DIM == 5
#define WI_OCL_DIM_5 WI_6
#elif WI_6_OCL_DIM == 6
#define WI_OCL_DIM_6 WI_6
#endif

#if WI_7_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_7
#elif WI_7_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_7
#elif WI_7_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_7
#elif WI_7_OCL_DIM == 3
#define WI_OCL_DIM_3 WI_7
#elif WI_7_OCL_DIM == 4
#define WI_OCL_DIM_4 WI_7
#elif WI_7_OCL_DIM == 5
#define WI_OCL_DIM_5 WI_7
#elif WI_7_OCL_DIM == 6
#define WI_OCL_DIM_6 WI_7
#endif

#if WI_1 == 1
    #define LOOP_WI_1 /* #define wi_1 0 */
    #define wi_1 0
#else
    #if WI_1_OCL_DIM == 0
    #define LOOP_WI_1 const size_t wi_1 = get_local_id(0) % (WI_OCL_DIM_0);
    #elif WI_1_OCL_DIM == 1
    #define LOOP_WI_1 const size_t wi_1 = get_local_id(0) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_1_OCL_DIM == 2
    #define LOOP_WI_1 const size_t wi_1 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1) % (WI_OCL_DIM_2);
    #elif WI_1_OCL_DIM == 3
    #define LOOP_WI_1 const size_t wi_1 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2) % (WI_OCL_DIM_3);
    #elif WI_1_OCL_DIM == 4
    #define LOOP_WI_1 const size_t wi_1 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3) % (WI_OCL_DIM_4);
    #elif WI_1_OCL_DIM == 5
    #define LOOP_WI_1 const size_t wi_1 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4) % (WI_OCL_DIM_5);
    #elif WI_1_OCL_DIM == 6
    #define LOOP_WI_1 const size_t wi_1 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4 * WI_OCL_DIM_5);
    #endif
#endif

#if WI_2 == 1
    #define LOOP_WI_2 /* #define wi_2 0 */
    #define wi_2 0
#else
    #if WI_2_OCL_DIM == 0
    #define LOOP_WI_2 const size_t wi_2 = get_local_id(0) % (WI_OCL_DIM_0);
    #elif WI_2_OCL_DIM == 1
    #define LOOP_WI_2 const size_t wi_2 = get_local_id(0) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_2_OCL_DIM == 2
    #define LOOP_WI_2 const size_t wi_2 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1) % (WI_OCL_DIM_2);
    #elif WI_2_OCL_DIM == 3
    #define LOOP_WI_2 const size_t wi_2 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2) % (WI_OCL_DIM_3);
    #elif WI_2_OCL_DIM == 4
    #define LOOP_WI_2 const size_t wi_2 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3) % (WI_OCL_DIM_4);
    #elif WI_2_OCL_DIM == 5
    #define LOOP_WI_2 const size_t wi_2 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4) % (WI_OCL_DIM_5);
    #elif WI_2_OCL_DIM == 6
    #define LOOP_WI_2 const size_t wi_2 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4 * WI_OCL_DIM_5);
    #endif
#endif

#if WI_3 == 1
    #define LOOP_WI_3 /* #define wi_3 0 */
    #define wi_3 0
#else
    #if WI_3_OCL_DIM == 0
    #define LOOP_WI_3 const size_t wi_3 = get_local_id(0) % (WI_OCL_DIM_0);
    #elif WI_3_OCL_DIM == 1
    #define LOOP_WI_3 const size_t wi_3 = get_local_id(0) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_3_OCL_DIM == 2
    #define LOOP_WI_3 const size_t wi_3 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1) % (WI_OCL_DIM_2);
    #elif WI_3_OCL_DIM == 3
    #define LOOP_WI_3 const size_t wi_3 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2) % (WI_OCL_DIM_3);
    #elif WI_3_OCL_DIM == 4
    #define LOOP_WI_3 const size_t wi_3 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3) % (WI_OCL_DIM_4);
    #elif WI_3_OCL_DIM == 5
    #define LOOP_WI_3 const size_t wi_3 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4) % (WI_OCL_DIM_5);
    #elif WI_3_OCL_DIM == 6
    #define LOOP_WI_3 const size_t wi_3 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4 * WI_OCL_DIM_5);
    #endif
#endif

#if WI_4 == 1
    #define LOOP_WI_4 /* #define wi_4 0 */
    #define wi_4 0
#else
    #if WI_4_OCL_DIM == 0
    #define LOOP_WI_4 const size_t wi_4 = get_local_id(0) % (WI_OCL_DIM_0);
    #elif WI_4_OCL_DIM == 1
    #define LOOP_WI_4 const size_t wi_4 = get_local_id(0) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_4_OCL_DIM == 2
    #define LOOP_WI_4 const size_t wi_4 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1) % (WI_OCL_DIM_2);
    #elif WI_4_OCL_DIM == 3
    #define LOOP_WI_4 const size_t wi_4 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2) % (WI_OCL_DIM_3);
    #elif WI_4_OCL_DIM == 4
    #define LOOP_WI_4 const size_t wi_4 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3) % (WI_OCL_DIM_4);
    #elif WI_4_OCL_DIM == 5
    #define LOOP_WI_4 const size_t wi_4 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4) % (WI_OCL_DIM_5);
    #elif WI_4_OCL_DIM == 6
    #define LOOP_WI_4 const size_t wi_4 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4 * WI_OCL_DIM_5);
    #endif
#endif

#if WI_5 == 1
    #define LOOP_WI_5 /* #define wi_5 0 */
    #define wi_5 0
#else
    #if WI_5_OCL_DIM == 0
    #define LOOP_WI_5 const size_t wi_5 = get_local_id(0) % (WI_OCL_DIM_0);
    #elif WI_5_OCL_DIM == 1
    #define LOOP_WI_5 const size_t wi_5 = get_local_id(0) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_5_OCL_DIM == 2
    #define LOOP_WI_5 const size_t wi_5 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1) % (WI_OCL_DIM_2);
    #elif WI_5_OCL_DIM == 3
    #define LOOP_WI_5 const size_t wi_5 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2) % (WI_OCL_DIM_3);
    #elif WI_5_OCL_DIM == 4
    #define LOOP_WI_5 const size_t wi_5 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3) % (WI_OCL_DIM_4);
    #elif WI_5_OCL_DIM == 5
    #define LOOP_WI_5 const size_t wi_5 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4) % (WI_OCL_DIM_5);
    #elif WI_5_OCL_DIM == 6
    #define LOOP_WI_5 const size_t wi_5 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4 * WI_OCL_DIM_5);
    #endif
#endif

#if WI_6 == 1
    #define LOOP_WI_6 /* #define wi_6 0 */
    #define wi_6 0
#else
    #if WI_6_OCL_DIM == 0
    #define LOOP_WI_6 const size_t wi_6 = get_local_id(0) % (WI_OCL_DIM_0);
    #elif WI_6_OCL_DIM == 1
    #define LOOP_WI_6 const size_t wi_6 = get_local_id(0) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_6_OCL_DIM == 2
    #define LOOP_WI_6 const size_t wi_6 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1) % (WI_OCL_DIM_2);
    #elif WI_6_OCL_DIM == 3
    #define LOOP_WI_6 const size_t wi_6 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2) % (WI_OCL_DIM_3);
    #elif WI_6_OCL_DIM == 4
    #define LOOP_WI_6 const size_t wi_6 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3) % (WI_OCL_DIM_4);
    #elif WI_6_OCL_DIM == 5
    #define LOOP_WI_6 const size_t wi_6 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4) % (WI_OCL_DIM_5);
    #elif WI_6_OCL_DIM == 6
    #define LOOP_WI_6 const size_t wi_6 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4 * WI_OCL_DIM_5);
    #endif
#endif

#if WI_7 == 1
    #define LOOP_WI_7 /* #define wi_7 0 */
    #define wi_7 0
#else
    #if WI_7_OCL_DIM == 0
    #define LOOP_WI_7 const size_t wi_7 = get_local_id(0) % (WI_OCL_DIM_0);
    #elif WI_7_OCL_DIM == 1
    #define LOOP_WI_7 const size_t wi_7 = get_local_id(0) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_7_OCL_DIM == 2
    #define LOOP_WI_7 const size_t wi_7 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1) % (WI_OCL_DIM_2);
    #elif WI_7_OCL_DIM == 3
    #define LOOP_WI_7 const size_t wi_7 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2) % (WI_OCL_DIM_3);
    #elif WI_7_OCL_DIM == 4
    #define LOOP_WI_7 const size_t wi_7 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3) % (WI_OCL_DIM_4);
    #elif WI_7_OCL_DIM == 5
    #define LOOP_WI_7 const size_t wi_7 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4) % (WI_OCL_DIM_5);
    #elif WI_7_OCL_DIM == 6
    #define LOOP_WI_7 const size_t wi_7 = get_local_id(0) / (WI_OCL_DIM_0 * WI_OCL_DIM_1 * WI_OCL_DIM_2 * WI_OCL_DIM_3 * WI_OCL_DIM_4 * WI_OCL_DIM_5);
    #endif
#endif

#define FLAT_WI_SIZE (WI_1 * WI_2 * WI_3 * WI_4 * WI_5 * WI_6 * WI_7)
#define FLAT_WI_IDX get_local_id(0)
