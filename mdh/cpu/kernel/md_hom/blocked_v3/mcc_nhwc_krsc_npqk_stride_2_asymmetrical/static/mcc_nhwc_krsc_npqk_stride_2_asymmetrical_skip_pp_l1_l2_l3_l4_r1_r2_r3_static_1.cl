#include "helper_ocl.h"

// MCC configuration:
#define STRIDE_H 2
#define STRIDE_W 2
// symmetry
#define INPUT_SIZE_H (STRIDE_H * INPUT_SIZE_2 + INPUT_SIZE_6 - 1)
#define INPUT_SIZE_W (STRIDE_W * INPUT_SIZE_3 + INPUT_SIZE_7 - 1)

#define OUT_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,            \
                wg_2, glb_2, wi_2, lcl_2, prv_2,            \
                wg_3, glb_3, wi_3, lcl_3, prv_3,            \
                wg_4, glb_4, wi_4, lcl_4, prv_4)            \
        ARRAY_4D_FLAT(out_glb,                              \
                      INPUT_SIZE_1                        , \
                      wg_1 * GLB_1 * WI_1 * LCL_1 * PRV_1 + \
                             glb_1 * WI_1 * LCL_1 * PRV_1 + \
                                     wi_1 * LCL_1 * PRV_1 + \
                                            lcl_1 * PRV_1 + \
                                                    prv_1 , \
                      INPUT_SIZE_2                        , \
                      wg_2 * GLB_2 * WI_2 * LCL_2 * PRV_2 + \
                             glb_2 * WI_2 * LCL_2 * PRV_2 + \
                                     wi_2 * LCL_2 * PRV_2 + \
                                            lcl_2 * PRV_2 + \
                                                    prv_2 , \
                      INPUT_SIZE_3                        , \
                      wg_3 * GLB_3 * WI_3 * LCL_3 * PRV_3 + \
                             glb_3 * WI_3 * LCL_3 * PRV_3 + \
                                     wi_3 * LCL_3 * PRV_3 + \
                                            lcl_3 * PRV_3 + \
                                                    prv_3 , \
                      INPUT_SIZE_4                        , \
                      wg_4 * GLB_4 * WI_4 * LCL_4 * PRV_4 + \
                             glb_4 * WI_4 * LCL_4 * PRV_4 + \
                                     wi_4 * LCL_4 * PRV_4 + \
                                            lcl_4 * PRV_4 + \
                                                    prv_4 )

#define OUT_PRV(lcl_1, prv_1,         \
                lcl_2, prv_2,         \
                lcl_3, prv_3,         \
                lcl_4, prv_4)         \
        ARRAY_4D_FLAT(out_prv,        \
                      LCL_1 * PRV_1 , \
                      lcl_1 * PRV_1 + \
                              prv_1 , \
                      LCL_2 * PRV_2 , \
                      lcl_2 * PRV_2 + \
                              prv_2 , \
                      LCL_3 * PRV_3 , \
                      lcl_3 * PRV_3 + \
                              prv_3 , \
                      LCL_4 * PRV_4 , \
                      lcl_4 * PRV_4 + \
                              prv_4 )

#define IMAGES_GLB(wg_1,  glb_1,  wi_1,  lcl_1,  prv_1,           \
                   wg_2,  glb_2,  wi_2,  lcl_2,  prv_2,           \
                   wg_3,  glb_3,  wi_3,  lcl_3,  prv_3,           \
                   wg_5,  glb_5,  wi_5,  lcl_5,  prv_5,           \
                   wg_6,  glb_6,  wi_6,  lcl_6,  prv_6,           \
                   wg_7,  glb_7,  wi_7,  lcl_7,  prv_7)          \
        ARRAY_4D_FLAT(images_glb,                                 \
                       INPUT_SIZE_1                             , \
                       wg_1  * GLB_1  * WI_1  * LCL_1  * PRV_1  + \
                               glb_1  * WI_1  * LCL_1  * PRV_1  + \
                                        wi_1  * LCL_1  * PRV_1  + \
                                                lcl_1  * PRV_1  + \
                                                         prv_1  , \
                       INPUT_SIZE_H                             , \
                      (wg_2  * GLB_2  * WI_2  * LCL_2  * PRV_2  + \
                               glb_2  * WI_2  * LCL_2  * PRV_2  + \
                                        wi_2  * LCL_2  * PRV_2  + \
                                                lcl_2  * PRV_2  + \
                                                         prv_2) * \
                                                      STRIDE_H  + \
                      (wg_6  * GLB_6  * WI_6  * LCL_6  * PRV_6  + \
                               glb_6  * WI_6  * LCL_6  * PRV_6  + \
                                        wi_6  * LCL_6  * PRV_6  + \
                                                lcl_6  * PRV_6  + \
                                                         prv_6) , \
                       INPUT_SIZE_W                             , \
                      (wg_3  * GLB_3  * WI_3  * LCL_3  * PRV_3  + \
                               glb_3  * WI_3  * LCL_3  * PRV_3  + \
                                        wi_3  * LCL_3  * PRV_3  + \
                                                lcl_3  * PRV_3  + \
                                                         prv_3) * \
                                                      STRIDE_W  + \
                      (wg_7  * GLB_7  * WI_7  * LCL_7  * PRV_7  + \
                               glb_7  * WI_7  * LCL_7  * PRV_7  + \
                                        wi_7  * LCL_7  * PRV_7  + \
                                                lcl_7  * PRV_7  + \
                                                         prv_7) , \
                       INPUT_SIZE_5                             , \
                       wg_5  * GLB_5  * WI_5  * LCL_5  * PRV_5  + \
                               glb_5  * WI_5  * LCL_5  * PRV_5  + \
                                        wi_5  * LCL_5  * PRV_5  + \
                                                lcl_5  * PRV_5  + \
                                                         prv_5  )

#define IMAGES_LCL(wi_1,  lcl_1,  prv_1,         \
                   wi_2,  lcl_2,  prv_2,         \
                   wi_3,  lcl_3,  prv_3,         \
                   wi_5,  lcl_5,  prv_5,         \
                   wi_6,  lcl_6,  prv_6,         \
                   wi_7,  lcl_7,  prv_7)        \
        ARRAY_4D_FLAT(images_lcl,                \
                       WI_1  * LCL_1  * PRV_1  , \
                       wi_1  * LCL_1  * PRV_1  + \
                               lcl_1  * PRV_1  + \
                                        prv_1  , \
                      (WI_2  * LCL_2  * PRV_2) * STRIDE_H - (STRIDE_H - 1) + (WI_6  * LCL_6  * PRV_6) - 1, \
                      (wi_2  * LCL_2  * PRV_2  + \
                               lcl_2  * PRV_2  + \
                                        prv_2) * \
                                     STRIDE_H  + \
                      (wi_6  * LCL_6  * PRV_6  + \
                               lcl_6  * PRV_6  + \
                                        prv_6) , \
                      (WI_3  * LCL_3  * PRV_3) * STRIDE_W - (STRIDE_W - 1) + (WI_7  * LCL_7  * PRV_7) - 1, \
                      (wi_3  * LCL_3  * PRV_3  + \
                               lcl_3  * PRV_3  + \
                                        prv_3) * \
                                     STRIDE_W  + \
                      (wi_7  * LCL_7  * PRV_7  + \
                               lcl_7  * PRV_7  + \
                                        prv_7) , \
                       WI_5  * LCL_5  * PRV_5  , \
                       wi_5  * LCL_5  * PRV_5  + \
                               lcl_5  * PRV_5  + \
                                        prv_5  )

#define IMAGES_PRV(prv_1,         \
                   prv_2,         \
                   prv_3,         \
                   prv_5,         \
                   prv_6,         \
                   prv_7)         \
        ARRAY_4D_FLAT(images_prv, \
                       PRV_1  ,   \
                       prv_1  ,   \
                      (PRV_2) * STRIDE_H - (STRIDE_H - 1) + (PRV_6) - 1, \
                      (prv_2) *   \
                    STRIDE_H  +   \
                      (prv_6) ,   \
                      (PRV_3) * STRIDE_W - (STRIDE_W - 1) + (PRV_7) - 1, \
                      (prv_3) *   \
                    STRIDE_W  +   \
                      (prv_7) ,   \
                       PRV_5  ,   \
                       prv_5  )

#define FILTER_GLB(wg_4,  glb_4,  wi_4,  lcl_4,  prv_4,          \
                   wg_5,  glb_5,  wi_5,  lcl_5,  prv_5,          \
                   wg_6,  glb_6,  wi_6,  lcl_6,  prv_6,          \
                   wg_7,  glb_7,  wi_7,  lcl_7,  prv_7)          \
        ARRAY_4D_FLAT(filter_glb,                                \
                      INPUT_SIZE_4                             , \
                      wg_4  * GLB_4  * WI_4  * LCL_4  * PRV_4  + \
                              glb_4  * WI_4  * LCL_4  * PRV_4  + \
                                       wi_4  * LCL_4  * PRV_4  + \
                                               lcl_4  * PRV_4  + \
                                                        prv_4  , \
                      INPUT_SIZE_6                             , \
                      wg_6  * GLB_6  * WI_6  * LCL_6  * PRV_6  + \
                              glb_6  * WI_6  * LCL_6  * PRV_6  + \
                                       wi_6  * LCL_6  * PRV_6  + \
                                               lcl_6  * PRV_6  + \
                                                        prv_6  , \
                      INPUT_SIZE_7                             , \
                      wg_7  * GLB_7  * WI_7  * LCL_7  * PRV_7  + \
                              glb_7  * WI_7  * LCL_7  * PRV_7  + \
                                       wi_7  * LCL_7  * PRV_7  + \
                                               lcl_7  * PRV_7  + \
                                                        prv_7  , \
                      INPUT_SIZE_5                             , \
                      wg_5  * GLB_5  * WI_5  * LCL_5  * PRV_5  + \
                              glb_5  * WI_5  * LCL_5  * PRV_5  + \
                                       wi_5  * LCL_5  * PRV_5  + \
                                               lcl_5  * PRV_5  + \
                                                        prv_5  )

#define FILTER_LCL(wi_4,  lcl_4,  prv_4,   \
                   wi_5,  lcl_5,  prv_5,   \
                   wi_6,  lcl_6,  prv_6,   \
                   wi_7,  lcl_7,  prv_7)  \
        ARRAY_4D_FLAT(filter_lcl,               \
                      WI_4  * LCL_4  * PRV_4  , \
                      wi_4  * LCL_4  * PRV_4  + \
                              lcl_4  * PRV_4  + \
                                       prv_4  , \
                      WI_6  * LCL_6  * PRV_6  , \
                      wi_6  * LCL_6  * PRV_6  + \
                              lcl_6  * PRV_6  + \
                                       prv_6  , \
                      WI_7  * LCL_7  * PRV_7  , \
                      wi_7  * LCL_7  * PRV_7  + \
                              lcl_7  * PRV_7  + \
                                       prv_7  , \
                      WI_5  * LCL_5  * PRV_5  , \
                      wi_5  * LCL_5  * PRV_5  + \
                              lcl_5  * PRV_5  + \
                                       prv_5  )

#define FILTER_PRV(prv_4,         \
                   prv_5,         \
                   prv_6,         \
                   prv_7)         \
        ARRAY_4D_FLAT(filter_prv, \
                      PRV_4  ,    \
                      prv_4  ,    \
                      PRV_6  ,    \
                      prv_6  ,    \
                      PRV_7  ,    \
                      prv_7  ,    \
                      PRV_5  ,    \
                      prv_5  )

KERNEL_QUALIFIER void mcc_nhwc_krsc_npqk_stride_2_asymmetrical_static_1(
        GLB_MEM_QUALIFIER float const * const __restrict__ images_glb,
        GLB_MEM_QUALIFIER float const * const __restrict__ filter_glb,
        GLB_MEM_QUALIFIER float * const __restrict__ _,
        GLB_MEM_QUALIFIER float * const __restrict__ out_glb) {
    #if OUT_CACHE_PRV == 1
    PRV_MEM_QUALIFIER float out_prv[(LCL_1 * PRV_1) *
                                    (LCL_2 * PRV_2) *
                                    (LCL_3 * PRV_3) *
                                    (LCL_4 * PRV_4)];
    #endif

    #if IMAGES_CACHE_LCL == 1
    LCL_MEM_QUALIFIER float images_lcl[(WI_1  * LCL_1  * PRV_1) *
                                       ((WI_2 * LCL_2  * PRV_2) * STRIDE_H - (STRIDE_H - 1) + (WI_6  * LCL_6  * PRV_6) - 1) *
                                       ((WI_3 * LCL_3  * PRV_3) * STRIDE_W - (STRIDE_W - 1) + (WI_7  * LCL_7  * PRV_7) - 1) *
                                       (WI_5  * LCL_5  * PRV_5)];
    #endif
    #if IMAGES_CACHE_PRV == 1
    PRV_MEM_QUALIFIER float images_prv[(PRV_1) *
                                       ((PRV_2) * STRIDE_H - (STRIDE_H - 1) + (PRV_6) - 1) *
                                       ((PRV_3) * STRIDE_W - (STRIDE_W - 1) + (PRV_7) - 1) *
                                       (PRV_5)];
    #endif

    #if FILTER_CACHE_LCL == 1
    LCL_MEM_QUALIFIER float filter_lcl[(WI_4  * LCL_4  * PRV_4) *
                                       (WI_6  * LCL_6  * PRV_6) *
                                       (WI_7  * LCL_7  * PRV_7) *
                                       (WI_5  * LCL_5  * PRV_5)];
    #endif
    #if FILTER_CACHE_PRV == 1
    PRV_MEM_QUALIFIER float filter_prv[(PRV_4) *
                                       (PRV_6) *
                                       (PRV_7) *
                                       (PRV_5)];
    #endif

    LOOP_WG_1 {
    LOOP_WG_2 {
    LOOP_WG_3 {
    LOOP_WG_4 {
    LOOP_WG_5 {
    LOOP_WG_6 {
    LOOP_WG_7 {

    LOOP_WI_1 {
    LOOP_WI_2 {
    LOOP_WI_3 {
    LOOP_WI_4 {
    LOOP_WI_5 {
    LOOP_WI_6 {
    LOOP_WI_7 {

    LOOP_GLB_1 {
    LOOP_GLB_2 {
    LOOP_GLB_3 {
    LOOP_GLB_4 {

    { // init prv
    LOOP_LCL_1 {
    LOOP_LCL_2 {
    LOOP_LCL_3 {
    LOOP_LCL_4 {
    LOOP_PRV_1 {
    LOOP_PRV_2 {
    LOOP_PRV_3 {
    LOOP_PRV_4 {
        #if OUT_CACHE_PRV == 1
        OUT_PRV(lcl_1, prv_1,
                lcl_2, prv_2,
                lcl_3, prv_3,
                lcl_4, prv_4)
        #else
        OUT_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
                wg_2, glb_2, wi_2, lcl_2, prv_2,
                wg_3, glb_3, wi_3, lcl_3, prv_3,
                wg_4, glb_4, wi_4, lcl_4, prv_4)
        #endif
            = 0.0f;
    }}}}}}}}
    }

    LOOP_GLB_5 {
    LOOP_GLB_6 {
    LOOP_GLB_7 {

        // copy wg -> wi
        #if IMAGES_CACHE_LCL == 1
        {
            BARRIER
        #if 1
            #define CACHE_BLOCK_OFFSET_1 (wg_1  * GLB_1  * WI_1  * LCL_1  * PRV_1  + glb_1  * WI_1  * LCL_1  * PRV_1 )
            #define CACHE_BLOCK_OFFSET_2 ((wg_2 * GLB_2  * WI_2  * LCL_2  * PRV_2  + glb_2  * WI_2  * LCL_2  * PRV_2 ) * STRIDE_H + (wg_6 * GLB_6 * WI_6 * LCL_6 * PRV_6 + glb_6 * WI_6 * LCL_6 * PRV_6))
            #define CACHE_BLOCK_OFFSET_3 ((wg_3 * GLB_3  * WI_3  * LCL_3  * PRV_3  + glb_3  * WI_3  * LCL_3  * PRV_3 ) * STRIDE_W + (wg_7 * GLB_7 * WI_7 * LCL_7 * PRV_7 + glb_7 * WI_7 * LCL_7 * PRV_7))
            #define CACHE_BLOCK_OFFSET_4 (wg_5  * GLB_5  * WI_5  * LCL_5  * PRV_5  + glb_5  * WI_5  * LCL_5  * PRV_5 )
            #define CACHE_BLOCK_SIZE_1 (WI_1  * LCL_1  * PRV_1 )
            #define CACHE_BLOCK_SIZE_2 ((WI_2 * LCL_2  * PRV_2 ) * STRIDE_H - (STRIDE_H - 1) + (WI_6 * LCL_6 * PRV_6) - 1)
            #define CACHE_BLOCK_SIZE_3 ((WI_3 * LCL_3  * PRV_3 ) * STRIDE_W - (STRIDE_W - 1) + (WI_7 * LCL_7 * PRV_7) - 1)
            #define CACHE_BLOCK_SIZE_4 (WI_5  * LCL_5  * PRV_5 )
            #define CACHE_BLOCK_SIZE (CACHE_BLOCK_SIZE_1 * CACHE_BLOCK_SIZE_2 * CACHE_BLOCK_SIZE_3 * CACHE_BLOCK_SIZE_4)
            #if INPUT_SIZE_5 % 4 == 0 && CACHE_BLOCK_OFFSET_4 % 4 == 0 && CACHE_BLOCK_SIZE_4 % 4 == 0
            #define VECTOR_LOAD_SIZE 4
            #define VECTOR_LOAD_TYPE float4
            #elif INPUT_SIZE_5 % 2 == 0 && CACHE_BLOCK_OFFSET_4 % 2 == 0 && CACHE_BLOCK_SIZE_4 % 2 == 0
            #define VECTOR_LOAD_SIZE 2
            #define VECTOR_LOAD_TYPE float2
            #else
            #define VECTOR_LOAD_SIZE 1
            #define VECTOR_LOAD_TYPE float
            #endif
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE > 0
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE == 1
            #define step 0
            #else
            #pragma unroll
            for (size_t step = 0; step < (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE; ++step)
            #endif
            {
                ((LCL_MEM_QUALIFIER VECTOR_LOAD_TYPE*)images_lcl)[step * FLAT_WI_SIZE + FLAT_WI_IDX] =
                ARRAY_4D_FLAT(((GLB_MEM_QUALIFIER VECTOR_LOAD_TYPE*)images_glb),
                              INPUT_SIZE_1,
                              CACHE_BLOCK_OFFSET_1 + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_2 * CACHE_BLOCK_SIZE_3 * (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE)) % (CACHE_BLOCK_SIZE_1)),
                              INPUT_SIZE_H,
                              CACHE_BLOCK_OFFSET_2 + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_3 * (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE)) % (CACHE_BLOCK_SIZE_2)),
                              INPUT_SIZE_W,
                              CACHE_BLOCK_OFFSET_3 + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE) % (CACHE_BLOCK_SIZE_3)),
                              (INPUT_SIZE_5 / VECTOR_LOAD_SIZE),
                              (CACHE_BLOCK_OFFSET_4 / VECTOR_LOAD_SIZE) + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) % ((CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE))));
            }
            #endif
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) % FLAT_WI_SIZE != 0
            if (FLAT_WI_IDX < ((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) % FLAT_WI_SIZE)) {
                ((LCL_MEM_QUALIFIER VECTOR_LOAD_TYPE*)images_lcl)[((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX] =
                ARRAY_4D_FLAT(((GLB_MEM_QUALIFIER VECTOR_LOAD_TYPE*)images_glb),
                              INPUT_SIZE_1,
                              CACHE_BLOCK_OFFSET_1 + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_2 * CACHE_BLOCK_SIZE_3 * (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE)) % (CACHE_BLOCK_SIZE_1)),
                              INPUT_SIZE_H,
                              CACHE_BLOCK_OFFSET_2 + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_3 * (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE)) % (CACHE_BLOCK_SIZE_2)),
                              INPUT_SIZE_W,
                              CACHE_BLOCK_OFFSET_3 + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE) % (CACHE_BLOCK_SIZE_3)),
                              (INPUT_SIZE_5 / VECTOR_LOAD_SIZE),
                              (CACHE_BLOCK_OFFSET_4 / VECTOR_LOAD_SIZE) + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) % ((CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE))));
            }
            #endif

            #ifdef step
            #undef step
            #endif
            #undef VECTOR_LOAD_TYPE
            #undef VECTOR_LOAD_SIZE
            #undef CACHE_BLOCK_SIZE
            #undef CACHE_BLOCK_SIZE_4
            #undef CACHE_BLOCK_SIZE_3
            #undef CACHE_BLOCK_SIZE_2
            #undef CACHE_BLOCK_SIZE_1
            #undef CACHE_BLOCK_OFFSET_4
            #undef CACHE_BLOCK_OFFSET_3
            #undef CACHE_BLOCK_OFFSET_2
            #undef CACHE_BLOCK_OFFSET_1
        #else
            LOOP_LCL_1
            LOOP_LCL_2
            LOOP_LCL_3
            LOOP_LCL_5
            LOOP_LCL_6
            LOOP_LCL_7
            LOOP_PRV_1
            LOOP_PRV_2
            LOOP_PRV_3
            LOOP_PRV_5
            LOOP_PRV_6
            LOOP_PRV_7
                IMAGES_LCL(wi_1,  lcl_1,  prv_1,
                           wi_2,  lcl_2,  prv_2,
                           wi_3,  lcl_3,  prv_3,
                           wi_5,  lcl_5,  prv_5,
                           wi_6,  lcl_6,  prv_6,
                           wi_7,  lcl_7,  prv_7) =
                IMAGES_GLB(wg_1,  glb_1,  wi_1,  lcl_1,  prv_1,
                           wg_2,  glb_2,  wi_2,  lcl_2,  prv_2,
                           wg_3,  glb_3,  wi_3,  lcl_3,  prv_3,
                           wg_5,  glb_5,  wi_5,  lcl_5,  prv_5,
                           wg_6,  glb_6,  wi_6,  lcl_6,  prv_6,
                           wg_7,  glb_7,  wi_7,  lcl_7,  prv_7);
        #endif
        }
        #endif
        #if FILTER_CACHE_LCL == 1
        {
            BARRIER
        #if 1
            #define CACHE_BLOCK_OFFSET_1 (wg_4  * GLB_4  * WI_4  * LCL_4  * PRV_4  + glb_4  * WI_4  * LCL_4  * PRV_4 )
            #define CACHE_BLOCK_OFFSET_2 (wg_6  * GLB_6  * WI_6  * LCL_6  * PRV_6  + glb_6  * WI_6  * LCL_6  * PRV_6 )
            #define CACHE_BLOCK_OFFSET_3 (wg_7  * GLB_7  * WI_7  * LCL_7  * PRV_7  + glb_7  * WI_7  * LCL_7  * PRV_7 )
            #define CACHE_BLOCK_OFFSET_4 (wg_5  * GLB_5  * WI_5  * LCL_5  * PRV_5  + glb_5  * WI_5  * LCL_5  * PRV_5 )
            #define CACHE_BLOCK_SIZE_1 (WI_4  * LCL_4  * PRV_4 )
            #define CACHE_BLOCK_SIZE_2 (WI_6  * LCL_6  * PRV_6 )
            #define CACHE_BLOCK_SIZE_3 (WI_7  * LCL_7  * PRV_7 )
            #define CACHE_BLOCK_SIZE_4 (WI_5  * LCL_5  * PRV_5 )
            #define CACHE_BLOCK_SIZE (CACHE_BLOCK_SIZE_1 * CACHE_BLOCK_SIZE_2 * CACHE_BLOCK_SIZE_3 * CACHE_BLOCK_SIZE_4)
            #if INPUT_SIZE_5 % 4 == 0 && CACHE_BLOCK_SIZE_4 % 4 == 0
            #define VECTOR_LOAD_SIZE 4
            #define VECTOR_LOAD_TYPE float4
            #elif INPUT_SIZE_5 % 2 == 0 && CACHE_BLOCK_SIZE_4 % 2 == 0
            #define VECTOR_LOAD_SIZE 2
            #define VECTOR_LOAD_TYPE float2
            #else
            #define VECTOR_LOAD_SIZE 1
            #define VECTOR_LOAD_TYPE float
            #endif
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE > 0
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE == 1
            #define step 0
            #else
            #pragma unroll
            for (size_t step = 0; step < (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE; ++step)
            #endif
            {
                ((LCL_MEM_QUALIFIER VECTOR_LOAD_TYPE*)filter_lcl)[step * FLAT_WI_SIZE + FLAT_WI_IDX] =
                ARRAY_4D_FLAT(((GLB_MEM_QUALIFIER VECTOR_LOAD_TYPE*)filter_glb),
                              INPUT_SIZE_4,
                              CACHE_BLOCK_OFFSET_1 + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_2 * CACHE_BLOCK_SIZE_3 * (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE)) % (CACHE_BLOCK_SIZE_1)),
                              INPUT_SIZE_6,
                              CACHE_BLOCK_OFFSET_2 + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_3 * (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE)) % (CACHE_BLOCK_SIZE_2)),
                              INPUT_SIZE_7,
                              CACHE_BLOCK_OFFSET_3 + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE) % (CACHE_BLOCK_SIZE_3)),
                              (INPUT_SIZE_5 / VECTOR_LOAD_SIZE),
                              (CACHE_BLOCK_OFFSET_4 / VECTOR_LOAD_SIZE) + ((step * FLAT_WI_SIZE + FLAT_WI_IDX) % ((CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE))));
            }
            #endif
            #if (CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) % FLAT_WI_SIZE != 0
            if (FLAT_WI_IDX < ((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) % FLAT_WI_SIZE)) {
                ((LCL_MEM_QUALIFIER VECTOR_LOAD_TYPE*)filter_lcl)[((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX] =
                ARRAY_4D_FLAT(((GLB_MEM_QUALIFIER VECTOR_LOAD_TYPE*)filter_glb),
                              INPUT_SIZE_4,
                              CACHE_BLOCK_OFFSET_1 + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_2 * CACHE_BLOCK_SIZE_3 * (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE)) % (CACHE_BLOCK_SIZE_1)),
                              INPUT_SIZE_6,
                              CACHE_BLOCK_OFFSET_2 + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_3 * (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE)) % (CACHE_BLOCK_SIZE_2)),
                              INPUT_SIZE_7,
                              CACHE_BLOCK_OFFSET_3 + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) / (CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE) % (CACHE_BLOCK_SIZE_3)),
                              (INPUT_SIZE_5 / VECTOR_LOAD_SIZE),
                              (CACHE_BLOCK_OFFSET_4 / VECTOR_LOAD_SIZE) + ((((CACHE_BLOCK_SIZE / VECTOR_LOAD_SIZE) / FLAT_WI_SIZE) * FLAT_WI_SIZE + FLAT_WI_IDX) % ((CACHE_BLOCK_SIZE_4 / VECTOR_LOAD_SIZE))));
            }
            #endif

            #ifdef step
            #undef step
            #endif
            #undef VECTOR_LOAD_TYPE
            #undef VECTOR_LOAD_SIZE
            #undef CACHE_BLOCK_SIZE
            #undef CACHE_BLOCK_SIZE_4
            #undef CACHE_BLOCK_SIZE_3
            #undef CACHE_BLOCK_SIZE_2
            #undef CACHE_BLOCK_SIZE_1
            #undef CACHE_BLOCK_OFFSET_4
            #undef CACHE_BLOCK_OFFSET_3
            #undef CACHE_BLOCK_OFFSET_2
            #undef CACHE_BLOCK_OFFSET_1
        #else
            LOOP_LCL_4
            LOOP_LCL_5
            LOOP_LCL_6
            LOOP_LCL_7
            LOOP_PRV_4
            LOOP_PRV_5
            LOOP_PRV_6
            LOOP_PRV_7
                FILTER_LCL(wi_4,  lcl_4,  prv_4,
                           wi_5,  lcl_5,  prv_5,
                           wi_6,  lcl_6,  prv_6,
                           wi_7,  lcl_7,  prv_7) =
                FILTER_GLB(wg_4,  glb_4,  wi_4,  lcl_4,  prv_4,
                           wg_5,  glb_5,  wi_5,  lcl_5,  prv_5,
                           wg_6,  glb_6,  wi_6,  lcl_6,  prv_6,
                           wg_7,  glb_7,  wi_7,  lcl_7,  prv_7);
        #endif
        }
        #endif
        #if IMAGES_CACHE_LCL == 1 || FILTER_CACHE_LCL == 1
        BARRIER
        #endif

        LOOP_LCL_5 {
        LOOP_LCL_6 {
        LOOP_LCL_7 {
        LOOP_LCL_1 {
        LOOP_LCL_2 {
        LOOP_LCL_3 {
        LOOP_LCL_4 {

            // copy wi -> prv
            #if IMAGES_CACHE_PRV == 1
            {
                LOOP_PRV_1
                LOOP_PRV_2
                LOOP_PRV_3
                LOOP_PRV_5
                LOOP_PRV_6
                LOOP_PRV_7
                    IMAGES_PRV(prv_1,
                               prv_2,
                               prv_3,
                               prv_5,
                               prv_6,
                               prv_7) =
                    #if IMAGES_CACHE_LCL == 1
                    IMAGES_LCL(wi_1,  lcl_1,  prv_1,
                               wi_2,  lcl_2,  prv_2,
                               wi_3,  lcl_3,  prv_3,
                               wi_5,  lcl_5,  prv_5,
                               wi_6,  lcl_6,  prv_6,
                               wi_7,  lcl_7,  prv_7)
                    #else
                    IMAGES_GLB(wg_1,  glb_1,  wi_1,  lcl_1,  prv_1,
                               wg_2,  glb_2,  wi_2,  lcl_2,  prv_2,
                               wg_3,  glb_3,  wi_3,  lcl_3,  prv_3,
                               wg_5,  glb_5,  wi_5,  lcl_5,  prv_5,
                               wg_6,  glb_6,  wi_6,  lcl_6,  prv_6,
                               wg_7,  glb_7,  wi_7,  lcl_7,  prv_7)
                    #endif
                        ;
            }
            #endif
            #if FILTER_CACHE_PRV == 1
            {
                LOOP_PRV_4
                LOOP_PRV_5
                LOOP_PRV_6
                LOOP_PRV_7
                    FILTER_PRV(prv_4,
                               prv_5,
                               prv_6,
                               prv_7) =
                    #if FILTER_CACHE_LCL == 1
                    FILTER_LCL(wi_4,  lcl_4,  prv_4,
                               wi_5,  lcl_5,  prv_5,
                               wi_6,  lcl_6,  prv_6,
                               wi_7,  lcl_7,  prv_7)
                    #else
                    FILTER_GLB(wg_4,  glb_4,  wi_4,  lcl_4,  prv_4,
                               wg_5,  glb_5,  wi_5,  lcl_5,  prv_5,
                               wg_6,  glb_6,  wi_6,  lcl_6,  prv_6,
                               wg_7,  glb_7,  wi_7,  lcl_7,  prv_7)
                    #endif
                        ;
            }
            #endif

            LOOP_PRV_5 {
            LOOP_PRV_6 {
            LOOP_PRV_7 {
            LOOP_PRV_1 {
            LOOP_PRV_2 {
            LOOP_PRV_3 {
            LOOP_PRV_4 {

            // scalar phase
            #if OUT_CACHE_PRV == 1
            OUT_PRV(lcl_1, prv_1,
                    lcl_2, prv_2,
                    lcl_3, prv_3,
                    lcl_4, prv_4)
            #else
            OUT_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
                    wg_2, glb_2, wi_2, lcl_2, prv_2,
                    wg_3, glb_3, wi_3, lcl_3, prv_3,
                    wg_4, glb_4, wi_4, lcl_4, prv_4)
            #endif
                +=
            #if IMAGES_CACHE_PRV == 1
            IMAGES_PRV(prv_1,
                       prv_2,
                       prv_3,
                       prv_5,
                       prv_6,
                       prv_7)
            #elif IMAGES_CACHE_LCL == 1
            IMAGES_LCL(wi_1,  lcl_1,  prv_1,
                       wi_2,  lcl_2,  prv_2,
                       wi_3,  lcl_3,  prv_3,
                       wi_5,  lcl_5,  prv_5,
                       wi_6,  lcl_6,  prv_6,
                       wi_7,  lcl_7,  prv_7)
            #else
            IMAGES_GLB(wg_1,  glb_1,  wi_1,  lcl_1,  prv_1,
                       wg_2,  glb_2,  wi_2,  lcl_2,  prv_2,
                       wg_3,  glb_3,  wi_3,  lcl_3,  prv_3,
                       wg_5,  glb_5,  wi_5,  lcl_5,  prv_5,
                       wg_6,  glb_6,  wi_6,  lcl_6,  prv_6,
                       wg_7,  glb_7,  wi_7,  lcl_7,  prv_7)
            #endif
            *
            #if FILTER_CACHE_PRV == 1
            FILTER_PRV(prv_4,
                       prv_5,
                       prv_6,
                       prv_7)
            #elif FILTER_CACHE_LCL == 1
            FILTER_LCL(wi_4,  lcl_4,  prv_4,
                       wi_5,  lcl_5,  prv_5,
                       wi_6,  lcl_6,  prv_6,
                       wi_7,  lcl_7,  prv_7)
            #else
            FILTER_GLB(wg_4,  glb_4,  wi_4,  lcl_4,  prv_4,
                       wg_5,  glb_5,  wi_5,  lcl_5,  prv_5,
                       wg_6,  glb_6,  wi_6,  lcl_6,  prv_6,
                       wg_7,  glb_7,  wi_7,  lcl_7,  prv_7)
            #endif
            ;

            }}}}}}} // PRV
        }}}}}}} // LCL
    }}} // GLB (+)

    #if OUT_CACHE_PRV == 1
    { // copy prv -> wg
    LOOP_LCL_1 {
    LOOP_LCL_2 {
    LOOP_LCL_3 {
    LOOP_LCL_4 {
    LOOP_PRV_1 {
    LOOP_PRV_2 {
    LOOP_PRV_3 {
    LOOP_PRV_4 {
        OUT_GLB(wg_1, glb_1, wi_1, lcl_1, prv_1,
                wg_2, glb_2, wi_2, lcl_2, prv_2,
                wg_3, glb_3, wi_3, lcl_3, prv_3,
                wg_4, glb_4, wi_4, lcl_4, prv_4) =
        OUT_PRV(lcl_1, prv_1,
                lcl_2, prv_2,
                lcl_3, prv_3,
                lcl_4, prv_4);
    }}}}}}}}
    }
    #endif

    }}}} // GLB (++)
    }}}}}}} // WI
    }}}}}}} // WG
} // kernel