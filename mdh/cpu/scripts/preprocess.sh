#!/usr/bin/env bash

config_json=$1
kernel_file=$2

# make temporary copy of kernel file
temp_file=$(mktemp --suffix ".c")
trap "rm -f $temp_file" 0 2 3 15

# parse json configuration
jq --raw-output 'to_entries[] | "#define \(.key) \(.value)"' "$config_json" > "$temp_file"

# add kernel code
printf "\n\n" >> "$temp_file"
cat "$kernel_file" >> "$temp_file"

# preprocess
gcc -I `dirname ${kernel_file}` -E "$temp_file" -o "${kernel_file}_preprocessed.cl"
sed -i '/^$/d' "${kernel_file}_preprocessed.cl"
sed -i -E "s!(^# [0-9]+ .*)!//\1!g" "${kernel_file}_preprocessed.cl"

# add debug-print-define to preprocessed kernel if debug print is used
grep "DEBUG_PRINT" "${kernel_file}_preprocessed.cl" &> /dev/null
if [[ $? == 0 ]]; then
  # make temporary copy of preprocessed kernel file
  temp_file_preprocessed=$(mktemp --suffix ".cl")
  trap "rm -f temp_file_preprocessed" 0 2 3 15
  printf "#ifndef DEBUG_PRINT\n#define DEBUG_PRINT(...) if (true) printf(__VA_ARGS__)\n#endif\n\n" > "$temp_file_preprocessed"
  cat "${kernel_file}_preprocessed.cl" >> "$temp_file_preprocessed"
  cp "$temp_file_preprocessed" "${kernel_file}_preprocessed.cl"
fi