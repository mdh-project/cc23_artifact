#define KERNEL_QUALIFIER extern "C" __global__
#define GLB_MEM_QUALIFIER
#define LCL_MEM_QUALIFIER __shared__
#define PRV_MEM_QUALIFIER
#define BARRIER __syncthreads();

#define ARRAY_2D_FLAT(mem, size_1, idx_1, size_2, idx_2) \
                      mem[(idx_1) * (size_2) +           \
                                     (idx_2) ]

#if GLB_1 == 1
#define LOOP_GLB_1 /* #define glb_1 0 */
#define glb_1 0
#else
#define LOOP_GLB_1 for (size_t glb_1 = 0; glb_1 < GLB_1; ++glb_1)
#endif
#if GLB_2 == 1
#define LOOP_GLB_2 /* #define glb_2 0 */
#define glb_2 0
#else
#define LOOP_GLB_2 for (size_t glb_2 = 0; glb_2 < GLB_2; ++glb_2)
#endif
#if GLB_3 == 1
#define LOOP_GLB_3 /* #define glb_3 0 */
#define glb_3 0
#else
#define LOOP_GLB_3 for (size_t glb_3 = 0; glb_3 < GLB_3; ++glb_3)
#endif

#if LCL_1 == 1
#define LOOP_LCL_1 /* #define lcl_1 0 */
#define lcl_1 0
#else
#define LOOP_LCL_1 for (size_t lcl_1 = 0; lcl_1 < LCL_1; ++lcl_1)
#endif
#if LCL_2 == 1
#define LOOP_LCL_2 /* #define lcl_2 0 */
#define lcl_2 0
#else
#define LOOP_LCL_2 for (size_t lcl_2 = 0; lcl_2 < LCL_2; ++lcl_2)
#endif
#if LCL_3 == 1
#define LOOP_LCL_3 /* #define lcl_3 0 */
#define lcl_3 0
#else
#define LOOP_LCL_3 for (size_t lcl_3 = 0; lcl_3 < LCL_3; ++lcl_3)
#endif

#if PRV_1 == 1
#define LOOP_PRV_1 /* #define prv_1 0 */
#define prv_1 0
#else
#define LOOP_PRV_1 for (size_t prv_1 = 0; prv_1 < PRV_1; ++prv_1)
#endif
#if PRV_2 == 1
#define LOOP_PRV_2 /* #define prv_2 0 */
#define prv_2 0
#else
#define LOOP_PRV_2 for (size_t prv_2 = 0; prv_2 < PRV_2; ++prv_2)
#endif
#if PRV_3 == 1
#define LOOP_PRV_3 /* #define prv_3 0 */
#define prv_3 0
#else
#define LOOP_PRV_3 for (size_t prv_3 = 0; prv_3 < PRV_3; ++prv_3)
#endif

#if WG_1_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_1
#elif WG_1_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_1
#elif WG_1_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_1
#endif

#if WG_2_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_2
#elif WG_2_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_2
#elif WG_2_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_2
#endif

#if WG_3_OCL_DIM == 0
#define WG_OCL_DIM_0 WG_3
#elif WG_3_OCL_DIM == 1
#define WG_OCL_DIM_1 WG_3
#elif WG_3_OCL_DIM == 2
#define WG_OCL_DIM_2 WG_3
#endif

#if WG_1 == 1
    #define LOOP_WG_1 /* #define wg_1 0 */
    #define wg_1 0
#else
    #if WG_1_OCL_DIM == 0
    #define LOOP_WG_1 const size_t wg_1 = ((int)blockIdx.x) % (WG_OCL_DIM_0);
    #elif WG_1_OCL_DIM == 1
    #define LOOP_WG_1 const size_t wg_1 = ((int)blockIdx.x) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_1_OCL_DIM == 2
    #define LOOP_WG_1 const size_t wg_1 = ((int)blockIdx.x) / (WG_OCL_DIM_0 * WG_OCL_DIM_1);
    #endif
#endif

#if WG_2 == 1
    #define LOOP_WG_2 /* #define wg_2 0 */
    #define wg_2 0
#else
    #if WG_2_OCL_DIM == 0
    #define LOOP_WG_2 const size_t wg_2 = ((int)blockIdx.x) % (WG_OCL_DIM_0);
    #elif WG_2_OCL_DIM == 1
    #define LOOP_WG_2 const size_t wg_2 = ((int)blockIdx.x) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_2_OCL_DIM == 2
    #define LOOP_WG_2 const size_t wg_2 = ((int)blockIdx.x) / (WG_OCL_DIM_0 * WG_OCL_DIM_1);
    #endif
#endif

#if WG_3 == 1
    #define LOOP_WG_3 /* #define wg_3 0 */
    #define wg_3 0
#else
    #if WG_3_OCL_DIM == 0
    #define LOOP_WG_3 const size_t wg_3 = ((int)blockIdx.x) % (WG_OCL_DIM_0);
    #elif WG_3_OCL_DIM == 1
    #define LOOP_WG_3 const size_t wg_3 = ((int)blockIdx.x) / (WG_OCL_DIM_0) % (WG_OCL_DIM_1);
    #elif WG_3_OCL_DIM == 2
    #define LOOP_WG_3 const size_t wg_3 = ((int)blockIdx.x) / (WG_OCL_DIM_0 * WG_OCL_DIM_1);
    #endif
#endif

#if WI_1_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_1
#elif WI_1_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_1
#elif WI_1_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_1
#endif

#if WI_2_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_2
#elif WI_2_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_2
#elif WI_2_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_2
#endif

#if WI_3_OCL_DIM == 0
#define WI_OCL_DIM_0 WI_3
#elif WI_3_OCL_DIM == 1
#define WI_OCL_DIM_1 WI_3
#elif WI_3_OCL_DIM == 2
#define WI_OCL_DIM_2 WI_3
#endif

#if WI_1 == 1
    #define LOOP_WI_1 /* #define wi_1 0 */
    #define wi_1 0
#else
    #if WI_1_OCL_DIM == 0
    #define LOOP_WI_1 const size_t wi_1 = ((int)threadIdx.x) % (WI_OCL_DIM_0);
    #elif WI_1_OCL_DIM == 1
    #define LOOP_WI_1 const size_t wi_1 = ((int)threadIdx.x) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_1_OCL_DIM == 2
    #define LOOP_WI_1 const size_t wi_1 = ((int)threadIdx.x) / (WI_OCL_DIM_0 * WI_OCL_DIM_1);
    #endif
#endif

#if WI_2 == 1
    #define LOOP_WI_2 /* #define wi_2 0 */
    #define wi_2 0
#else
    #if WI_2_OCL_DIM == 0
    #define LOOP_WI_2 const size_t wi_2 = ((int)threadIdx.x) % (WI_OCL_DIM_0);
    #elif WI_2_OCL_DIM == 1
    #define LOOP_WI_2 const size_t wi_2 = ((int)threadIdx.x) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_2_OCL_DIM == 2
    #define LOOP_WI_2 const size_t wi_2 = ((int)threadIdx.x) / (WI_OCL_DIM_0 * WI_OCL_DIM_1);
    #endif
#endif

#if WI_3 == 1
    #define LOOP_WI_3 /* #define wi_3 0 */
    #define wi_3 0
#else
    #if WI_3_OCL_DIM == 0
    #define LOOP_WI_3 const size_t wi_3 = ((int)threadIdx.x) % (WI_OCL_DIM_0);
    #elif WI_3_OCL_DIM == 1
    #define LOOP_WI_3 const size_t wi_3 = ((int)threadIdx.x) / (WI_OCL_DIM_0) % (WI_OCL_DIM_1);
    #elif WI_3_OCL_DIM == 2
    #define LOOP_WI_3 const size_t wi_3 = ((int)threadIdx.x) / (WI_OCL_DIM_0 * WI_OCL_DIM_1);
    #endif
#endif

#define FLAT_WI_SIZE (WI_1 * WI_2 * WI_3)
#define FLAT_WI_IDX ((int)threadIdx.x)
