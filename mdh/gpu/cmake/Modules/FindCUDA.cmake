# Find the CUDA driver and runtime library from NVIDIA (looks in CUDA_TOOLKIT_ROOT_DIR)
#
#  CUDA_FOUND - System has CUDA
#  CUDA_INCLUDE_DIRS - CUDA include files directories
#  CUDA_CUDA_LIBRARY - The CUDA library
#  CUDA_LIBRARY - The CUDA library

find_library(CUDA_CUDA_LIBRARY cuda
        PATHS ${CUDA_TOOLKIT_ROOT_DIR}
        PATH_SUFFIXES lib lib64 lib/x64 targets/x86_64-linux/lib targets/x86_64-linux/lib/stubs)
if(CUDA_CUDA_LIBRARY)
    message(STATUS "Found libcuda: ${CUDA_CUDA_LIBRARY}")
    set(CUDA_FOUND true)
else()
    message(FATAL_ERROR "Cannot find libcuda.so")
    set(CUDA_FOUND false)
endif()
find_library(CUDA_CUDART_LIBRARY cudart
        PATHS ${CUDA_TOOLKIT_ROOT_DIR}
        PATH_SUFFIXES lib lib64 lib/x64)
if(CUDA_CUDART_LIBRARY)
    set(CUDA_FOUND true)
    message(STATUS "Found libcudart: ${CUDA_CUDART_LIBRARY}")
else()
    set(CUDA_FOUND false)
    message(FATAL_ERROR "Cannot find libcudart.so")
endif()