//
//  cuda_buffer.hpp
//  ocal_cuda
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef cuda_buffer_hpp
#define cuda_buffer_hpp


namespace ocal
{

namespace core
{

namespace cuda
{


template< typename T >
class buffer : public abstract::abstract_buffer< buffer<T>,                          // (required for static polymorphism)
                                                 std::shared_ptr<T>,                 // host_memory_t
                                                 CUdeviceptr,                        // buffer_t
                                                 CUevent_wrapper,                    // even_t
                                                 ::ocal::common::pointer_wrapper<T>, // host_memory_ptr_t
                                                 T
                                               >
{
  // friend class parent
  friend class abstract::abstract_buffer< buffer<T>,                          // (required for static polymorphism)
                                          std::shared_ptr<T>,                 // host_memory_t
                                          CUdeviceptr,                        // buffer_t
                                          CUevent_wrapper,                    // even_t
                                          ::ocal::common::pointer_wrapper<T>, // host_memory_ptr_t
                                          T
                                        >;
  
  private:
    // enabling easier access to parent's members
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_size;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_host_memory;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_ptr_to_host_memory;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_unique_device_id_to_device_buffer;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_host_is_dirty;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_unique_device_id_to_dirty_flag;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_unique_device_id_to_command_queue_or_stream_id;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_id_of_last_active_device;


  public:
    buffer( size_t size = 0 )
      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( size, new T[ size ], std::default_delete<T[]>() )
    {}
  
  
    buffer( size_t size, T init_value )
      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( size, new T[ size ], std::default_delete<T[]>() )
    {
      if( init_value == 0 )
        std::memset( _ptr_to_host_memory.get(), 0, _size * sizeof(T) );
      
      else
        for( int i = 0 ; i < _size ; ++i )
          this->operator[]( i ) = init_value;
    }
  

    buffer( const std::vector<T>& vec )
      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( vec.size(), new T[ vec.size() ], std::default_delete<T[]>() )
    {
      this->set_content( vec.data() );
    }


    buffer( size_t size, T* content )
      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, CUdeviceptr, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( size, content )
    {}

  
    //buffer()                    = default;  // Note: buffer are initialized with input size
    buffer( const buffer<T>&  ) = default; // Note: required to put buffers in std::vector
    buffer(       buffer<T>&& ) = default;

    buffer& operator=( const buffer<T>&  ) = default; // Note: required to put buffers in std::vector
    buffer& operator=(       buffer<T>&& ) = default;


    ~buffer()
    {
//      // free host memory
//      delete[] _host_memory;
      
      // free device buffers
      for( auto& elem : _unique_device_id_to_device_buffer )
        CUDA_SAFE_CALL( cuMemFree( elem.second ) );
    }
  
  
    // ----------------------------------------------------------
    //   set/get host memory pointer
    // ----------------------------------------------------------
  
//    void set_host_memory_ptr( std::shared_ptr<T> new_content )
//    {
//      // wait for critical events to finish
//      this->sync_read_events_on_host_memory();
//      this->sync_write_events_on_host_memory();
//
//      // set values in host buffer
//      _host_memory = new_content;
//
//      // set only host buffer as up to date
//      this->set_all_device_buffers_as_dirty();
//      this->set_host_memory_as_up_to_date();
//    }


    T* get_host_memory_ptr()
    {
      this->update_host_memory();
    
      // set only host buffer as up to date
      this->set_all_device_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
      
      return _ptr_to_host_memory;
    }


    // ----------------------------------------------------------
    //   set/get host memory (static polymorphism)
    // ----------------------------------------------------------
  
    void set_content( T* new_content )
    {
      // wait for critical events to finish
      this->sync_read_events_on_host_memory();
      this->sync_write_events_on_host_memory();
      
      // copy data
      std::memcpy( _ptr_to_host_memory, new_content, _size * sizeof( T ) );
      
      // set only host buffer as up to date
      this->set_all_device_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
    }


    std::vector<T> get_content()
    {
      this->update_host_memory();
    
      // declare result vector
      std::vector<T> content;
      content.reserve( _size );
      
      // copy data
      content.insert( content.end(), &_ptr_to_host_memory[ 0 ], &_ptr_to_host_memory[ _size ] );
  
      // set only host buffer as up to date
      this->set_all_device_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
      
      return content;
    }
  

    // ---------------------------------------------------------------------------
    //   implicit cast to CUdeviceptr (enables compatibility to CUDA-Libraries)
    // ---------------------------------------------------------------------------

    operator CUdeviceptr()
    {
      assert( _unique_device_id_to_device_buffer.find( _id_of_last_active_device ) != _unique_device_id_to_device_buffer.end() );
      
      auto stream_id = _unique_device_id_to_command_queue_or_stream_id.at( _id_of_last_active_device );
      
      return this->get_device_buffer_with_read_write_access( _id_of_last_active_device, stream_id );
    }
  

  private:
  
    // -------------------------------------------------------------------
    //   native Buffer management: creation and H2D/D2H data transfers
    // -------------------------------------------------------------------

    void create_device_buffer_if_not_existent( int unique_device_id, int stream_id ) 
    {
      // set cuda context
      set_cuda_context( unique_device_id ); // TODO: really necessary?

      // create device buffer if it does not already exist
      if( _unique_device_id_to_device_buffer.find( unique_device_id ) == _unique_device_id_to_device_buffer.end() )
      {
        // create buffer
        auto& device_ptr = _unique_device_id_to_device_buffer[ unique_device_id ];
        CUDA_SAFE_CALL( cuMemAlloc( &device_ptr, _size * sizeof( T ) ) );

        // set dirty flags
        _unique_device_id_to_dirty_flag[ unique_device_id ] = true; // Note: buffer does not exist and is read so it is dirty
        
        // set command queue id
        _unique_device_id_to_command_queue_or_stream_id[ unique_device_id ] = stream_id;
      }
    }
  
  
    void copy_data_from_device_buffer_to_device_buffer( int device_id_of_source, int device_id_of_destination ) 
    {
      // TODO: enable CUDA peer access -> Nachtrag: prüfen ob durch unified buffer bereits getan.
      
//      // set cuda context
//      set_cuda_context( unique_device_id ); // TODO: really necessary?
      
      // get default cuda stream of device "device_id_of_source"
      auto cuda_stream_id = _unique_device_id_to_command_queue_or_stream_id.at( device_id_of_source );
      auto cuda_stream    = unique_device_id_to_cuda_streams.at( device_id_of_source ).at( cuda_stream_id );

      // copy data from device to device
      auto source_buffer      = _unique_device_id_to_device_buffer.at( device_id_of_source      );
      auto destination_buffer = _unique_device_id_to_device_buffer.at( device_id_of_destination );
      
      CUDA_SAFE_CALL( cuMemcpyDtoDAsync( destination_buffer,
                                         source_buffer,
                                         _size * sizeof(T),
                                         cuda_stream
                                       );
      );

      // create and record event
      CUevent_wrapper event( device_id_of_source, cuda_stream_id );
      
      this->set_event_device_buffer_of_device_is_written( event, device_id_of_destination );
      this->set_event_device_buffer_of_device_is_read(  event, device_id_of_source      );
    }


    void copy_data_from_host_memory_to_device_buffer( int unique_device_id ) 
    {
//      // set cuda context
//      set_cuda_context( unique_device_id ); // TODO: really necessary?
    
      // get default cuda stream of device "unique_device_id"
      auto cuda_stream_id = _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id );
      auto cuda_stream    = unique_device_id_to_cuda_streams.at( unique_device_id ).at( cuda_stream_id );
      
      // copy data from host to device
      auto destination_buffer = _unique_device_id_to_device_buffer.at( unique_device_id );
      
      CUDA_SAFE_CALL( cuMemcpyHtoDAsync( destination_buffer,
                                         static_cast< void* >( _host_memory.get() ),
                                         _size * sizeof(T),
                                         cuda_stream
                                       );
      );

      // create and record event
      CUevent_wrapper event( unique_device_id, cuda_stream_id ); 
      
      this->set_event_device_buffer_of_device_is_written( event, unique_device_id );
      this->set_event_host_memory_is_read( event );
    }
  

    void copy_data_from_device_buffer_to_host_memory( int unique_device_id ) 
    {
      assert( _unique_device_id_to_device_buffer.find( unique_device_id ) != _unique_device_id_to_device_buffer.end() );  // assert device buffer exists
      
//      // set cuda context
//      set_cuda_context( unique_device_id );  // TODO: really necessary?
      
      // get cuda stream of device "unique_device_id"
      auto  cuda_stream_id = _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id );
      auto& cuda_stream    = unique_device_id_to_cuda_streams.at( unique_device_id ).at( cuda_stream_id );
      
      // copy data from device to host
      auto destination_buffer = _unique_device_id_to_device_buffer.at( unique_device_id );
      
      CUDA_SAFE_CALL( cuMemcpyDtoHAsync( reinterpret_cast< void* >( _host_memory.get() ),
                                         destination_buffer,
                                         _size * sizeof(T),
                                         cuda_stream
                                       );
      );
      
      // create and record event
      CUevent_wrapper event( unique_device_id, cuda_stream_id );
      
      this->set_event_device_buffer_of_device_is_read( event, unique_device_id );
      this->set_event_host_memory_is_written( event );
    }
};


} // namespace "cuda"

} // namespace "core"

} // namespace "ocal"


#endif /* cuda_buffer_hpp */
