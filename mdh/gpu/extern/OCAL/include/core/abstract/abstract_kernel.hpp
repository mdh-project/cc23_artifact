//
//  abstract_kernel.hpp
//  ocal
//
//  Created by Ari Rasch on 25.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef abstract_kernel_h
#define abstract_kernel_h


namespace ocal
{

namespace core
{

namespace abstract
{


template< typename child_t, typename kernel_t >
class abstract_kernel
{
  // friend classes
  template< typename, typename, typename >
  friend class abstract_device;

  public:
    abstract_kernel()                          = delete;  // Note: ctor of abstract_kernel should always take arugments (e.g., kernel's source code)
    abstract_kernel( const abstract_kernel&  ) = delete;
    abstract_kernel(       abstract_kernel&& ) = default;
  
    virtual ~abstract_kernel() = default;
  
    template< typename source_t > //, typename string_t, typename = std::enable_if_t< std::is_same< string_t, std::string >::value || std::is_same< string_t, char const[13] >::value > >
    abstract_kernel( const source_t& kernel_source_wrapper, const std::string& kernel_name, const std::vector<std::string>& kernel_flags = {}, const ::ocal::common::static_parameters& sps = ::ocal::common::static_parameters{} )
      : _kernel_as_string( kernel_source_wrapper.get_source() ), _kernel_name( kernel_name ), _kernel_flags( kernel_flags ), _unique_device_id_to_kernel(), _sps( sps )
    {}

  
    template< typename source_t >
    abstract_kernel( const source_t& kernel_source_wrapper, const std::vector<std::string>& kernel_flags, const ::ocal::common::static_parameters& sps = ::ocal::common::static_parameters{} )
      : _kernel_as_string( kernel_source_wrapper.get_source() ), _kernel_name(), _kernel_flags( kernel_flags ), _unique_device_id_to_kernel(), _sps( sps )
    {}


    template< typename source_t >
    abstract_kernel( const source_t& kernel_source_wrapper, const ::ocal::common::static_parameters& sps = ::ocal::common::static_parameters{} )
      : _kernel_as_string( kernel_source_wrapper.get_source() ), _kernel_name(), _kernel_flags(), _unique_device_id_to_kernel(), _sps( sps )
    {}
  
  
    const std::string& get_source() const
    {
      return _kernel_as_string;
    }
  

  // ----------------------------------------------------------
  //   helper
  // ----------------------------------------------------------
  
  protected:
    std::vector<std::string> get_kernel_names_in_source( const std::regex& rgx, const std::string& kernel_source )
    {
      // remove comments out of source
      auto kernel_source_without_comments = common::remove_c_style_comments( kernel_source ); 
      
      // vector for storing the found kernel names
      std::vector<std::string> kernel_names;

      try
      {
        // iterators for iterating over rgx matches in source file
        std::sregex_iterator begin( kernel_source_without_comments.begin(), kernel_source_without_comments.end(), rgx );
        std::sregex_iterator end;
        
        // iterate over string and match each occurence of rgx
        while( begin != end )
        {
          std::smatch match = *begin;
          kernel_names.push_back( match.str( 1 ) );
          ++begin;
        }
        
        // if no kernel names found => error
        if( kernel_names.empty() )
        {
          std::wcerr << "No kernel found in source file." << std::endl;
          exit( EXIT_FAILURE );
        }
        
        return kernel_names;
      }
      catch( std::regex_error& e )
      {
        // Syntax error in rgx expression
        throw e;
      }
    }
  
  
    // ----------------------------------------------------------
    //   interface for friend class "device"
    // ----------------------------------------------------------
  
    bool thread_configuration_set( int unique_device_id ) const
    {
      return _unique_device_id_to_thread_configurations.find( unique_device_id ) != _unique_device_id_to_thread_configurations.end();
    }
  
  
    auto thread_configuration( int unique_device_id ) const
    {
      return _unique_device_id_to_thread_configurations.at( unique_device_id );
    }
  
  
    auto get( int unique_device_id ) const
    {
      try
      {
        auto kernel = _unique_device_id_to_kernel.at( unique_device_id );
        return kernel;
      }
      catch( std::out_of_range )
      {
        std::wcerr << "Internal error: kernel not found" << std::endl;
        exit( EXIT_FAILURE );
      }
    }
  
  
    bool contains_kernel_for_device( int unique_device_id ) const
    {
      if( _unique_device_id_to_kernel.find( unique_device_id ) == _unique_device_id_to_kernel.end() )
        return false;
      else
        return true;
    }
  
    // ----------------------------------------------------------
    //   static polymorphism
    // ----------------------------------------------------------

    template< typename... Ts >
    void build_kernel_for_device( const Ts&... args )
    {      
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function 
      me_as_child->build_kernel_for_device( args... );      
    }


  protected:
    std::string                                                 _kernel_as_string;
    std::string                                                 _kernel_name;
    std::vector<std::string>                                    _kernel_flags;
    std::unordered_map<int, kernel_t>                           _unique_device_id_to_kernel;
    ::ocal::common::static_parameters                           _sps;
    std::unordered_map< int, std::array<std::array<int,3> ,2> > _unique_device_id_to_thread_configurations;
};


// helper classes for differentiation between passing kernel 1) as path to source and 2) kernel's source as string
class path
{
  public:
    path( const std::string& path )
      : _path( path )
    {}
  
    std::string get_source() const
    {
      std::ifstream file( _path );
      
      if( !file.is_open() )
      {
        std::cout << "error opening kernel file" << std::endl;
        exit( EXIT_FAILURE );
      }

      std::stringstream ss;
      ss << file.rdbuf();
      auto source = ss.str();
      
      return source;
    }
  
  private:
    std::string _path;
};


class source
{
  public:
    source( const std::string& source )
      : _source( source )
    {}
  
    std::string get_source() const
    {
      return _source;
    }
  
  private:
    std::string _source;
};



} // namespace "abstract"

} // namespace "core"

} // namespace "ocal"

#endif /* abstract_kernel_h */
