//
//  helper.hpp
//  ocal
//
//  Created by Ari Rasch on 30.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef core_common_helper_hpp
#define core_common_helper_hpp

namespace ocal
{

namespace core
{

namespace common
{



// checks if "base" comprises all tokens (i.e., sequences of characters splitted by whitespaces a.k.a. words) in token_string
bool contains_all_sub_strings( std::string base, std::string token_string )
{
  // split "part" in substrings splittet by white spaces
  std::vector<std::string> tokens;
  std::istringstream iss( token_string );
  std::copy( std::istream_iterator< std::string >( iss ),
             std::istream_iterator< std::string >(),
             std::back_inserter( tokens )
           );
  
  // checks for each token in tokens if it is comprised by base
  for( const auto& token : tokens )
    if( base.find( token ) == std::string::npos )
      return false;
  
  return true;
}


std::string remove_c_style_comments( std::string input )
{
  auto rgx_single_line_comments = std::regex( "(?:\\/\\/(?:\\\\\\n|[^\\n])*\\n)" );
  auto rgx_multi_line_comments  = std::regex( "(?:\\/\\*[\\s\\S]*?\\*\\/)"       );
  
  auto input_without_single_line_comments           = regex_replace( input,                              rgx_single_line_comments, "\n" );
  auto input_without_multi_and_single_line_comments = regex_replace( input_without_single_line_comments, rgx_multi_line_comments,  "" );
  
  return input_without_multi_and_single_line_comments;
}


bool file_exists (const std::string& path_to_file )
{
  std::ifstream f( path_to_file .c_str() );
  return f.good();
}


template< size_t SIZE, typename info_t, typename... info_val_t >
class info_class
{
  public:
    template< typename... Ts >
    info_class( Ts... vals )
      : _info_value_pairs( vals... )
    {}


    template< typename callable_t >
    auto check( callable_t scalar_checker  )
    {
      return check_helper_indirection( scalar_checker, std::make_index_sequence< SIZE >{} );
    }

  
  private:

    template< typename callable_t, size_t... Is >
    bool check_helper_indirection( callable_t scalar_checker, std::index_sequence<Is...> )
    {
      return check_helper( scalar_checker, std::get<Is>( _info_value_pairs )... );
    }
  
  
    template< typename callable_t, typename T, typename... Ts >
    bool check_helper( callable_t scalar_checker, T info_value_pair, Ts... rest )
    {
      const auto& info     = info_value_pair.first;
      const auto& info_val = info_value_pair.second;
      
      if( !scalar_checker( info, info_val ) )
        return false;
      else
        return check_helper( scalar_checker, rest... );
    }
  
  
    // IA
    template< typename callable_t >
    bool check_helper( callable_t )
    {
      return true;
    }
  
  private:
    std::tuple< std::pair< info_t, info_val_t >... > _info_value_pairs;
};


// factories
template< typename info_t, typename... info_val_t >
auto info( std::pair< info_t, info_val_t >... infos )
{
  return info_class< sizeof...( info_val_t ), info_t, info_val_t... >( infos... );
}




} // namespace "common"

} // namespace "core"

} // namespace "ocal"


#endif /* core_common_helper_hpp */
