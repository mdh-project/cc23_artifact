//
//  abstract_thread_configuration.hpp
//  ocal
//
//  Created by Ari Rasch on 25.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef abstract_thread_configuration_h
#define abstract_thread_configuration_h


namespace ocal
{

namespace core
{

namespace abstract
{


class abstract_thread_configuration
{
  public:
    abstract_thread_configuration() = default;
  
    virtual ~abstract_thread_configuration() = default;
  
    abstract_thread_configuration( int x, int y = 1, int z = 1 )
      : _x( x ), _y( y ), _z( z )
    {}

    abstract_thread_configuration( const std::array< int, 3 >& thread_configuration )
      : _x( thread_configuration[ 0 ] ), _y( thread_configuration[ 1 ] ), _z( thread_configuration[ 2 ] )
    {}
  

    int x() const { return _x; }
    int y() const { return _y; }
    int z() const { return _z; }

    void set(size_t index, int val) {
        switch(index) {
            case 0:
                _x = val;
                break;
            case 1:
                _y = val;
                break;
            case 2:
                _z = val;
                break;
        }
    }

    int get(size_t index) const {
        switch (index) {
            case 0:
                return _x;
            case 1:
                return _y;
            case 2:
                return _z;
            default:
                return -1;
        }
    }


  private:
    int _x;
    int _y;
    int _z;
};


} // namespace "abstract"

} // namespace "core"

} // namespace "ocal"


#endif /* abstract_thread_configuration_h */
