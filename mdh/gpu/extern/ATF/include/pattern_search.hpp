/**
 * \file    pattern_search.hpp
 * \author  Leon Schöpfner <l_scho46@uni-muenster.de>
 * \date    15.05.18.
 */


#ifndef pattern_search_h
#define pattern_search_h

#include "coordinate_space.hpp"

#include "tuner_with_constraints.hpp"
#include "tuner_without_constraints.hpp"

#ifdef EVAL
#include <cctof.hpp>
#endif


namespace atf
{
    /**
     * \brief Implementation of the ATF-interface for a pattern search technique
     *
     * \tparam T      type of tuner to inherit from
    */
    template< typename T = tuner_with_constraints>
    class pattern_search_class : public T
    {
    public:
        template< typename... Ts >
        pattern_search_class( Ts... params )
                : T(  params... )
        {
        }

        /**
         * \brief Initializes the pattern search for the search space it works on.
         *
         * Pattern search uses the coordinate space to work with, an abstraction of the ATF default search space.
         * Initializes the search space, the first base point to start with, the step size and sets the first state to
         * "INITIALIZATION".
         *
         * \param search_space      search space to initialize the pattern search for
         */
        void initialize( const search_space &search_space )
        {
#ifdef EVAL
            cctof::scoped_timer<cctof::search_timer> t;
#endif
            _search_space = coordinate_space {search_space};
	    _base = _search_space.random_point();
            _trigger = false;
            _step_size = 0.1;
	    _current_parameter = 0;
            _current_state = INITIALIZATION;
        }

        /**
         * \brief Provides the next configuration.
         *
         * Calculates new parameters. Therefore every parameter is first increased and later decreased by the
         * step size, depending on what the current state and the current parameter counter are. After each change the
         * new configuration is returned. In the PATTERN state, it just returns the current pattern point to check its
         * result.
         *
         * \return Returns the next configuration to check.
         */
        configuration get_next_config()
        {
#ifdef EVAL
            cctof::scoped_timer<cctof::search_timer> t;
#endif
            switch (_current_state) {
                case INITIALIZATION: {
                    _exploratory_point = _base;
                    _pattern_point = _base;
                    return _search_space.get_config(_base);
                }
                case EXPLORATORY_PLUS: {
                    point p = _exploratory_point;
                    p[_current_parameter] += _step_size;
                    return _search_space.get_config(p);
                }
                case EXPLORATORY_MINUS: {
                    point p = _exploratory_point;
                    if(_trigger) {
                        p[_current_parameter] -= 2*_step_size;
                    }
                    else {
                        p[_current_parameter] -= _step_size;
                    }
                    return _search_space.get_config(p);
                }
                case PATTERN: {
                    return _search_space.get_config(_pattern_point);
                }
                default:
                    throw std::runtime_error( "Invalid algorithm state" );
            }
        }

        /**
         * \brief Depending on its value the result is saved and the state is changed.
         *
         * Compares the given result of the last configuration with the one of the current exploratory point. If it is
         * lower, the parameter changes and the new result is taken over, so the exploratory point has moved in the
         * search space and therefore succeeds. If every parameter has been checked, the exploratory move has finished
         * and at success the pattern move follows. If it fails the step size is decreased and the exploratory move is
         * restarted at the base. The PATTERN state just saves the result of the checked pattern point.
         *
         * \param result the runtime costs of the last checked configuration
         */
        void report_result( const unsigned long long& result )
        {
#ifdef EVAL
      cctof::search_timer<> t;
      t.start();
#endif
            switch(_current_state)
            {
                case INITIALIZATION: {
                    if(result == std::numeric_limits<unsigned long long>::max()) {
                        _base = _search_space.random_point();
                        break;
                    }
                    _base_fitness = result;
                    _exploratory_point_fitness = result;
                    _pattern_point_fitness = result;
                    _current_state = EXPLORATORY_PLUS;
                    break;
                }
                case EXPLORATORY_PLUS: {
                    if (result < _exploratory_point_fitness) {
                        _exploratory_point[_current_parameter] += _step_size;
                        _exploratory_point = _search_space.convert_to_valid_point_mod(_exploratory_point);
                        _exploratory_point_fitness = result;
                        _trigger = true;
                    }
                    _current_state = EXPLORATORY_MINUS;
                    break;
                }
                case EXPLORATORY_MINUS: {
                    if (result < _exploratory_point_fitness) {
                        if(_trigger) {
                            _exploratory_point[_current_parameter] -= 2*_step_size;
                        }
                        else {
                            _exploratory_point[_current_parameter] -= _step_size;
                        }
                        _exploratory_point = _search_space.convert_to_valid_point_mod(_exploratory_point);
                        _exploratory_point_fitness = result;
                    }
                    _trigger = false;
                    ++_current_parameter;

                    if (_current_parameter == _base.dimension()) {
                        if (_exploratory_point_fitness < _pattern_point_fitness) {
                            _pattern_point = _search_space.convert_to_valid_point_mod(
                                    _exploratory_point + (_exploratory_point - _base));
                            _base = _exploratory_point;
                            _base_fitness = _exploratory_point_fitness;
                            _exploratory_point = _pattern_point;
                            _current_state = PATTERN;
                        } else {
                            _exploratory_point = _base;
                            _pattern_point = _base;
                            _pattern_point_fitness = _base_fitness;
                            _exploratory_point_fitness = _base_fitness;
                            decrement_step_size();
                            _current_state = EXPLORATORY_PLUS;
                        }
                        _current_parameter = 0;
                    } else {
                        _current_state = EXPLORATORY_PLUS;
                    }
                    break;
                }
                case PATTERN: {
                    _pattern_point_fitness = result;
                    _exploratory_point_fitness = result;
                    _current_state = EXPLORATORY_PLUS;
		            break;
                }
		        default:
              	    throw std::runtime_error( "Invalid algorithm state" );
            }
#ifdef EVAL
    t.stop();
    cctof::timetable::get() += t;
#endif
        }

        /**
         * \brief Finalizes the pattern search technique.
         */
        void finalize()
        {}

    private:
        /** state to indicate what do do next */
        enum State{
            INITIALIZATION,
            EXPLORATORY_PLUS,
            EXPLORATORY_MINUS,
            PATTERN
        };

        /** coordinate search space */
        coordinate_space        _search_space;
        /** base point to go back to and its fitness */
        point                   _base;
        unsigned long long                  _base_fitness;
        /** point that moves through the search space and its fitness */
        point                   _exploratory_point;
        unsigned long long                  _exploratory_point_fitness;
        /** point and its fitness after pattern move */
        point                   _pattern_point;
        unsigned long long                  _pattern_point_fitness;
        /** trigger to flag if Parameter has increased */
        bool                    _trigger;
        /** index of current parameter */
        size_t                  _current_parameter;
        /** current step size */
        double                  _step_size;
        /** current state */
        State                   _current_state;

        /**
         * \brief Decrement the step size.
         */
        void decrement_step_size()
        {
            _step_size *= 0.5;
        }


        std::string display_string() const {
            return "Pattern Search";
        }
    };

    /**
     * \brief Construct a new instance of pattern search technique.
     *
     * \tparam Ts     types to forward to tuner base class
     * \param args    arguments to forward to tuner base class
     *
     * \return        Returns a new instance of the pattern search technique.
     */
    template< typename... Ts >
    auto pattern_search( Ts... args )
    {
        return pattern_search_class<>{ args... };
    }

    /**
     * \brief Construct a new instance of pattern search technique, derived from specified tuner.
     *
     * \tparam Ts     types to forward to tuner base class
     * \param args    arguments to forward to tuner base class
     *
     * \return        Returns a new instance of the pattern search technique.
     */
    template< typename T, typename... Ts >
    auto pattern_search( Ts... args )
    {
        return pattern_search_class<T>{ args... };
    }

} // namespace "atf"

#endif /* pattern_search_h */
