//
//  exhaustive.hpp
//  new_atf_lib
//
//  Created by Richard Schulze on 24/04/2018.
//  Copyright © 2018 Richard Schulze. All rights reserved.
//

#ifndef random_search_h
#define random_search_h

#include <random>
#include <chrono>

#include "tuner_with_constraints.hpp"
#include "tuner_without_constraints.hpp"

namespace atf
{

template< typename T = tuner_with_constraints>
class random_search_class : public T
{
public:
    template< typename... Ts >
    random_search_class( Ts&... params )
            : T(  params... )
    {}


    void initialize( const search_space& search_space )
    {
        _search_space = &search_space;
        _search_space_size = _search_space->num_configs();

        _generator = std::default_random_engine( static_cast<unsigned int>(
                                                         std::chrono::system_clock::now().time_since_epoch().count() ));
        _int_distribution = std::uniform_int_distribution<int>( 0, static_cast<int>( _search_space_size ) );
    }


    configuration get_next_config()
    {
        return (*_search_space)[ _int_distribution(_generator) ];
    }


    void report_result( const unsigned long long& result )
    {
        // TODO
    }


    void finalize()
    {}

    std::string display_string() const {
        return "Random search";
    }

private:
    std::default_random_engine _generator;
    std::uniform_int_distribution<int> _int_distribution;

    search_space const* _search_space;
    size_t _search_space_size;
};


template< typename... Ts >
auto random_search( Ts... args )
{
    return random_search_class<>{ args... };
}


template< typename T, typename... Ts >
auto random_search( Ts... args )
{
    return random_search_class<T>{ args... };
}


} // namespace "atf"



#endif /* random_search_h */