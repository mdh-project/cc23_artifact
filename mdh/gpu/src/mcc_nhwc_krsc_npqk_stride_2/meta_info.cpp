
void add_arguments(TCLAP::CmdLine &cmd) {

}

std::vector<std::string> inputs_str(int case_nr, int input_size_l_1, int input_size_l_2, int input_size_l_3, int input_size_l_4, int input_size_r_1, int input_size_r_2, int input_size_r_3) {
    return {std::to_string(input_size_l_1) + "x" + std::to_string(2 * input_size_l_2 - 1 + input_size_r_2 - 1) + "x" + std::to_string(2 * input_size_l_3 - 1 + input_size_r_3 - 1) + "x" + std::to_string(input_size_r_1),
            std::to_string(input_size_l_4) + "x" + std::to_string(input_size_r_2) + "x" + std::to_string(input_size_r_3) + "x" + std::to_string(input_size_r_1)
    };
}

std::vector<std::string> outputs_str(int case_nr, int input_size_l_1, int input_size_l_2, int input_size_l_3, int input_size_l_4, int input_size_r_1, int input_size_r_2, int input_size_r_3) {
    return {std::to_string(input_size_l_1) + "x" + std::to_string(input_size_l_2) + "x" + std::to_string(input_size_l_3) + "x" + std::to_string(input_size_l_4)};
}

void generation_data_path(std::vector<std::string> &path) {

}

void gold_data_path(std::vector<std::string> &path) {

}

void tuning_data_path(std::vector<std::string> &path) {

}

void benchmark_data_path(std::vector<std::string> &path) {

}