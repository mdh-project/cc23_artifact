void calculate_gold(const std::string &data_dir, int input_size_l_1, int input_size_l_2, int input_size_r_1) {
    std::ofstream gold_file(data_dir + "/gold.tsv", std::ios::out | std::ios::trunc);
    gold_file << std::fixed << std::setprecision(0);
    for (size_t i = 0; i < input_size_l_1; ++i) {
        for (size_t j = 0; j < input_size_l_2; ++j) {
            float acc = 0;
            for (size_t k = 0; k < input_size_r_1; ++k) {
                acc += a[i * input_size_r_1 + k] * b[k * input_size_l_2 + j];
            }
            gold_file << acc << "\t";
        }
        gold_file << std::endl;
    }
    gold_file.close();
}