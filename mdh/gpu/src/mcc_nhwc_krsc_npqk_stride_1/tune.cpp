#include "data_util/include/data_util.hpp"
#include "ATF/atf.h"
#include "tclap/CmdLine.h"
#include "json/json.hpp"
using json = nlohmann::json;

#include <sys/types.h>
#include <sys/wait.h>

#include "meta_info.cpp"
#include "data_info.cpp"

int main(int argc, const char **argv) {
    // check if process wrapper is called
    bool is_wrapper = atf::tuner_with_constraints::is_process_wrapper(argc, argv);

    // define parameters
    TCLAP::CmdLine cmd("Tune md_hom kernel.");

    TCLAP::ValueArg<int> device_id_arg("d", "device-id", "OpenCL device id.", false, 0, "int");
    cmd.add(device_id_arg);

    TCLAP::ValueArg<bool> dynamic_arg("", "dynamic", "Use kernel with dynamic input sizes.", false, false, "bool");
    cmd.add(dynamic_arg);

    TCLAP::MultiArg<unsigned long long> weights_arg("", "weights", "Weights for average tuning.", false, "unsigned long long");
    cmd.add(weights_arg);

    std::vector<std::string> weighting_functions({
            "weighted_sum",
            "baseline_weighting"
    });
    TCLAP::ValueArg<std::string> weighting_function_arg("", "weighting-function", "The weighting function to use on the case results.", false, "weighted_sum",
                                                        new TCLAP::ValuesConstraint<std::string>(weighting_functions));
    cmd.add(weighting_function_arg);

    TCLAP::MultiArg<std::string> weighting_function_params_arg("", "weighting-function-params", "Parameters for the weighting function.", false, "string");
    cmd.add(weighting_function_params_arg);

    std::vector<std::string> search_space_restrictions({
            "images-cache-l-0", "images-cache-l-1", "images-cache-l-2", "images-cache-l-no-2", "images-cache-p-0", "images-cache-p-1", "images-cache-p-2", "images-cache-p-no-2",
            "filter-cache-l-0", "filter-cache-l-1", "filter-cache-l-2", "filter-cache-l-no-2", "filter-cache-p-0", "filter-cache-p-1", "filter-cache-p-2", "filter-cache-p-no-2",
            "int-res-out-cache-l-0", "int-res-out-cache-l-1", "int-res-out-cache-l-no-2", "int-res-out-cache-p-0", "int-res-out-cache-p-1", "int-res-out-cache-p-no-2",
            "l-cb-res-g", "p-cb-res-g",
            "l-cb-res-l", "p-cb-res-l",
            "l-cb-res-p", "p-cb-res-p",
            "pow2", "md_poly", "fixed_res_mem",
            "no-pp-l-1", "no-pp-l-2", "no-pp-l-3", "no-pp-l-4", "no-pp-r-1", "no-pp-r-2", "no-pp-r-3",
            "one-wg-l-1", "one-wg-l-2", "one-wg-l-3", "one-wg-l-4", "one-wg-r-1", "one-wg-r-2", "one-wg-r-3",
            "pow2-num-wg-l-1", "pow2-num-wg-l-2", "pow2-num-wg-l-3", "pow2-num-wg-l-4", "pow2-num-wg-r-1", "pow2-num-wg-r-2", "pow2-num-wg-r-3",
            "one-wi-l-1", "one-wi-l-2", "one-wi-l-3", "one-wi-l-4", "one-wi-r-1", "one-wi-r-2", "one-wi-r-3",
            "pow2-num-wi-l-1", "pow2-num-wi-l-2", "pow2-num-wi-l-3", "pow2-num-wi-l-4", "pow2-num-wi-r-1", "pow2-num-wi-r-2", "pow2-num-wi-r-3",
            "one-l-cb-size-l-1", "one-l-cb-size-l-2", "one-l-cb-size-l-3", "one-l-cb-size-l-4", "one-l-cb-size-r-1", "one-l-cb-size-r-2", "one-l-cb-size-r-3",
            "is-l-cb-size-l-1", "is-l-cb-size-l-2", "is-l-cb-size-l-3", "is-l-cb-size-l-4", "is-l-cb-size-r-1", "is-l-cb-size-r-2", "is-l-cb-size-r-3",
            "pow2-l-cb-size-l-1", "pow2-l-cb-size-l-2", "pow2-l-cb-size-l-3", "pow2-l-cb-size-l-4", "pow2-l-cb-size-r-1", "pow2-l-cb-size-r-2", "pow2-l-cb-size-r-3",
            "one-p-cb-size-l-1", "one-p-cb-size-l-2", "one-p-cb-size-l-3", "one-p-cb-size-l-4", "one-p-cb-size-r-1", "one-p-cb-size-r-2", "one-p-cb-size-r-3",
            "is-p-cb-size-l-1", "is-p-cb-size-l-2", "is-p-cb-size-l-3", "is-p-cb-size-l-4", "is-p-cb-size-r-1", "is-p-cb-size-r-2", "is-p-cb-size-r-3",
            "pow2-p-cb-size-l-1", "pow2-p-cb-size-l-2", "pow2-p-cb-size-l-3", "pow2-p-cb-size-l-4", "pow2-p-cb-size-r-1", "pow2-p-cb-size-r-2", "pow2-p-cb-size-r-3",
            "pow2-l-1", "pow2-l-2", "pow2-l-3", "pow2-l-4", "pow2-r-1", "pow2-r-2", "pow2-r-3"
    });
    TCLAP::MultiArg<std::string> search_space_restriction_arg("r", "search-space-restriction", "Search space type.", false,
            new TCLAP::ValuesConstraint<std::string>(search_space_restrictions));
    cmd.add(search_space_restriction_arg);

    std::vector<std::string> tuner_types({"exhaustive", "OpenTuner", "random"});
    TCLAP::ValueArg<std::string> tuner_type_arg("t", "tuner", "Tuner type.", false, "exhaustive",
            new TCLAP::ValuesConstraint<std::string>(tuner_types));
    cmd.add(tuner_type_arg);

    std::vector<std::string> abort_condition_types({"evaluations", "valid_evaluations", "seconds", "minutes", "hours"});
    TCLAP::ValueArg<std::string> abort_condition_type_arg("a", "abort-condition", "Abort condition type.", false, "evaluations",
            new TCLAP::ValuesConstraint<std::string>(abort_condition_types));
    cmd.add(abort_condition_type_arg);

    TCLAP::ValueArg<int> abort_condition_value_arg("v", "abort-condition-value", "Abort condition value.", false, 0, "int");
    cmd.add(abort_condition_value_arg);

    TCLAP::ValueArg<int> warm_ups_arg("w", "warm-ups", "Number of warm ups per configuration.", false, 3, "int");
    cmd.add(warm_ups_arg);

    TCLAP::ValueArg<int> evaluations_arg("e", "evaluations", "Number of evaluations per configuration.", false, 5, "int");
    cmd.add(evaluations_arg);

    TCLAP::ValueArg<std::string> kernel_file_1_arg("", "kernel-file-1", "The kernel file for the first kernel to tune. If no file is specified, the default kernel file from the kernel directory will be used.", false, "", "string");
    cmd.add(kernel_file_1_arg);

    TCLAP::ValueArg<std::string> kernel_file_2_arg("", "kernel-file-2", "The kernel file for the second kernel to tune. If no file is specified, the default kernel file from the kernel directory will be used.", false, "", "string");
    cmd.add(kernel_file_2_arg);

    std::vector<std::string> wrapper_types({"none", "local", "remote"});
    TCLAP::ValueArg<std::string> wrapper_type_arg("", "wrapper-type", "Process wrapper type.", false, "none",
                                                  new TCLAP::ValuesConstraint<std::string>(wrapper_types));
    cmd.add(wrapper_type_arg);

    TCLAP::ValueArg<std::string> wrapper_command_arg("", "wrapper-command", "Process wrapper command.", false, std::string(argv[0]), "string");
    cmd.add(wrapper_command_arg);

    TCLAP::MultiArg<std::string> gold_file_arg("g", "gold-file", "Gold files for result check.", false, "string");
    cmd.add(gold_file_arg);

    std::vector<std::string> check_types({"none", "all", "final"});
    TCLAP::ValueArg<std::string> check_type_arg("", "check-type", "Result check type.", false, "none",
                                                  new TCLAP::ValuesConstraint<std::string>(check_types));
    cmd.add(check_type_arg);

    TCLAP::ValueArg<double> epsilon_arg("", "epsilon", "The epsilon to use for result checking.", false, 0.0, "double");
    cmd.add(epsilon_arg);

    std::vector<std::string> timeout_types({"absolute", "factor"});
    TCLAP::ValueArg<std::string> warm_up_timeout_type_arg("", "warm-up-timeout-type", "Warm-up timeout type.", false, "absolute",
                                                  new TCLAP::ValuesConstraint<std::string>(timeout_types));
    cmd.add(warm_up_timeout_type_arg);

    TCLAP::ValueArg<std::string> warm_up_timeout_value_arg("", "warm-up-timeout-value", "Warm-up timeout value.", false, "0", "unsigned long long/float");
    cmd.add(warm_up_timeout_value_arg);

    TCLAP::ValueArg<std::string> evaluation_timeout_type_arg("", "evaluation-timeout-type", "Evaluation timeout type.", false, "absolute",
                                                  new TCLAP::ValuesConstraint<std::string>(timeout_types));
    cmd.add(evaluation_timeout_type_arg);

    TCLAP::ValueArg<std::string> evaluation_timeout_value_arg("", "evaluation-timeout-value", "Evaluation timeout value.", false, "0", "unsigned long long/float");
    cmd.add(evaluation_timeout_value_arg);

    TCLAP::ValueArg<unsigned long long> progress_callback_timeout_arg("", "progress-callback-timeout", "Number of seconds after which the progress info callback is called.", false, 600, "unsigned long long");
    cmd.add(progress_callback_timeout_arg);

    TCLAP::ValueArg<std::string> path_suffix_arg("x", "suffix", "Data path suffix.", false, "", "string");
    cmd.add(path_suffix_arg);

    TCLAP::ValueArg<std::string> full_path_arg("", "path", "Data path.", false, "", "string");
    cmd.add(full_path_arg);

    TCLAP::MultiArg<int> input_size_l_1_arg("", "input-size-l-1", "Input size in dimension L_1.", true, "int");
    cmd.add(input_size_l_1_arg);

    TCLAP::MultiArg<int> input_size_l_2_arg("", "input-size-l-2", "Input size in dimension L_2.", true, "int");
    cmd.add(input_size_l_2_arg);

    TCLAP::MultiArg<int> input_size_l_3_arg("", "input-size-l-3", "Input size in dimension L_3.", true, "int");
    cmd.add(input_size_l_3_arg);

    TCLAP::MultiArg<int> input_size_l_4_arg("", "input-size-l-4", "Input size in dimension L_4.", true, "int");
    cmd.add(input_size_l_4_arg);

    TCLAP::MultiArg<int> input_size_r_1_arg("", "input-size-r-1", "Input size in dimension R_1.", true, "int");
    cmd.add(input_size_r_1_arg);

    TCLAP::MultiArg<int> input_size_r_2_arg("", "input-size-r-2", "Input size in dimension R_2.", true, "int");
    cmd.add(input_size_r_2_arg);

    TCLAP::MultiArg<int> input_size_r_3_arg("", "input-size-r-3", "Input size in dimension R_3.", true, "int");
    cmd.add(input_size_r_3_arg);

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        exit(EXIT_FAILURE);
    }
    int num_cases = input_size_l_1_arg.getValue().size();
    input_size_l_1_arg.getValue().size() == input_size_l_2_arg.getValue().size() && input_size_l_2_arg.getValue().size() == input_size_l_3_arg.getValue().size() && input_size_l_3_arg.getValue().size() == input_size_l_4_arg.getValue().size() && input_size_l_4_arg.getValue().size() == input_size_r_1_arg.getValue().size() && input_size_r_1_arg.getValue().size() == input_size_r_2_arg.getValue().size() && input_size_r_3_arg.getValue().size() == input_size_l_1_arg.getValue().size();
    if (input_size_l_1_arg.getValue().size() != num_cases || input_size_l_2_arg.getValue().size() != num_cases || input_size_l_3_arg.getValue().size() != num_cases || input_size_l_4_arg.getValue().size() != num_cases || input_size_r_1_arg.getValue().size() != num_cases || input_size_r_2_arg.getValue().size() != num_cases || input_size_r_3_arg.getValue().size() != num_cases) {
        std::cerr << "different numbers of values for input size arguments" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (weights_arg.getValue().size() % num_cases != 0) {
        std::cerr << "number of weights has to be a multiple of number of cases" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (gold_file_arg.getValue().size() % num_cases != 0) {
        std::cerr << "number of gold files has to be a multiple of number of cases" << std::endl;
        exit(EXIT_FAILURE);
    }
    atf::PROCESS_WRAPPER_TYPE wrapper_type = wrapper_type_arg.getValue() == "none" ? atf::NONE : (wrapper_type_arg.getValue() == "local" ? atf::LOCAL : atf::REMOTE);
    atf::cf::CHECK_INTERVAL check_interval = check_type_arg.getValue() == "none" ? atf::cf::CHECK_NONE : (check_type_arg.getValue() == "final" ? atf::cf::CHECK_FINAL : atf::cf::CHECK_ALL);
    std::vector<std::pair<char, int>> skip_postprocessing;
    for (const auto &str : search_space_restriction_arg.getValue()) {
        if (starts_with(str, "no-pp-")) {
            char type = str[6];
            int nr = std::atoi(str.substr(8).c_str());
            int insert_index = 0;
            for (const auto &val : skip_postprocessing) {
                if (type == 'l' && val.first == 'r' || (type == val.first && nr < val.second)) break;
                ++insert_index;
            }
            skip_postprocessing.emplace(skip_postprocessing.begin() + insert_index, type, nr);
        }
    }

    // initialize device
    atf::cf::device_info<CUDA> device_info(0, device_id_arg.getValue());
    device_info.initialize();
    std::string dev_name = device_info.device().name();
    std::vector<size_t> max_wi_size = device_info.device().max_group_size();
    size_t combined_max_wi_size = *std::max_element(max_wi_size.begin(), max_wi_size.end());
    size_t max_wg_size_val = device_info.device().max_threads_per_group();

    // prepare directory for tuning results
    std::string application = "md_hom";
    std::string version = "blocked_v3";
    std::string routine = "mcc_nhwc_krsc_npqk_stride_1";
    std::vector<int> input_size_l_1 = input_size_l_1_arg.getValue();
    int avg_input_size_l_1 = std::accumulate(input_size_l_1.begin(), input_size_l_1.end(), 0) / input_size_l_1.size();
    int max_input_size_l_1 = *std::max_element(input_size_l_1.begin(), input_size_l_1.end());
    std::vector<int> input_size_l_2 = input_size_l_2_arg.getValue();
    int avg_input_size_l_2 = std::accumulate(input_size_l_2.begin(), input_size_l_2.end(), 0) / input_size_l_2.size();
    int max_input_size_l_2 = *std::max_element(input_size_l_2.begin(), input_size_l_2.end());
    std::vector<int> input_size_l_3 = input_size_l_3_arg.getValue();
    int avg_input_size_l_3 = std::accumulate(input_size_l_3.begin(), input_size_l_3.end(), 0) / input_size_l_3.size();
    int max_input_size_l_3 = *std::max_element(input_size_l_3.begin(), input_size_l_3.end());
    std::vector<int> input_size_l_4 = input_size_l_4_arg.getValue();
    int avg_input_size_l_4 = std::accumulate(input_size_l_4.begin(), input_size_l_4.end(), 0) / input_size_l_4.size();
    int max_input_size_l_4 = *std::max_element(input_size_l_4.begin(), input_size_l_4.end());
    std::vector<int> input_size_r_1 = input_size_r_1_arg.getValue();
    int avg_input_size_r_1 = std::accumulate(input_size_r_1.begin(), input_size_r_1.end(), 0) / input_size_r_1.size();
    int max_input_size_r_1 = *std::max_element(input_size_r_1.begin(), input_size_r_1.end());
    std::vector<int> input_size_r_2 = input_size_r_2_arg.getValue();
    int avg_input_size_r_2 = std::accumulate(input_size_r_2.begin(), input_size_r_2.end(), 0) / input_size_r_2.size();
    int max_input_size_r_2 = *std::max_element(input_size_r_2.begin(), input_size_r_2.end());
    std::vector<int> input_size_r_3 = input_size_r_3_arg.getValue();
    int avg_input_size_r_3 = std::accumulate(input_size_r_3.begin(), input_size_r_3.end(), 0) / input_size_r_3.size();
    int max_input_size_r_3 = *std::max_element(input_size_r_3.begin(), input_size_r_3.end());
    std::string input_size = std::to_string(avg_input_size_l_1) + "x" + std::to_string(avg_input_size_l_2) + "x" + std::to_string(avg_input_size_l_3) + "x" + std::to_string(avg_input_size_l_4) + "x" + std::to_string(avg_input_size_r_1) + "x" + std::to_string(avg_input_size_r_2) + "x" + std::to_string(avg_input_size_r_3);
    std::vector<std::string> path = {dev_name, application, version, routine, dynamic_arg.getValue() ? "dynamic" : "static", input_size};
    std::string data_dir = full_path_arg.getValue();
    if (data_dir.empty()) {
        // build path for tuning results if no path was specified in args
        tuning_data_path(path);
        if (!path_suffix_arg.getValue().empty()) path.emplace_back(path_suffix_arg.getValue());
        data_dir = "../" + data_directory(path);
    }
    if (!is_wrapper)
        prepare_data_directory(data_dir, true);

    // get path for kernel files and kernel name
    path = {"kernel", application, version, routine, dynamic_arg.getValue() ? "dynamic" : "static"};
    generation_data_path(path);
    std::string kernel_dir = "../" + data_directory(path);
    std::string file_1 = kernel_dir + "/" + routine, file_2 = kernel_dir + "/" + routine;
    if (!skip_postprocessing.empty()) {
        file_1 += "_skip_pp";
        file_2 += "_skip_pp";
    }
    for (const auto &val : skip_postprocessing) {
        file_1 += "_" + std::string(1, val.first) + std::to_string(val.second);
        file_2 += "_" + std::string(1, val.first) + std::to_string(val.second);
    }
    std::string kernel_name;
    if (dynamic_arg.getValue() == true) {
        file_1 += "_dynamic_1";
        file_2 += "_dynamic_2";
        kernel_name = routine + "_dynamic";
    } else {
        file_1 += "_static_1";
        file_2 += "_static_2";
        kernel_name = routine + "_static";
    }
    file_1 += ".cuda";
    file_2 += ".cuda";
    if (!kernel_file_1_arg.getValue().empty())
        file_1 = kernel_file_1_arg.getValue();
    if (!kernel_file_2_arg.getValue().empty())
        file_2 = kernel_file_1_arg.getValue();

    // initialize data
    init_data(input_size_l_1, input_size_l_2, input_size_l_3, input_size_l_4, input_size_r_1, input_size_r_2, input_size_r_3);

    // create tuner
    std::string log_file = data_dir + "/tuning_log.csv";
    std::unique_ptr<atf::tuner_with_constraints> tuner_ptr;
    auto create_tuner = [&] (const auto &abort_condition) {
        if (tuner_type_arg.getValue() == "exhaustive") {
            tuner_ptr = std::make_unique<atf::exhaustive_class<>>(abort_condition, log_file, wrapper_type, wrapper_command_arg.getValue());
        } else if (tuner_type_arg.getValue() == "OpenTuner") {
            std::string database_file = data_dir + "/tuning_log.db";
            tuner_ptr = std::make_unique<atf::open_tuner_class<>>(abort_condition, log_file, wrapper_type, wrapper_command_arg.getValue());
            dynamic_cast<atf::open_tuner_class<>*>(tuner_ptr.get())->set_path_to_database(database_file);
        } else if (tuner_type_arg.getValue() == "random") {
            tuner_ptr = std::make_unique<atf::random_search_class<>>(abort_condition, log_file, wrapper_type, wrapper_command_arg.getValue());
        }
    };
    if (abort_condition_type_arg.getValue() == "evaluations") {
        create_tuner(atf::cond::evaluations(abort_condition_value_arg.getValue()));
    } else if (abort_condition_type_arg.getValue() == "valid_evaluations") {
        create_tuner(atf::cond::valid_evaluations(abort_condition_value_arg.getValue()));
    } else if (abort_condition_type_arg.getValue() == "seconds") {
        create_tuner(atf::cond::duration<std::chrono::seconds>(abort_condition_value_arg.getValue()));
    } else if (abort_condition_type_arg.getValue() == "minutes") {
        create_tuner(atf::cond::duration<std::chrono::minutes>(abort_condition_value_arg.getValue()));
    } else if (abort_condition_type_arg.getValue() == "hours") {
        create_tuner(atf::cond::duration<std::chrono::hours>(abort_condition_value_arg.getValue()));
    }
    auto& tuner = *tuner_ptr;
    tuner.enable_process_wrapper(argc, argv);

    // prepare meta callback
    auto create_meta = [&] (const auto &tuner, const std::string &status) -> json {
        // write meta information
        json meta_json;
        meta_json["status"] = status;
        meta_json["search space restrictions"] = search_space_restriction_arg.getValue();
        meta_json["unconstrained search space size"] = tuner.unconstrained_search_space_size().toString();
        meta_json["constrained search space size"] = std::to_string(tuner.constrained_search_space_size());
        meta_json["tuner type"] = tuner_type_arg.getValue();
        meta_json["abort condition type"] = abort_condition_type_arg.getValue();
        meta_json["abort condition value"] = std::to_string(abort_condition_value_arg.getValue());
        meta_json["wrapper type"] = wrapper_type_arg.getValue();
        meta_json["wrapper command"] = wrapper_command_arg.getValue();
        meta_json["gold file"] = gold_file_arg.getValue();
        meta_json["check type"] = check_type_arg.getValue();
        meta_json["result check epsilon"] = epsilon_arg.getValue();
        meta_json["warm ups per configuration"] = std::to_string(warm_ups_arg.getValue());
        meta_json["warm up timeout type"] = warm_up_timeout_type_arg.getValue();
        meta_json["warm up timeout value"] = warm_up_timeout_value_arg.getValue();
        meta_json["evaluations per configuration"] = std::to_string(evaluations_arg.getValue());
        meta_json["evaluation timeout type"] = evaluation_timeout_type_arg.getValue();
        meta_json["evaluation timeout value"] = evaluation_timeout_value_arg.getValue();
        meta_json["evaluations"] = std::to_string(tuner.number_of_evaluated_configs());
        meta_json["valid evaluations"] = std::to_string(tuner.number_of_valid_evaluated_configs());
        meta_json["dynamic input sizes"] = dynamic_arg.getValue();
        meta_json["kernel 1 source file"] = file_1;
        meta_json["kernel 2 source file"] = file_2;
        meta_json["average tuning"]["weighting function"] = weighting_function_arg.getValue();
        if (weighting_function_arg.getValue() == "weighted_sum") {
            // no weighting function parameters
        } else if (weighting_function_arg.getValue() == "baseline_weighting") {
            meta_json["average tuning"]["weighting function parameters"]["exponent"] = weighting_function_params_arg.getValue().size() <= 0 ? "1.0" : weighting_function_params_arg.getValue()[0];
            meta_json["average tuning"]["weighting function parameters"]["precision"] = weighting_function_params_arg.getValue().size() <= 1 ? "4" : weighting_function_params_arg.getValue()[1];
            meta_json["average tuning"]["weighting function parameters"]["print speedups"] = weighting_function_params_arg.getValue().size() <= 1 ? "false" : (weighting_function_params_arg.getValue()[2] != "0" ? "true" : "false");
        }
        meta_json["average tuning"]["cases"] = json::array();
        for (int i = 0; i < num_cases; ++i) {
            meta_json["average tuning"]["cases"].emplace_back();
            meta_json["average tuning"]["cases"].back()["device id"] = device_id_arg.getValue();
            meta_json["average tuning"]["cases"].back()["weights"] =
                    weighting_function_arg.getValue() == "weighted_sum" && weights_arg.getValue().empty()
                    ? atf::weights(1)
                    : std::vector<unsigned long long>(weights_arg.getValue().begin() + i * weights_arg.getValue().size() / num_cases, weights_arg.getValue().begin() + (i + 1) * weights_arg.getValue().size() / num_cases);
            meta_json["average tuning"]["cases"].back()["inputs"] = inputs_str(i, input_size_l_1[i], input_size_l_2[i], input_size_l_3[i], input_size_l_4[i], input_size_r_1[i], input_size_r_2[i], input_size_r_3[i]);
            meta_json["average tuning"]["cases"].back()["outputs"] = outputs_str(i, input_size_l_1[i], input_size_l_2[i], input_size_l_3[i], input_size_l_4[i], input_size_r_1[i], input_size_r_2[i], input_size_r_3[i]);
        }
        meta_json["generation_time"] = tuner.generation_time();
        meta_json["history_length"] = tuner.history().size();
        meta_json["history"] = std::vector<json>();
        for (const auto &entry : tuner.history()) {
            json entry_json;
            entry_json["evaluations"] = std::get<0>(entry);
            entry_json["valid_evaluations"] = std::get<1>(entry);
            entry_json["milliseconds_since_start"] = std::chrono::duration_cast<std::chrono::milliseconds>(std::get<2>(entry) - std::get<2>(tuner.history().front())).count();
            for (const auto &val : std::get<3>(entry)) {
                switch (val.second.value().type_id()) {
                    case atf::value_type::int_t:
                        entry_json["configuration"][val.first] = val.second.value().int_val();
                        break;
                    case atf::value_type::size_t_t:
                        entry_json["configuration"][val.first] = val.second.value().size_t_val();
                        break;
                    case atf::value_type::float_t:
                        entry_json["configuration"][val.first] = val.second.value().float_val();
                        break;
                    case atf::value_type::double_t:
                        entry_json["configuration"][val.first] = val.second.value().double_val();
                        break;
                    case atf::value_type::string_t:
                        entry_json["configuration"][val.first] = val.second.value().string_val();
                        break;
                    default:
                        entry_json["configuration"][val.first] = std::string(val.second.value());
                }
            }
            entry_json["cost"] = std::get<4>(entry);
            meta_json["history"].push_back(entry_json);
        }

        return meta_json;
    };

    // write meta data to file
    std::ofstream meta_file;
    if (!is_wrapper) {
        meta_file.open(data_dir + "/tuning_meta", std::ios::out | std::ios::trunc);
        meta_file << std::setw(4) << create_meta(tuner, "generating");
        meta_file.close();
    }

    // define search space
    auto restrictions_begin = search_space_restriction_arg.getValue().begin();
    auto restrictions_end = search_space_restriction_arg.getValue().end();
    auto res_dest_range = [&] (const std::string &level) {
        int res_dest_min = 0;
        int res_dest_max = 2;
        if (std::find(restrictions_begin, restrictions_end, level + "-cb-res-g") != restrictions_end) {
            res_dest_min = 2;
            res_dest_max = 2;
        }
        if (std::find(restrictions_begin, restrictions_end, level + "-cb-res-l") != restrictions_end) {
            res_dest_min = 1;
            res_dest_max = 1;
        }
        if (std::find(restrictions_begin, restrictions_end, level + "-cb-res-p") != restrictions_end) {
            res_dest_min = 0;
            res_dest_max = 0;
        }
        return atf::interval<int>(res_dest_min, res_dest_max);
    };

    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {0});
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {0});
    auto L_REDUCTION = atf::tp("L_REDUCTION", {1});

    auto IMAGES_CACHE_LCL = atf::tp("IMAGES_CACHE_LCL", {0, 1});
    auto IMAGES_CACHE_PRV = atf::tp("IMAGES_CACHE_PRV", {0, 1});
    auto FILTER_CACHE_LCL = atf::tp("FILTER_CACHE_LCL", {0, 1});
    auto FILTER_CACHE_PRV = atf::tp("FILTER_CACHE_PRV", {0, 1});
    auto OUT_CACHE_PRV = atf::tp("OUT_CACHE_PRV", {0, 1});

    auto WG_1_OCL_DIM  = atf::tp("WG_1_OCL_DIM",  {3});
    auto WG_2_OCL_DIM  = atf::tp("WG_2_OCL_DIM",  {2}, atf::unequal(WG_1_OCL_DIM));
    auto WG_3_OCL_DIM  = atf::tp("WG_3_OCL_DIM",  {1}, atf::unequal(WG_1_OCL_DIM) && atf::unequal(WG_2_OCL_DIM));
    auto WG_4_OCL_DIM  = atf::tp("WG_4_OCL_DIM",  {0}, atf::unequal(WG_1_OCL_DIM) && atf::unequal(WG_2_OCL_DIM) && atf::unequal(WG_3_OCL_DIM));
    auto WG_5_OCL_DIM  = atf::tp("WG_5_OCL_DIM",  {4}, atf::unequal(WG_1_OCL_DIM) && atf::unequal(WG_2_OCL_DIM) && atf::unequal(WG_3_OCL_DIM) && atf::unequal(WG_4_OCL_DIM));
    auto WG_6_OCL_DIM  = atf::tp("WG_6_OCL_DIM",  {5}, atf::unequal(WG_1_OCL_DIM) && atf::unequal(WG_2_OCL_DIM) && atf::unequal(WG_3_OCL_DIM) && atf::unequal(WG_4_OCL_DIM) && atf::unequal(WG_5_OCL_DIM));
    auto WG_7_OCL_DIM  = atf::tp("WG_7_OCL_DIM",  {6}, atf::unequal(WG_1_OCL_DIM) && atf::unequal(WG_2_OCL_DIM) && atf::unequal(WG_3_OCL_DIM) && atf::unequal(WG_4_OCL_DIM) && atf::unequal(WG_5_OCL_DIM) && atf::unequal(WG_6_OCL_DIM));

    auto WI_1_OCL_DIM  = atf::tp("WI_1_OCL_DIM",  {3});
    auto WI_2_OCL_DIM  = atf::tp("WI_2_OCL_DIM",  {2}, atf::unequal(WI_1_OCL_DIM));
    auto WI_3_OCL_DIM  = atf::tp("WI_3_OCL_DIM",  {1}, atf::unequal(WI_1_OCL_DIM) && atf::unequal(WI_2_OCL_DIM));
    auto WI_4_OCL_DIM  = atf::tp("WI_4_OCL_DIM",  {0}, atf::unequal(WI_1_OCL_DIM) && atf::unequal(WI_2_OCL_DIM) && atf::unequal(WI_3_OCL_DIM));
    auto WI_5_OCL_DIM  = atf::tp("WI_5_OCL_DIM",  {4}, atf::unequal(WI_1_OCL_DIM) && atf::unequal(WI_2_OCL_DIM) && atf::unequal(WI_3_OCL_DIM) && atf::unequal(WI_4_OCL_DIM));
    auto WI_6_OCL_DIM  = atf::tp("WI_6_OCL_DIM",  {5}, atf::unequal(WI_1_OCL_DIM) && atf::unequal(WI_2_OCL_DIM) && atf::unequal(WI_3_OCL_DIM) && atf::unequal(WI_4_OCL_DIM) && atf::unequal(WI_5_OCL_DIM));
    auto WI_7_OCL_DIM  = atf::tp("WI_7_OCL_DIM",  {6}, atf::unequal(WI_1_OCL_DIM) && atf::unequal(WI_2_OCL_DIM) && atf::unequal(WI_3_OCL_DIM) && atf::unequal(WI_4_OCL_DIM) && atf::unequal(WI_5_OCL_DIM) && atf::unequal(WI_6_OCL_DIM));

    auto INPUT_SIZE_1 = atf::tp("INPUT_SIZE_1", {max_input_size_l_1});
    auto WG_1         = atf::tp("WG_1",         atf::interval<int>(1, max_input_size_l_1), atf::divides(INPUT_SIZE_1));
    auto GLB_1        = atf::tp("GLB_1",        atf::interval<int>(1, max_input_size_l_1), atf::divides(INPUT_SIZE_1 / WG_1));
    auto WI_1         = atf::tp("WI_1",         atf::interval<int>(1, max_input_size_l_1), atf::divides(INPUT_SIZE_1 / WG_1 / GLB_1));
    auto LCL_1        = atf::tp("LCL_1",        atf::interval<int>(1, max_input_size_l_1), atf::divides(INPUT_SIZE_1 / WG_1 / GLB_1 / WI_1));
    auto PRV_1        = atf::tp("PRV_1",        atf::interval<int>(1, max_input_size_l_1), atf::equal(INPUT_SIZE_1 / WG_1 / GLB_1 / WI_1 / LCL_1));

    auto INPUT_SIZE_2 = atf::tp("INPUT_SIZE_2", {max_input_size_l_2});
    auto WG_2         = atf::tp("WG_2",         atf::interval<int>(1, max_input_size_l_2), atf::divides(INPUT_SIZE_2));
    auto GLB_2        = atf::tp("GLB_2",        atf::interval<int>(1, max_input_size_l_2), atf::divides(INPUT_SIZE_2 / WG_2));
    auto WI_2         = atf::tp("WI_2",         atf::interval<int>(1, max_input_size_l_2), atf::divides(INPUT_SIZE_2 / WG_2 / GLB_2));
    auto LCL_2        = atf::tp("LCL_2",        atf::interval<int>(1, max_input_size_l_2), atf::divides(INPUT_SIZE_2 / WG_2 / GLB_2 / WI_2));
    auto PRV_2        = atf::tp("PRV_2",        atf::interval<int>(1, max_input_size_l_2), atf::equal(INPUT_SIZE_2 / WG_2 / GLB_2 / WI_2 / LCL_2));

    auto INPUT_SIZE_3 = atf::tp("INPUT_SIZE_3", {max_input_size_l_3});
    auto WG_3         = atf::tp("WG_3",         atf::interval<int>(1, max_input_size_l_3), atf::divides(INPUT_SIZE_3));
    auto GLB_3        = atf::tp("GLB_3",        atf::interval<int>(1, max_input_size_l_3), atf::divides(INPUT_SIZE_3 / WG_3));
    auto WI_3         = atf::tp("WI_3",         atf::interval<int>(1, max_input_size_l_3), atf::divides(INPUT_SIZE_3 / WG_3 / GLB_3));
    auto LCL_3        = atf::tp("LCL_3",        atf::interval<int>(1, max_input_size_l_3), atf::divides(INPUT_SIZE_3 / WG_3 / GLB_3 / WI_3));
    auto PRV_3        = atf::tp("PRV_3",        atf::interval<int>(1, max_input_size_l_3), atf::equal(INPUT_SIZE_3 / WG_3 / GLB_3 / WI_3 / LCL_3));

    auto INPUT_SIZE_4 = atf::tp("INPUT_SIZE_4", {max_input_size_l_4});
    auto WG_4         = atf::tp("WG_4",         atf::interval<int>(1, max_input_size_l_4), atf::divides(INPUT_SIZE_4));
    auto GLB_4        = atf::tp("GLB_4",        atf::interval<int>(1, max_input_size_l_4), atf::divides(INPUT_SIZE_4 / WG_4));
    auto WI_4         = atf::tp("WI_4",         atf::interval<int>(1, max_input_size_l_4), atf::divides(INPUT_SIZE_4 / WG_4 / GLB_4));
    auto LCL_4        = atf::tp("LCL_4",        atf::interval<int>(1, max_input_size_l_4), atf::divides(INPUT_SIZE_4 / WG_4 / GLB_4 / WI_4));
    auto PRV_4        = atf::tp("PRV_4",        atf::interval<int>(1, max_input_size_l_4), atf::equal(INPUT_SIZE_4 / WG_4 / GLB_4 / WI_4 / LCL_4));

    auto INPUT_SIZE_5 = atf::tp("INPUT_SIZE_5", {max_input_size_r_1});
    auto WG_5         = atf::tp("WG_5",         atf::interval<int>(1,                  1), atf::divides(INPUT_SIZE_5));
    auto GLB_5        = atf::tp("GLB_5",        atf::interval<int>(1, max_input_size_r_1), atf::divides(INPUT_SIZE_5 / WG_5));
    auto WI_5         = atf::tp("WI_5",         atf::interval<int>(1,                  1), atf::divides(INPUT_SIZE_5 / WG_5 / GLB_5));
    auto LCL_5        = atf::tp("LCL_5",        atf::interval<int>(1, max_input_size_r_1), atf::divides(INPUT_SIZE_5 / WG_5 / GLB_5 / WI_5));
    auto PRV_5        = atf::tp("PRV_5",        atf::interval<int>(1, max_input_size_r_1), atf::equal(INPUT_SIZE_5 / WG_5 / GLB_5 / WI_5 / LCL_5));

    auto INPUT_SIZE_6 = atf::tp("INPUT_SIZE_6", {max_input_size_r_2});
    auto WG_6         = atf::tp("WG_6",         atf::interval<int>(1,                  1), atf::divides(INPUT_SIZE_6));
    auto GLB_6        = atf::tp("GLB_6",        atf::interval<int>(1, max_input_size_r_2), atf::divides(INPUT_SIZE_6 / WG_6));
    auto WI_6         = atf::tp("WI_6",         atf::interval<int>(1,                  1), atf::divides(INPUT_SIZE_6 / WG_6 / GLB_6));
    auto LCL_6        = atf::tp("LCL_6",        atf::interval<int>(1, max_input_size_r_2), atf::divides(INPUT_SIZE_6 / WG_6 / GLB_6 / WI_6));
    auto PRV_6        = atf::tp("PRV_6",        atf::interval<int>(1, max_input_size_r_2), atf::equal(INPUT_SIZE_6 / WG_6 / GLB_6 / WI_6 / LCL_6));

    auto INPUT_SIZE_7 = atf::tp("INPUT_SIZE_7", {max_input_size_r_3});
    auto WG_7         = atf::tp("WG_7",         atf::interval<int>(1,                  1), atf::divides(INPUT_SIZE_7));
    auto GLB_7        = atf::tp("GLB_7",        atf::interval<int>(1, max_input_size_r_3), atf::divides(INPUT_SIZE_7 / WG_7));
    auto WI_7         = atf::tp("WI_7",         atf::interval<int>(1,                  1), atf::divides(INPUT_SIZE_7 / WG_7 / GLB_7));
    auto LCL_7        = atf::tp("LCL_7",        atf::interval<int>(1, max_input_size_r_3), atf::divides(INPUT_SIZE_7 / WG_7 / GLB_7 / WI_7));
    auto PRV_7        = atf::tp("PRV_7",        atf::interval<int>(1, max_input_size_r_3), atf::equal(INPUT_SIZE_7 / WG_7 / GLB_7 / WI_7 / LCL_7));

    // add parameters to tuner
    tuner(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL);
    tuner(L_REDUCTION);
    tuner(IMAGES_CACHE_LCL)(IMAGES_CACHE_PRV)(FILTER_CACHE_LCL)(FILTER_CACHE_PRV)(OUT_CACHE_PRV);
    tuner(WG_1_OCL_DIM, WG_2_OCL_DIM, WG_3_OCL_DIM, WG_4_OCL_DIM, WG_5_OCL_DIM, WG_6_OCL_DIM, WG_7_OCL_DIM);
    tuner(WI_1_OCL_DIM, WI_2_OCL_DIM, WI_3_OCL_DIM, WI_4_OCL_DIM, WI_5_OCL_DIM, WI_6_OCL_DIM, WI_7_OCL_DIM);
    tuner(INPUT_SIZE_1,  WG_1,  GLB_1,  WI_1,  LCL_1,  PRV_1);
    tuner(INPUT_SIZE_2,  WG_2,  GLB_2,  WI_2,  LCL_2,  PRV_2);
    tuner(INPUT_SIZE_3,  WG_3,  GLB_3,  WI_3,  LCL_3,  PRV_3);
    tuner(INPUT_SIZE_4,  WG_4,  GLB_4,  WI_4,  LCL_4,  PRV_4);
    tuner(INPUT_SIZE_5,  WG_5,  GLB_5,  WI_5,  LCL_5,  PRV_5);
    tuner(INPUT_SIZE_6,  WG_6,  GLB_6,  WI_6,  LCL_6,  PRV_6);
    tuner(INPUT_SIZE_7,  WG_7,  GLB_7,  WI_7,  LCL_7,  PRV_7);

    auto is_valid = [&](atf::configuration &config) -> bool {
        // check work item sizes
        size_t num_wi = 1;
        num_wi *= config[WI_1.name()].value().int_val();
        num_wi *= config[WI_2.name()].value().int_val();
        num_wi *= config[WI_3.name()].value().int_val();
        num_wi *= config[WI_4.name()].value().int_val();
        num_wi *= config[WI_5.name()].value().int_val();
        num_wi *= config[WI_6.name()].value().int_val();
        num_wi *= config[WI_7.name()].value().int_val();

        return num_wi <= max_wi_size[0] && num_wi <= max_wg_size_val;
    };
    tuner.set_is_valid(is_valid);

    // create kernel wrapper
    atf::cf::timeout warm_up_timeout;
    if (warm_up_timeout_type_arg.getValue() == "absolute") {
        warm_up_timeout.type = atf::cf::ABSOLUTE;
        std::istringstream ss(warm_up_timeout_value_arg.getValue());
        if (!(ss >> warm_up_timeout.value.absolute)) {
            std::cerr << "could not parse \"" << warm_up_timeout_value_arg.getValue() << "\" as unsigned long long" << std::endl;
            exit(EXIT_FAILURE);
        }
    } else if (warm_up_timeout_type_arg.getValue() == "factor") {
        warm_up_timeout.type = atf::cf::FACTOR;
        std::istringstream ss(warm_up_timeout_value_arg.getValue());
        if (!(ss >> warm_up_timeout.value.factor)) {
            std::cerr << "could not parse \"" << warm_up_timeout_value_arg.getValue() << "\" as float" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    atf::cf::timeout evaluation_timeout;
    if (evaluation_timeout_type_arg.getValue() == "absolute") {
        evaluation_timeout.type = atf::cf::ABSOLUTE;
        std::istringstream ss(evaluation_timeout_value_arg.getValue());
        if (!(ss >> evaluation_timeout.value.absolute)) {
            std::cerr << "could not parse \"" << evaluation_timeout_value_arg.getValue() << "\" as unsigned long long" << std::endl;
            exit(EXIT_FAILURE);
        }
    } else if (evaluation_timeout_type_arg.getValue() == "factor") {
        evaluation_timeout.type = atf::cf::FACTOR;
        std::istringstream ss(evaluation_timeout_value_arg.getValue());
        if (!(ss >> evaluation_timeout.value.factor)) {
            std::cerr << "could not parse \"" << evaluation_timeout_value_arg.getValue() << "\" as float" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    auto wrapper = atf::cf::ocal_md_hom<CUDA, 7, decltype(inputs(0)), decltype(outputs(0))>(
            atf::cf::kernel_info(cuda::path(file_1), kernel_name + "_1", {"-I" + kernel_dir}),
            atf::cf::GS(WG_1 * WI_1 * WG_2 * WI_2 * WG_3 * WI_3 * WG_4 * WI_4 * WG_5 * WI_5 * WG_6 * WI_6 * WG_7 * WI_7),
            atf::cf::LS(WI_1 * WI_2 * WI_3 * WI_4 * WI_5 * WI_6 * WI_7),

            atf::cf::kernel_info(cuda::path(file_2), kernel_name + "_2", {"-I" + kernel_dir}),
            atf::cf::GS(WG_1 * WI_1 * WG_2 * WI_2 * WG_3 * WI_3 * WG_4 * WI_4 * WI_5 * WI_6 * WI_7),
            atf::cf::LS(WI_1 * WI_2 * WI_3 * WI_4 * WI_5 * WI_6 * WI_7),

            atf::tps(WG_5, WG_6, WG_7),
            atf::tps(WI_5, WI_6, WI_7),
            atf::tps(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL),
            atf::tps(L_REDUCTION),

            dynamic_arg.getValue(),

            warm_ups_arg.getValue(), evaluations_arg.getValue(),
            warm_up_timeout, evaluation_timeout
    );
    // add cases
    for (int i = 0; i < num_cases; ++i) {
        wrapper.add_case(device_info,
                         weighting_function_arg.getValue() == "weighted_sum" && weights_arg.getValue().empty()
                         ? atf::weights(1)
                         : std::vector<unsigned long long>(weights_arg.getValue().begin() + i * weights_arg.getValue().size() / num_cases, weights_arg.getValue().begin() + (i + 1) * weights_arg.getValue().size() / num_cases),
                         atf::input_sizes(input_size_l_1[i], input_size_l_2[i], input_size_l_3[i], input_size_l_4[i], input_size_r_1[i], input_size_r_2[i], input_size_r_3[i]),
                         inputs(i),
                         outputs(i));
    }
    // set weighting function
    if (weighting_function_arg.getValue() == "weighted_sum") {
        wrapper.set_weighting_function(atf::cf::weighting::weighted_sum);
    } else if (weighting_function_arg.getValue() == "baseline_weighting") {
        wrapper.set_weighting_function(atf::cf::weighting::baseline_weighting(
                weighting_function_params_arg.getValue().size() <= 0 ? 1.0 : std::stod(weighting_function_params_arg.getValue()[0]),
                weighting_function_params_arg.getValue().size() <= 1 ? 4 : std::stoul(weighting_function_params_arg.getValue()[1]),
                weighting_function_params_arg.getValue().size() <= 2 ? false : (std::stoi(weighting_function_params_arg.getValue()[2]) != 0)
        ));
    }

    // check results
    if (!gold_file_arg.getValue().empty()) {
        std::vector<std::vector<std::string>> structured_gold_files(num_cases);
        for (int i = 0; i < num_cases; ++i) {
            structured_gold_files[i] = std::vector<std::string>(
                    gold_file_arg.getValue().begin() + i * gold_file_arg.getValue().size() / num_cases,
                    gold_file_arg.getValue().begin() + (i + 1) * gold_file_arg.getValue().size() / num_cases
            );
        }
        wrapper.check_results(check_interval, epsilon_arg.getValue(), structured_gold_files);
    }

    // start tuning
    auto tuned_configuration = tuner(wrapper);

    // log final data
    if (!is_wrapper) {
        // write meta with number of tested configurations (valid, invalid)
        meta_file.open(data_dir + "/tuning_meta", std::ios::out | std::ios::trunc);
        meta_file << std::setw(4) << create_meta(tuner, "finished");
        meta_file.close();

        if (tuner.number_of_valid_evaluated_configs() > 0) {
            // write best configuration to file
            std::map<std::string, int> json_configuration;
            for (const auto &val : tuned_configuration) {
                json_configuration[val.first] = val.second.value().int_val();
            }
            std::ofstream config_file(data_dir + "/tuned_configuration", std::ios::out | std::ios::trunc);
            config_file << std::setw(4) << json(json_configuration);
            config_file.close();
        }
    }
}