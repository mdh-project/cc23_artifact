void calculate_gold(const std::string &data_dir) {
    std::ofstream gold_file(data_dir + "/gold.tsv", std::ios::out | std::ios::trunc);

    auto images_mat = [&] (int n, int c, int h, int w) {
        return images[n * h_arg.getValue() * w_arg.getValue() * c_arg.getValue() + h * w_arg.getValue() * c_arg.getValue() + w * c_arg.getValue() + c];
    };
    auto filter_mat = [&] (int k, int c, int r, int s) {
        return filter[k * r_arg.getValue() * s_arg.getValue() * c_arg.getValue() + r * s_arg.getValue() * c_arg.getValue() + s * c_arg.getValue() + c];
    };

    for (size_t n = 0; n < n_arg.getValue(); ++n) {
        for (size_t p = 0; p < p_arg.getValue(); ++p) {
            for (size_t q = 0; q < q_arg.getValue(); ++q) {
                for (size_t k = 0; k < k_arg.getValue(); ++k) {

                    float acc = 0;
                    for (size_t r = 0; r < r_arg.getValue(); ++r) {
                        for (size_t s = 0; s < s_arg.getValue(); ++s) {
                            for (size_t c = 0; c < c_arg.getValue(); ++c) {
                                acc += images_mat(n, c, p * h_stride_arg.getValue() + r, q * w_stride_arg.getValue() + s) * filter_mat(k, c, r, s);
                            }
                        }
                    }
                    gold_file << acc << "\t";
                }
                gold_file << std::endl;
            }
            gold_file << std::endl;
        }
        gold_file << std::endl;
    }
    gold_file.close();
}