TCLAP::ValueArg<int> n_arg("n", "n", "Input size N (number of image batches).", true, 0, "int");
TCLAP::ValueArg<int> k_arg("k", "k", "Input size K (number of filter batches).", true, 0, "int");
TCLAP::ValueArg<int> h_arg("h", "h", "Input size H (input image height).", true, 0, "int");
TCLAP::ValueArg<int> w_arg("w", "w", "Input size W (input image width).", true, 0, "int");
TCLAP::ValueArg<int> p_arg("p", "p", "Input size P (output image height).", true, 0, "int");
TCLAP::ValueArg<int> q_arg("q", "q", "Input size Q (output image width).", true, 0, "int");
TCLAP::ValueArg<int> c_arg("c", "c", "Input size C (number of input channels).", true, 0, "int");
TCLAP::ValueArg<int> r_arg("r", "r", "Input size R (filter height).", true, 0, "int");
TCLAP::ValueArg<int> s_arg("s", "s", "Input size S (filter width).", true, 0, "int");
TCLAP::ValueArg<int> h_stride_arg("", "h-stride", "Stride for input size H.", false, 1, "int");
TCLAP::ValueArg<int> w_stride_arg("", "w-stride", "Stride for input size W.", false, 1, "int");

void add_arguments(TCLAP::CmdLine &cmd) {
    cmd.add(n_arg);
    cmd.add(k_arg);
    cmd.add(h_arg);
    cmd.add(w_arg);
    cmd.add(p_arg);
    cmd.add(q_arg);
    cmd.add(c_arg);
    cmd.add(r_arg);
    cmd.add(s_arg);
    cmd.add(h_stride_arg);
    cmd.add(w_stride_arg);
}

void benchmark_data_path(std::vector<std::string> &path) {
    path.push_back(
            std::to_string(n_arg.getValue()) + "_" + std::to_string(k_arg.getValue()) + "_" + std::to_string(h_arg.getValue()) + "x" + std::to_string(w_arg.getValue()) + "_" + std::to_string(p_arg.getValue()) + "x" + std::to_string(q_arg.getValue()) + "_" + std::to_string(c_arg.getValue()) + "_" + std::to_string(r_arg.getValue()) + "_" + std::to_string(s_arg.getValue()) + "_" +
            std::to_string(h_stride_arg.getValue()) + "_" + std::to_string(w_stride_arg.getValue())
    );
}

void gold_data_path(std::vector<std::string> &path) {
    path.push_back(
            std::to_string(n_arg.getValue()) + "_" + std::to_string(k_arg.getValue()) + "_" + std::to_string(h_arg.getValue()) + "x" + std::to_string(w_arg.getValue()) + "_" + std::to_string(p_arg.getValue()) + "x" + std::to_string(q_arg.getValue()) + "_" + std::to_string(c_arg.getValue()) + "_" + std::to_string(r_arg.getValue()) + "_" + std::to_string(s_arg.getValue()) + "_" +
            std::to_string(h_stride_arg.getValue()) + "_" + std::to_string(w_stride_arg.getValue())
    );
}