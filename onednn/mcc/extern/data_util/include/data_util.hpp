#ifndef DATA_UTIL_HPP
#define DATA_UTIL_HPP

#include <string>
#include <vector>
#include <cmath>
#include <stdexcept>

// trim from start (in place)
static inline void ltrim(std::string &s);

// trim from end (in place)
static inline void rtrim(std::string &s);

// trim from both ends (in place)
static inline void trim(std::string &s);

// trim from start (copying)
static inline std::string ltrim_copy(std::string s);

// trim from end (copying)
static inline std::string rtrim_copy(std::string s);

// trim from both ends (copying)
static inline std::string trim_copy(std::string s);

std::string data_directory(const std::vector<std::string> &parts);

template <typename... T>
std::string data_directory(const T&... parts) {
    return data_directory(std::vector<std::string>({parts...}));
}

void prepare_data_directory(const std::string &path, bool allow_exiting = false);

template <typename T>
T data_sequence(size_t nr) {
    return static_cast<T>((nr % 10) + 1);
}

bool starts_with(const std::string &value, const std::string &prefix);
bool ends_with(const std::string &value, const std::string &suffix);

// assumes that values is sorted
template<typename T>
size_t get_quantile_index(const std::vector<T> &values, float quantile) {
    if (values.empty()) throw std::runtime_error("not enough values");
    size_t index = round((values.size() - 1) * (quantile));
    return index;
}

// assumes that values is sorted
template<typename T>
size_t get_percentile_index(const std::vector<T> &values, int percentile) {
    if (percentile < 0 || percentile > 100) throw std::runtime_error("percentile has to be in [0,100]");
    return get_quantile_index(values, percentile / 100.0f);
}

// assumes that values is sorted
template<typename T>
std::pair<size_t, size_t> get_confidence_interval_indices(const std::vector<T> &values, size_t index, float percent) {
    if (index >= values.size()) throw std::runtime_error("index out of values' bounds");
    size_t interval_width = ceil(values.size() * percent);
    if (index < interval_width / 2 || index + interval_width / 2 >= values.size())
        throw std::runtime_error("confidence interval out of values' bounds");
    return std::make_pair(index - interval_width / 2, index + interval_width / 2);
}

#endif //DATA_UTIL_HPP
