#!/usr/bin/env bash

##################################################
# Hardware Selection
##################################################
export ENABLE_GPU=1
export ENABLE_CPU=1

##################################################
# Framework Selection
##################################################
export ENABLE_MDH=1
export ENABLE_TVM=1
export ENABLE_CUBLAS=1
export ENABLE_CUDNN=1
export ENABLE_ONEMKL=1
export ENABLE_ONEDNN=1

##################################################
# Experiment Selection
##################################################
export ENABLE_RESNET50_TRAINING_MCC=1
export ENABLE_RESNET50_TRAINING_MATMUL=1
export ENABLE_RESNET50_INFERENCE_MCC=1
export ENABLE_RESNET50_INFERENCE_MATMUL=1

export ENABLE_VGG16_TRAINING_MCC=1
export ENABLE_VGG16_TRAINING_MATMUL=1
export ENABLE_VGG16_INFERENCE_MCC=1
export ENABLE_VGG16_INFERENCE_MATMUL=1

export ENABLE_MOBILENET_TRAINING_MCC=1
export ENABLE_MOBILENET_INFERENCE_MCC=1

##################################################
# Custom Dependency Paths
##################################################

# TVM path (set TVM_HOME and uncomment these lines, if you built TVM from source)
export TVM_HOME=/opt/tvm
export PYTHONPATH=$TVM_HOME/python:$PYTHONPATH

# NVIDIA HPC SDK
export NVHPC_DIR=/opt/nvidia/hpc_sdk/Linux_x86_64/22.1/

# Intel oneAPI Base Toolkit
export ONEAPI_DIR=/opt/intel/oneapi/
export OpenCL_INCLUDE_DIR=/opt/intel/oneapi/



export ARTIFACT_ROOT=`pwd`
