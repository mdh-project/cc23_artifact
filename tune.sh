#!/usr/bin/env bash

# change this line, if you want to change the tuning time per framework and routine
TUNING_TIME_IN_MINUTES=720

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }

TVM_ITERATIONS_PER_CALL=512

if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
    if [[ -n "$ENABLE_MDH" && "$ENABLE_MDH" -eq 1 ]]; then
        # MDH (ResNet-50 Training MCC)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 0 ]] && (
          echo "Tuning MDH ResNet-50 Training MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_2_asymmetrical/static/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical --path $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 7 --input-size-r-3 7 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_2_asymmetrical/static/* $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/kernel_1.cuda
        )
        # MDH (ResNet-50 Training MatMul)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 1 ]] && (
          echo "Tuning MDH ResNet-50 Training MatMul for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/matmul/static/matmul_skip_pp_l1_l2_r1_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 1000 --input-size-r-1 2048 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-r-1 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_matmul [flags]"

          cp ../kernel/md_hom/blocked_v3/matmul/static/* $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/kernel_1.cuda
        )
        # MDH (ResNet-50 Inference MCC)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 2 ]] && (
          echo "Tuning MDH ResNet-50 Inference MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_2_asymmetrical/static/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical --path $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 7 --input-size-r-3 7 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_2_asymmetrical/static/* $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/kernel_1.cuda
        )
        # MDH (ResNet-50 Inference MatMul)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 3 ]] && (
          echo "Tuning MDH ResNet-50 Inference MatMul for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/matmul/static/matmul_skip_pp_l1_l2_r1_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 1000 --input-size-r-1 2048 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-r-1 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_matmul [flags]"

          cp ../kernel/md_hom/blocked_v3/matmul/static/* $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/kernel_1.cuda
        )
        # MDH (VGG-16 Training MCC)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 4 ]] && (
          echo "Tuning MDH VGG-16 Training MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_1/static/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 --path $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 224 --input-size-l-3 224 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_1/static/* $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/kernel_1.cuda
        )
        # MDH (VGG-16 Training MatMul)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 5 ]] && (
          echo "Tuning MDH VGG-16 Training MatMul for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/matmul/static/matmul_skip_pp_l1_l2_r1_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 4096 --input-size-r-1 25088 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-r-1 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_matmul [flags]"

          cp ../kernel/md_hom/blocked_v3/matmul/static/* $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/kernel_1.cuda
        )
        # MDH (VGG-16 Inference MCC)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 6 ]] && (
          echo "Tuning MDH VGG-16 Inference MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_1/static/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 --path $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 224 --input-size-l-3 224 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_1/static/* $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/kernel_1.cuda
        )
        # MDH (VGG-16 Inference MatMul)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 7 ]] && (
          echo "Tuning MDH VGG-16 Inference MatMul for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/matmul/static/matmul_skip_pp_l1_l2_r1_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 4096 --input-size-r-1 25088 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-r-1 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_matmul [flags]"

          cp ../kernel/md_hom/blocked_v3/matmul/static/* $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/kernel_1.cuda
        )
        # MDH (MobileNet Training MCC)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 8 ]] && (
          echo "Tuning MDH MobileNet Training MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_2/static/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 --path $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 32 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_2/static/* $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/kernel_1.cuda
        )
        # MDH (MobileNet Inference MCC)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 9 ]] && (
          echo "Tuning MDH MobileNet Inference MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_2/static/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cuda_preprocessed.cuda &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 --path $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 32 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_2/static/* $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda
          mv $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cuda_preprocessed.cuda $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/kernel_1.cuda
        )
    fi

    if [[ -n "$ENABLE_TVM" && "$ENABLE_TVM" -eq 1 ]]; then
        # TVM (ResNet-50 Training MCC)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 10 ]] && (
          echo "Tuning TVM ResNet-50 Training MCC for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 64 3 230 230 7 7 112 112 2
          done
        )
        # TVM (ResNet-50 Training MatMul)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 11 ]] && (
          echo "Tuning TVM ResNet-50 Training MatMul for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL matmul 16 1000 2048
          done
        )
        # TVM (ResNet-50 Inference MCC)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 12 ]] && (
          echo "Tuning TVM ResNet-50 Inference MCC for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 64 3 230 230 7 7 112 112 2
          done
        )
        # TVM (ResNet-50 Inference MatMul)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 13 ]] && (
          echo "Tuning TVM ResNet-50 Inference MatMul for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL matmul 1 1000 2048
          done
        )
        # TVM (VGG-16 Training MCC)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 14 ]] && (
          echo "Tuning TVM VGG-16 Training MCC for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 64 3 226 226 3 3 224 224 1
          done
        )
        # TVM (VGG-16 Training MatMul)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 15 ]] && (
          echo "Tuning TVM VGG-16 Training MatMul for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL matmul 16 4096 25088
          done
        )
        # TVM (VGG-16 Inference MCC)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 16 ]] && (
          echo "Tuning TVM VGG-16 Inference MCC for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 64 3 226 226 3 3 224 224 1
          done
        )
        # TVM (VGG-16 Inference MatMul)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 17 ]] && (
          echo "Tuning TVM VGG-16 Inference MatMul for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL matmul 1 4096 25088
          done
        )
        # TVM (MobileNet Training MCC)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 18 ]] && (
          echo "Tuning TVM MobileNet Training MCC for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 32 3 225 225 3 3 112 112 2
          done
        )
        # TVM (MobileNet Inference MCC)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 19 ]] && (
          echo "Tuning TVM MobileNet Inference MCC for GPU"
          rm -rf $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/tvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/tvm/ cuda gpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 32 3 225 225 3 3 112 112 2
          done
        )
    fi
fi

if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
    if [[ -n "$ENABLE_MDH" && "$ENABLE_MDH" -eq 1 ]]; then
        # MDH (ResNet-50 Training MCC)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 20 ]] && (
          echo "Tuning MDH ResNet-50 Training MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_2_asymmetrical/static/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical --path $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 7 --input-size-r-3 7 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_2_asymmetrical/static/* $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/kernel_1.cl
        )
        # MDH (ResNet-50 Training MatMul)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 21 ]] && (
          echo "Tuning MDH ResNet-50 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/matmul/static/matmul_skip_pp_l1_l2_r1_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 1000 --input-size-r-1 2048 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-r-1 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_matmul [flags]"

          cp ../kernel/md_hom/blocked_v3/matmul/static/* $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/kernel_1.cl
        )
        # MDH (ResNet-50 Inference MCC)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 22 ]] && (
          echo "Tuning MDH ResNet-50 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_2_asymmetrical/static/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical --path $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 7 --input-size-r-3 7 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_2_asymmetrical/static/* $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_asymmetrical_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/kernel_1.cl
        )
        # MDH (ResNet-50 Inference MatMul)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 23 ]] && (
          echo "Tuning MDH ResNet-50 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/matmul/static/matmul_skip_pp_l1_l2_r1_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 1000 --input-size-r-1 2048 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-r-1 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_matmul [flags]"

          cp ../kernel/md_hom/blocked_v3/matmul/static/* $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/kernel_1.cl
        )
        # MDH (VGG-16 Training MCC)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 24 ]] && (
          echo "Tuning MDH VGG-16 Training MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_1/static/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 --path $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 224 --input-size-l-3 224 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_1/static/* $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/kernel_1.cl
        )
        # MDH (VGG-16 Training MatMul)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 25 ]] && (
          echo "Tuning MDH VGG-16 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/matmul/static/matmul_skip_pp_l1_l2_r1_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 4096 --input-size-r-1 25088 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-r-1 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_matmul [flags]"

          cp ../kernel/md_hom/blocked_v3/matmul/static/* $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/kernel_1.cl
        )
        # MDH (VGG-16 Inference MCC)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 26 ]] && (
          echo "Tuning MDH VGG-16 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_1/static/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 --path $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 224 --input-size-l-3 224 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_1/static/* $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_1_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/kernel_1.cl
        )
        # MDH (VGG-16 Inference MatMul)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 27 ]] && (
          echo "Tuning MDH VGG-16 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/matmul/static/matmul_skip_pp_l1_l2_r1_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 4096 --input-size-r-1 25088 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-r-1 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_matmul [flags]"

          cp ../kernel/md_hom/blocked_v3/matmul/static/* $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/matmul_skip_pp_l1_l2_r1_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/kernel_1.cl
        )
        # MDH (MobileNet Training MCC)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 28 ]] && (
          echo "Tuning MDH MobileNet Training MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_2/static/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 --path $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 32 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_2/static/* $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/kernel_1.cl
        )
        # MDH (MobileNet Inference MCC)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 29 ]] && (
          echo "Tuning MDH MobileNet Inference MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build || exit 1

          rm -rf $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/ &> /dev/null
          rm ../kernel/md_hom/strided_v3/mcc_nhwc_krsc_npqk_stride_2/static/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_?.cl_preprocessed.cl &> /dev/null

          ./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 --path $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 32 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r no-pp-r-2 -r no-pp-r-3 -r fixed_res_mem --wrapper-type local --wrapper-command "./tune_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 [flags]"

          cp ../kernel/md_hom/blocked_v3/mcc_nhwc_krsc_npqk_stride_2/static/* $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/
          source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/tuned_configuration $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl
          mv $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/mcc_nhwc_krsc_npqk_stride_2_skip_pp_l1_l2_l3_l4_r1_r2_r3_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/kernel_1.cl
        )
    fi

    if [[ -n "$ENABLE_TVM" && "$ENABLE_TVM" -eq 1 ]]; then
        # TVM (ResNet-50 Training MCC, OpenCL)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 30 ]] && (
          echo "Tuning TVM (OpenCL) ResNet-50 Training MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 64 3 230 230 7 7 112 112 2
          done
        )
        # TVM (ResNet-50 Training MatMul, OpenCL)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 31 ]] && (
          echo "Tuning TVM (OpenCL) ResNet-50 Training MatMul for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL matmul 16 1000 2048
          done
        )
        # TVM (ResNet-50 Inference MCC, OpenCL)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 32 ]] && (
          echo "Tuning TVM (OpenCL) ResNet-50 Inference MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 64 3 230 230 7 7 112 112 2
          done
        )
        # TVM (ResNet-50 Inference MatMul, OpenCL)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 33 ]] && (
          echo "Tuning TVM (OpenCL) ResNet-50 Inference MatMul for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL matmul 1 1000 2048
          done
        )
        # TVM (VGG-16 Training MCC, OpenCL)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 34 ]] && (
          echo "Tuning TVM (OpenCL) VGG-16 Training MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 64 3 226 226 3 3 224 224 1
          done
        )
        # TVM (VGG-16 Training MatMul, OpenCL)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 35 ]] && (
          echo "Tuning TVM (OpenCL) VGG-16 Training MatMul for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL matmul 16 4096 25088
          done
        )
        # TVM (VGG-16 Inference MCC, OpenCL)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 36 ]] && (
          echo "Tuning TVM (OpenCL) VGG-16 Inference MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 64 3 226 226 3 3 224 224 1
          done
        )
        # TVM (VGG-16 Inference MatMul, OpenCL)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 37 ]] && (
          echo "Tuning TVM (OpenCL) VGG-16 Inference MatMul for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL matmul 1 4096 25088
          done
        )
        # TVM (MobileNet Training MCC, OpenCL)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 38 ]] && (
          echo "Tuning TVM (OpenCL) MobileNet Training MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 32 3 225 225 3 3 112 112 2
          done
        )
        # TVM (MobileNet Inference MCC, OpenCL)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 39 ]] && (
          echo "Tuning TVM (OpenCL) MobileNet Inference MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/opencl/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/opencl/ opencl cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 32 3 225 225 3 3 112 112 2
          done
        )

        # TVM (ResNet-50 Training MCC, LLVM)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 40 ]] && (
          echo "Tuning TVM (LLVM) ResNet-50 Training MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 64 3 230 230 7 7 112 112 2
          done
        )
        # TVM (ResNet-50 Training MatMul, LLVM)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 41 ]] && (
          echo "Tuning TVM (LLVM) ResNet-50 Training MatMul for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL matmul 16 1000 2048
          done
        )
        # TVM (ResNet-50 Inference MCC, LLVM)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 42 ]] && (
          echo "Tuning TVM (LLVM) ResNet-50 Inference MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 64 3 230 230 7 7 112 112 2
          done
        )
        # TVM (ResNet-50 Inference MatMul, LLVM)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 43 ]] && (
          echo "Tuning TVM (LLVM) ResNet-50 Inference MatMul for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL matmul 1 1000 2048
          done
        )
        # TVM (VGG-16 Training MCC, LLVM)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 44 ]] && (
          echo "Tuning TVM (LLVM) VGG-16 Training MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 64 3 226 226 3 3 224 224 1
          done
        )
        # TVM (VGG-16 Training MatMul, LLVM)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 45 ]] && (
          echo "Tuning TVM (LLVM) VGG-16 Training MatMul for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL matmul 16 4096 25088
          done
        )
        # TVM (VGG-16 Inference MCC, LLVM)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 46 ]] && (
          echo "Tuning TVM (LLVM) VGG-16 Inference MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 64 3 226 226 3 3 224 224 1
          done
        )
        # TVM (VGG-16 Inference MatMul, LLVM)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 47 ]] && (
          echo "Tuning TVM (LLVM) VGG-16 Inference MatMul for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL matmul 1 4096 25088
          done
        )
        # TVM (MobileNet Training MCC, LLVM)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 48 ]] && (
          echo "Tuning TVM (LLVM) MobileNet Training MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 16 32 3 225 225 3 3 112 112 2
          done
        )
        # TVM (MobileNet Inference MCC, LLVM)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 49 ]] && (
          echo "Tuning TVM (LLVM) MobileNet Inference MCC for CPU"
          rm -rf $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/llvm/ &> /dev/null
          cd $ARTIFACT_ROOT/tvm &&
          START=`date +%s`
          while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
            python tune.py $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/llvm/ llvm cpu 0 $TVM_ITERATIONS_PER_CALL mcc_nhwc_krsc_npqk 1 32 3 225 225 3 3 112 112 2
          done
        )
    fi
fi
