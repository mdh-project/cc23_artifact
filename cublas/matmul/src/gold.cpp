#include "data_util/include/data_util.hpp"
#include "tclap/CmdLine.h"
#include <fstream>

#include "meta_info.cpp"
#include "data_info.cpp"
#include "gold_info.cpp"

int main(int argc, const char **argv) {
    TCLAP::CmdLine cmd("Calculate gold results.");

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    // prepare directory for gold results
    std::string application = "cuBLAS";
    std::string routine = "sgemm_row_major";
    std::vector<std::string> path = {"gold", application, routine};
    gold_data_path(path);
    std::string data_dir = "../" + data_directory(path);
    prepare_data_directory(data_dir);

    // calculate gold result
    init_data();
    calculate_gold(data_dir);
}