void calculate_gold(const std::string &data_dir) {
    std::ofstream gold_file(data_dir + "/gold.tsv", std::ios::out | std::ios::trunc);

    auto a_mat = [&] (int i, int k) {
        if (transpose_a_arg.getValue()) {
            return a[k * m_arg.getValue() + i];
        } else {
            return a[i * k_arg.getValue() + k];
        }
    };
    auto b_mat = [&] (int k, int j) {
        if (transpose_b_arg.getValue()) {
            return b[j * k_arg.getValue() + k];
        } else {
            return b[k * n_arg.getValue() + j];
        }
    };

    for (size_t i = 0; i < m_arg.getValue(); ++i) {
        for (size_t j = 0; j < n_arg.getValue(); ++j) {
            float acc = 0;
            for (size_t k = 0; k < k_arg.getValue(); ++k) {
                acc += a_mat(i, k) * b_mat(k, j);
            }
            gold_file << (alpha_arg.getValue() * acc + beta_arg.getValue() * c[i * n_arg.getValue() + j]) << "\t";
        }
        gold_file << std::endl;
    }
    gold_file.close();
}