#include "data_util/include/data_util.hpp"
#include "tclap/CmdLine.h"
#include "json/json.hpp"
using json = nlohmann::json;

#include <algorithm>
#include <utility>
#include <fstream>

#include <cuda_runtime.h>
#include <cublas_v2.h>

#define CUDA_SAFE_CALL(x)                                   \
do {                                                        \
    cudaError_t cuda_call_result = x;                       \
    if (cuda_call_result != cudaSuccess) {                  \
        std::cerr << "error: " #x " failed with error "     \
                  << cudaGetErrorString(cuda_call_result);  \
        exit(1);                                            \
    }                                                       \
} while(false)

const char *cublasGetErrorString(cublasStatus_t status);

#define CUBLAS_SAFE_CALL(x)                                 \
do {                                                        \
    cublasStatus_t cublas_call_result = x;                  \
    if (cublas_call_result != CUBLAS_STATUS_SUCCESS) {      \
        std::stringstream error;                            \
        error << "error: " #x " failed with error ";        \
        error << cublasGetErrorString(cublas_call_result);  \
        std::cerr << error.str() << "\n";                   \
        exit(1);                                            \
    }                                                       \
} while(false)

const char *cublasGetErrorString(cublasStatus_t status) {
    switch (status) {
        case CUBLAS_STATUS_SUCCESS:
            return "CUBLAS_STATUS_SUCCESS";
        case CUBLAS_STATUS_NOT_INITIALIZED:
            return "CUBLAS_STATUS_NOT_INITIALIZED";
        case CUBLAS_STATUS_ALLOC_FAILED:
            return "CUBLAS_STATUS_ALLOC_FAILED";
        case CUBLAS_STATUS_INVALID_VALUE:
            return "CUBLAS_STATUS_INVALID_VALUE";
        case CUBLAS_STATUS_ARCH_MISMATCH:
            return "CUBLAS_STATUS_ARCH_MISMATCH";
        case CUBLAS_STATUS_MAPPING_ERROR:
            return "CUBLAS_STATUS_MAPPING_ERROR";
        case CUBLAS_STATUS_EXECUTION_FAILED:
            return "CUBLAS_STATUS_EXECUTION_FAILED";
        case CUBLAS_STATUS_INTERNAL_ERROR:
            return "CUBLAS_STATUS_INTERNAL_ERROR";
        case CUBLAS_STATUS_NOT_SUPPORTED:
            return "CUBLAS_STATUS_NOT_SUPPORTED";
        case CUBLAS_STATUS_LICENSE_ERROR:
            return "CUBLAS_STATUS_LICENSE_ERROR";
    }
    return "unknown error";
}

#include "meta_info.cpp"
#include "data_info.cpp"
#include "bench_info.cpp"

int main(int argc, const char **argv) {
    TCLAP::CmdLine cmd("Benchmark cuBLAS routines.");

    TCLAP::ValueArg<int> device_id_arg("d", "device-id", "CUDA device id.", false, 0, "int");
    cmd.add(device_id_arg);

    TCLAP::ValueArg<int> warm_ups_arg("w", "warm-ups", "Number of warm ups.", false, 10, "int");
    cmd.add(warm_ups_arg);

    TCLAP::ValueArg<int> evaluations_arg("e", "evaluations", "Number of evaluations.", false, 200, "int");
    cmd.add(evaluations_arg);

    TCLAP::ValueArg<std::string> gold_file_arg("g", "gold-file", "Gold file for result check.", false, "", "string");
    cmd.add(gold_file_arg);

    TCLAP::ValueArg<std::string> path_suffix_arg("x", "suffix", "Data path suffix.", false, "", "string");
    cmd.add(path_suffix_arg);

    TCLAP::ValueArg<std::string> full_path_arg("", "path", "Data path.", false, "", "string");
    cmd.add(full_path_arg);

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    // set device
    CUDA_SAFE_CALL(cudaSetDevice(device_id_arg.getValue()));

    // create cuBLAS handle
    cublasHandle_t handle;
    CUBLAS_SAFE_CALL(cublasCreate(&handle));

    // prepare directory for tuning results
    struct cudaDeviceProp device_props{};
    CUDA_SAFE_CALL(cudaGetDeviceProperties(&device_props, device_id_arg.getValue()));
    std::string dev_name = device_props.name;
    int version_id;
    CUBLAS_SAFE_CALL(cublasGetVersion(handle, &version_id));
    std::string version = std::to_string(version_id);
    std::string application = "cuBLAS";
    std::string routine = "sgemm_row_major";
    std::string data_dir = full_path_arg.getValue();
    if (data_dir.empty()) {
        std::vector<std::string> path = {dev_name, application, version, routine};
        benchmark_data_path(path);
        if (!path_suffix_arg.getValue().empty()) path.emplace_back(path_suffix_arg.getValue());
        data_dir = "../" + data_directory(path);
    }
    prepare_data_directory(data_dir, true);

    // allocate buffers
    init_data();
    alloc_buffers();

    std::map<std::string, std::map<std::string, std::vector<long long>>> runtimes;
    std::map<std::string, std::string> error_status;
    for (int algorithm = 0; algorithm < 1; ++algorithm) {
        // check if algorithm is supported
        if (exec_api_call(handle, algorithm))
            continue;

        // warm ups
        runtimes[algorithm_name(algorithm)]["warm ups"] = {};
        for (int i = 0; i < warm_ups_arg.getValue(); ++i) {
            cudaEvent_t start, stop;
            CUDA_SAFE_CALL(cudaEventCreate(&start));
            CUDA_SAFE_CALL(cudaEventCreate(&stop));
            CUDA_SAFE_CALL(cudaEventRecord(start));
            exec_api_call(handle, algorithm);
            CUDA_SAFE_CALL(cudaEventRecord(stop));
            CUDA_SAFE_CALL(cudaEventSynchronize(stop));
            float runtime = std::numeric_limits<float>::max();
            CUDA_SAFE_CALL(cudaEventElapsedTime(&runtime, start, stop));
            runtimes[algorithm_name(algorithm)]["warm ups"].push_back(static_cast<long long>(runtime * 1000000));
            CUDA_SAFE_CALL(cudaEventDestroy(start));
            CUDA_SAFE_CALL(cudaEventDestroy(stop));
        }

        // evaluations
        runtimes[algorithm_name(algorithm)]["evaluations"] = {};
        for (int i = 0; i < evaluations_arg.getValue(); ++i) {
            cudaEvent_t start, stop;
            CUDA_SAFE_CALL(cudaEventCreate(&start));
            CUDA_SAFE_CALL(cudaEventCreate(&stop));
            CUDA_SAFE_CALL(cudaEventRecord(start));
            exec_api_call(handle, algorithm);
            CUDA_SAFE_CALL(cudaEventRecord(stop));
            CUDA_SAFE_CALL(cudaEventSynchronize(stop));
            float runtime = std::numeric_limits<float>::max();
            CUDA_SAFE_CALL(cudaEventElapsedTime(&runtime, start, stop));
            runtimes[algorithm_name(algorithm)]["evaluations"].push_back(static_cast<long long>(runtime * 1000000));
            CUDA_SAFE_CALL(cudaEventDestroy(start));
            CUDA_SAFE_CALL(cudaEventDestroy(stop));
        }

        // check results
        if (gold_file_arg.getValue().empty()) {
            error_status[algorithm_name(algorithm)] = "not checked";
        } else {
            // check result
            if (result_correct(gold_file_arg.getValue())) {
                error_status[algorithm_name(algorithm)] = "no errors found";
            } else {
                error_status[algorithm_name(algorithm)] = "errors found";
            }
        }
    }

    // free buffers
    free_buffers();

    // write profiling data to files
    json meta_json;
    meta_json["warm ups"] = std::to_string(warm_ups_arg.getValue());
    meta_json["evaluations"] = std::to_string(evaluations_arg.getValue());
    meta_json["gold file"] = gold_file_arg.getValue();
    meta_json["error status"] = error_status;
    meta_json["cuBLAS version"] = version;
    std::ofstream meta_file(data_dir + "/benchmark_meta", std::ios::out | std::ios::trunc);
    meta_file << std::setw(4) << meta_json;
    meta_file.close();
    json runtimes_json;
    runtimes_json["runtimes"] = runtimes;
    long long runtimes_min = std::numeric_limits<long long>::max();
    long long runtimes_median = 0;
    long long runtimes_average = 0;
    long long runtimes_max = 0;
    for (auto &warm_ups_and_evaluations : runtimes) {
        long long tmp_runtimes_min = *std::min_element(warm_ups_and_evaluations.second["evaluations"].begin(), warm_ups_and_evaluations.second["evaluations"].end());
        std::nth_element(warm_ups_and_evaluations.second["evaluations"].begin(),
                         warm_ups_and_evaluations.second["evaluations"].begin() + (warm_ups_and_evaluations.second["evaluations"].size() / 2),
                         warm_ups_and_evaluations.second["evaluations"].end());
        long long tmp_runtimes_median = warm_ups_and_evaluations.second["evaluations"][warm_ups_and_evaluations.second["evaluations"].size() / 2];
        long long tmp_runtimes_average = std::accumulate(warm_ups_and_evaluations.second["evaluations"].begin(), warm_ups_and_evaluations.second["evaluations"].end(), 0ll) / warm_ups_and_evaluations.second["evaluations"].size();
        long long tmp_runtimes_max = *std::max_element(warm_ups_and_evaluations.second["evaluations"].begin(), warm_ups_and_evaluations.second["evaluations"].end());
        if (tmp_runtimes_min < runtimes_min) {
            runtimes_min = tmp_runtimes_min;
            runtimes_median = tmp_runtimes_median;
            runtimes_average = tmp_runtimes_average;
            runtimes_max = tmp_runtimes_max;
        }
        std::ofstream runtimes_min_file(data_dir + "/runtimes_min_" + warm_ups_and_evaluations.first, std::ios::out | std::ios::trunc);
        runtimes_min_file << tmp_runtimes_min;
        runtimes_min_file.close();
        std::ofstream runtimes_median_file(data_dir + "/runtimes_median_" + warm_ups_and_evaluations.first, std::ios::out | std::ios::trunc);
        runtimes_median_file << tmp_runtimes_median;
        runtimes_median_file.close();
        std::ofstream runtimes_average_file(data_dir + "/runtimes_average_" + warm_ups_and_evaluations.first, std::ios::out | std::ios::trunc);
        runtimes_average_file << tmp_runtimes_average;
        runtimes_average_file.close();
        std::ofstream runtimes_max_file(data_dir + "/runtimes_max_" + warm_ups_and_evaluations.first, std::ios::out | std::ios::trunc);
        runtimes_max_file << tmp_runtimes_max;
        runtimes_max_file.close();
    }
    std::ofstream runtimes_file(data_dir + "/runtimes", std::ios::out | std::ios::trunc);
    runtimes_file << std::setw(4) << json(runtimes_json);
    runtimes_file.close();
    std::ofstream runtimes_min_file(data_dir + "/runtimes_min", std::ios::out | std::ios::trunc);
    runtimes_min_file << runtimes_min;
    runtimes_min_file.close();
    std::ofstream runtimes_median_file(data_dir + "/runtimes_median", std::ios::out | std::ios::trunc);
    runtimes_median_file << runtimes_median;
    runtimes_median_file.close();
    std::ofstream runtimes_average_file(data_dir + "/runtimes_average", std::ios::out | std::ios::trunc);
    runtimes_average_file << runtimes_average;
    runtimes_average_file.close();
    std::ofstream runtimes_max_file(data_dir + "/runtimes_max", std::ios::out | std::ios::trunc);
    runtimes_max_file << runtimes_max;
    runtimes_max_file.close();
}

