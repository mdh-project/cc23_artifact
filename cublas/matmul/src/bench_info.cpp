float* dev_a;
float* dev_b;
float* dev_c;

void alloc_buffers() {
    CUDA_SAFE_CALL(cudaMalloc(&dev_a, a.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMalloc(&dev_b, b.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMalloc(&dev_c, c.size() * sizeof(float)));

    CUBLAS_SAFE_CALL(cublasSetMatrix(k_arg.getValue(), m_arg.getValue(), sizeof(float), a.data(), k_arg.getValue(), dev_a, k_arg.getValue()));
    CUBLAS_SAFE_CALL(cublasSetMatrix(n_arg.getValue(), k_arg.getValue(), sizeof(float), b.data(), n_arg.getValue(), dev_b, n_arg.getValue()));
    CUBLAS_SAFE_CALL(cublasSetMatrix(n_arg.getValue(), m_arg.getValue(), sizeof(float), c.data(), n_arg.getValue(), dev_c, n_arg.getValue()));

    CUDA_SAFE_CALL(cudaDeviceSynchronize());
}

bool exec_api_call(cublasHandle_t &handle, int algo) {
    // NOTE: M, N, K, and a, b are swapped when passed to cublasSgemm, because it expects and produces column-major matrices
    CUBLAS_SAFE_CALL(cublasSgemm(handle, transpose_b_arg.getValue() ? CUBLAS_OP_T : CUBLAS_OP_N, transpose_a_arg.getValue() ? CUBLAS_OP_T : CUBLAS_OP_N,
                                 n_arg.getValue(), m_arg.getValue(), k_arg.getValue(), &alpha_arg.getValue(),
                                 dev_b, transpose_b_arg.getValue() ? k_arg.getValue() : n_arg.getValue(),
                                 dev_a, transpose_a_arg.getValue() ? m_arg.getValue() : k_arg.getValue(),
                                 &beta_arg.getValue(),
                                 dev_c, n_arg.getValue()));
    return false;
}

bool result_correct(const std::string &gold_file) {
    std::ifstream is(gold_file);
    std::istream_iterator<float> start(is), end;
    std::vector<float> c_gold(start, end);

    CUBLAS_SAFE_CALL(cublasGetMatrix(n_arg.getValue(), m_arg.getValue(), sizeof(float), dev_c, n_arg.getValue(), c.data(), n_arg.getValue()));

    CUDA_SAFE_CALL(cudaDeviceSynchronize());

    return !memcmp(c.data(), c_gold.data(), c_gold.size() * sizeof(float));
}

void free_buffers() {
    CUDA_SAFE_CALL(cudaFree(dev_a));
    CUDA_SAFE_CALL(cudaFree(dev_b));
    CUDA_SAFE_CALL(cudaFree(dev_c));
}