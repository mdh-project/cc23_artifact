std::vector<float> images;
std::vector<float> filter;
std::vector<float> out;

void init_data() {
    images = std::vector<float>(n_arg.getValue() * h_arg.getValue() * w_arg.getValue() * c_arg.getValue()); for (int i = 0; i < images.size(); ++i) images[i] = data_sequence<float>(i);
    filter = std::vector<float>(r_arg.getValue() * s_arg.getValue() * c_arg.getValue() * k_arg.getValue()); for (int i = 0; i < filter.size(); ++i) filter[i] = data_sequence<float>(i);
    out = std::vector<float>(n_arg.getValue() * p_arg.getValue() * q_arg.getValue() * k_arg.getValue()); for (int i = 0; i < out.size(); ++i) out[i] = 0;
}