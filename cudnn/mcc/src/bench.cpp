#include "data_util/include/data_util.hpp"
#include "tclap/CmdLine.h"
#include "json/json.hpp"
using json = nlohmann::json;

#include <algorithm>
#include <utility>
#include <fstream>

#include <cuda_runtime.h>
#include <cudnn.h>

#define CUDA_SAFE_CALL(x)                                   \
do {                                                        \
    cudaError_t cuda_call_result = x;                       \
    if (cuda_call_result != cudaSuccess) {                  \
        std::cerr << "error: " #x " failed with error "     \
                  << cudaGetErrorString(cuda_call_result);  \
        exit(1);                                            \
    }                                                       \
} while(false)

#define CUDNN_SAFE_CALL(expression)                          \
  {                                                          \
    cudnnStatus_t status = (expression);                     \
    if (status != CUDNN_STATUS_SUCCESS) {                    \
      std::cerr << "Error on line " << __LINE__ << ": "      \
                << cudnnGetErrorString(status) << std::endl; \
      std::exit(EXIT_FAILURE);                               \
    }                                                        \
  }

#include "meta_info.cpp"
#include "data_info.cpp"
#include "bench_info.cpp"

int main(int argc, const char **argv) {
    TCLAP::CmdLine cmd("Benchmark cuDNN routines.", ' ', "none", false);

    TCLAP::ValueArg<int> device_id_arg("d", "device-id", "CUDA device id.", false, 0, "int");
    cmd.add(device_id_arg);

    TCLAP::ValueArg<int> warm_ups_arg("", "warm-ups", "Number of warm ups.", false, 10, "int");
    cmd.add(warm_ups_arg);

    TCLAP::ValueArg<int> evaluations_arg("", "evaluations", "Number of evaluations.", false, 200, "int");
    cmd.add(evaluations_arg);

    TCLAP::ValueArg<std::string> gold_file_arg("g", "gold-file", "Gold file for result check.", false, "", "string");
    cmd.add(gold_file_arg);

    TCLAP::ValueArg<std::string> path_suffix_arg("x", "suffix", "Data path suffix.", false, "", "string");
    cmd.add(path_suffix_arg);

    TCLAP::ValueArg<std::string> full_path_arg("", "path", "Data path.", false, "", "string");
    cmd.add(full_path_arg);

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    // set device
    CUDA_SAFE_CALL(cudaSetDevice(device_id_arg.getValue()));

    // create cuDNN handle
    cudnnHandle_t handle;
    CUDNN_SAFE_CALL(cudnnCreate(&handle));

    // prepare directory for tuning results
    struct cudaDeviceProp device_props{};
    CUDA_SAFE_CALL(cudaGetDeviceProperties(&device_props, device_id_arg.getValue()));
    std::string dev_name = device_props.name;
    std::string version = std::to_string(cudnnGetVersion());
    std::string application = "cuDNN";
    std::string routine = "nhwc_krsc_npqk_v8";
    std::string data_dir = full_path_arg.getValue();
    if (data_dir.empty()) {
        std::vector<std::string> path = {dev_name, application, version, routine};
        benchmark_data_path(path);
        if (!path_suffix_arg.getValue().empty()) path.emplace_back(path_suffix_arg.getValue());
        data_dir = "../" + data_directory(path);
    }
    prepare_data_directory(data_dir, true);

    // initialize host buffers
    init_data();

    std::map<std::string, std::map<std::string, std::vector<long long>>> runtimes;
    std::string error_status = "not checked";
    for (size_t algorithm = 0; algorithm < 8; ++algorithm) {
        // allocate buffers
        alloc_buffers(handle, algorithm);

        // warm ups
        std::vector<long long> warm_ups;
        for (int i = 0; i < warm_ups_arg.getValue(); ++i) {
            cudaEvent_t start, stop;
            CUDA_SAFE_CALL(cudaEventCreate(&start));
            CUDA_SAFE_CALL(cudaEventCreate(&stop));
            CUDA_SAFE_CALL(cudaEventRecord(start));
            exec_api_call(handle);
            CUDA_SAFE_CALL(cudaEventRecord(stop));
            CUDA_SAFE_CALL(cudaEventSynchronize(stop));
            float runtime = std::numeric_limits<float>::max();
            CUDA_SAFE_CALL(cudaEventElapsedTime(&runtime, start, stop));
            warm_ups.push_back(static_cast<long long>(runtime * 1000000));
            CUDA_SAFE_CALL(cudaEventDestroy(start));
            CUDA_SAFE_CALL(cudaEventDestroy(stop));
        }

        // evaluations
        std::vector<long long> evaluations;
        for (int i = 0; i < evaluations_arg.getValue(); ++i) {
            cudaEvent_t start, stop;
            CUDA_SAFE_CALL(cudaEventCreate(&start));
            CUDA_SAFE_CALL(cudaEventCreate(&stop));
            CUDA_SAFE_CALL(cudaEventRecord(start));
            exec_api_call(handle);
            CUDA_SAFE_CALL(cudaEventRecord(stop));
            CUDA_SAFE_CALL(cudaEventSynchronize(stop));
            float runtime = std::numeric_limits<float>::max();
            CUDA_SAFE_CALL(cudaEventElapsedTime(&runtime, start, stop));
            evaluations.push_back(static_cast<long long>(runtime * 1000000));
            CUDA_SAFE_CALL(cudaEventDestroy(start));
            CUDA_SAFE_CALL(cudaEventDestroy(stop));
        }

        // check results
        if (!gold_file_arg.getValue().empty() && error_status != "errors found") {
            // check result
            if (result_correct(gold_file_arg.getValue())) {
                error_status = "no errors found";
            } else {
                error_status = "errors found";
            }
        }

        // free buffers
        free_buffers();

        // save profiling data
        runtimes[algorithm_name(algorithm)]["warm ups"] = warm_ups;
        runtimes[algorithm_name(algorithm)]["evaluations"] = evaluations;
    }

    // write profiling data to files
    json meta_json;
    meta_json["warm ups"] = std::to_string(warm_ups_arg.getValue());
    meta_json["evaluations"] = std::to_string(evaluations_arg.getValue());
    meta_json["gold file"] = gold_file_arg.getValue();
    meta_json["error status"] = error_status;
    meta_json["cuDNN version"] = version;
    std::ofstream meta_file(data_dir + "/benchmark_meta", std::ios::out | std::ios::trunc);
    meta_file << std::setw(4) << meta_json;
    meta_file.close();
    json runtimes_json;
    runtimes_json["runtimes"] = runtimes;
    long long runtimes_min = std::numeric_limits<long long>::max();
    long long runtimes_median = 0;
    long long runtimes_average = 0;
    long long runtimes_max = 0;
    for (auto &warm_ups_and_evaluations : runtimes) {
        long long tmp_runtimes_min = *std::min_element(warm_ups_and_evaluations.second["evaluations"].begin(), warm_ups_and_evaluations.second["evaluations"].end());
        std::nth_element(warm_ups_and_evaluations.second["evaluations"].begin(),
                         warm_ups_and_evaluations.second["evaluations"].begin() + (warm_ups_and_evaluations.second["evaluations"].size() / 2),
                         warm_ups_and_evaluations.second["evaluations"].end());
        long long tmp_runtimes_median = warm_ups_and_evaluations.second["evaluations"][warm_ups_and_evaluations.second["evaluations"].size() / 2];
        long long tmp_runtimes_average = std::accumulate(warm_ups_and_evaluations.second["evaluations"].begin(), warm_ups_and_evaluations.second["evaluations"].end(), 0ll) / warm_ups_and_evaluations.second["evaluations"].size();
        long long tmp_runtimes_max = *std::max_element(warm_ups_and_evaluations.second["evaluations"].begin(), warm_ups_and_evaluations.second["evaluations"].end());
        if (tmp_runtimes_min < runtimes_min) {
            runtimes_min = tmp_runtimes_min;
            runtimes_median = tmp_runtimes_median;
            runtimes_average = tmp_runtimes_average;
            runtimes_max = tmp_runtimes_max;
        }
        std::ofstream runtimes_min_file(data_dir + "/runtimes_min_" + warm_ups_and_evaluations.first, std::ios::out | std::ios::trunc);
        runtimes_min_file << tmp_runtimes_min;
        runtimes_min_file.close();
        std::ofstream runtimes_median_file(data_dir + "/runtimes_median_" + warm_ups_and_evaluations.first, std::ios::out | std::ios::trunc);
        runtimes_median_file << tmp_runtimes_median;
        runtimes_median_file.close();
        std::ofstream runtimes_average_file(data_dir + "/runtimes_average_" + warm_ups_and_evaluations.first, std::ios::out | std::ios::trunc);
        runtimes_average_file << tmp_runtimes_average;
        runtimes_average_file.close();
        std::ofstream runtimes_max_file(data_dir + "/runtimes_max_" + warm_ups_and_evaluations.first, std::ios::out | std::ios::trunc);
        runtimes_max_file << tmp_runtimes_max;
        runtimes_max_file.close();
    }
    std::ofstream runtimes_file(data_dir + "/runtimes", std::ios::out | std::ios::trunc);
    runtimes_file << std::setw(4) << json(runtimes_json);
    runtimes_file.close();
    std::ofstream runtimes_min_file(data_dir + "/runtimes_min", std::ios::out | std::ios::trunc);
    runtimes_min_file << runtimes_min;
    runtimes_min_file.close();
    std::ofstream runtimes_median_file(data_dir + "/runtimes_median", std::ios::out | std::ios::trunc);
    runtimes_median_file << runtimes_median;
    runtimes_median_file.close();
    std::ofstream runtimes_average_file(data_dir + "/runtimes_average", std::ios::out | std::ios::trunc);
    runtimes_average_file << runtimes_average;
    runtimes_average_file.close();
    std::ofstream runtimes_max_file(data_dir + "/runtimes_max", std::ios::out | std::ios::trunc);
    runtimes_max_file << runtimes_max;
    runtimes_max_file.close();
}

