cudnnTensorDescriptor_t input_descriptor;
cudnnTensorDescriptor_t output_descriptor;
cudnnFilterDescriptor_t filter_descriptor;
cudnnConvolutionDescriptor_t convolution_descriptor;
cudnnConvolutionFwdAlgo_t convolution_algorithm;
size_t workspace_bytes = 0;
void* d_workspace{nullptr};
float* d_filter{nullptr};
float* d_input{nullptr};
float* d_output{nullptr};

std::string algorithm_name(int algorithm_id) {
    switch (algorithm_id) {
        case 0:
            return "CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM";
        case 1:
            return "CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM";
        case 2:
            return "CUDNN_CONVOLUTION_FWD_ALGO_GEMM";
        case 3:
            return "CUDNN_CONVOLUTION_FWD_ALGO_DIRECT";
        case 4:
            return "CUDNN_CONVOLUTION_FWD_ALGO_FFT";
        case 5:
            return "CUDNN_CONVOLUTION_FWD_ALGO_FFT_TILING";
        case 6:
            return "CUDNN_CONVOLUTION_FWD_ALGO_WINOGRAD";
        case 7:
            return "CUDNN_CONVOLUTION_FWD_ALGO_WINOGRAD_NONFUSED";
    }
}

cudnnConvolutionFwdAlgo_t algorithm(int algorithm_id) {
    switch (algorithm_id) {
        case 0:
            return CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;
        case 1:
            return CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM;
        case 2:
            return CUDNN_CONVOLUTION_FWD_ALGO_GEMM;
        case 3:
            return CUDNN_CONVOLUTION_FWD_ALGO_DIRECT;
        case 4:
            return CUDNN_CONVOLUTION_FWD_ALGO_FFT;
        case 5:
            return CUDNN_CONVOLUTION_FWD_ALGO_FFT_TILING;
        case 6:
            return CUDNN_CONVOLUTION_FWD_ALGO_WINOGRAD;
        case 7:
            return CUDNN_CONVOLUTION_FWD_ALGO_WINOGRAD_NONFUSED;
    }
}

void alloc_buffers(cudnnHandle_t &handle, int algorithm_id) {
    // describe input tensor
    CUDNN_SAFE_CALL(cudnnCreateTensorDescriptor(&input_descriptor));
    CUDNN_SAFE_CALL(cudnnSetTensor4dDescriptor(
            input_descriptor,
            CUDNN_TENSOR_NHWC,
            CUDNN_DATA_FLOAT,
            n_arg.getValue(),
            c_arg.getValue(),
            h_arg.getValue(),
            w_arg.getValue()
    ));

    // describe filter tensor
    CUDNN_SAFE_CALL(cudnnCreateFilterDescriptor(&filter_descriptor));
    CUDNN_SAFE_CALL(cudnnSetFilter4dDescriptor(
            filter_descriptor,
            CUDNN_DATA_FLOAT,
            CUDNN_TENSOR_NHWC,
            k_arg.getValue(),
            c_arg.getValue(),
            r_arg.getValue(),
            s_arg.getValue()
    ));

    // describe output tensor
    CUDNN_SAFE_CALL(cudnnCreateTensorDescriptor(&output_descriptor));
    CUDNN_SAFE_CALL(cudnnSetTensor4dDescriptor(
            output_descriptor,
            CUDNN_TENSOR_NHWC,
            CUDNN_DATA_FLOAT,
            n_arg.getValue(),
            k_arg.getValue(),
            p_arg.getValue(),
            q_arg.getValue()
    ));

    // describe convolution
    CUDNN_SAFE_CALL(cudnnCreateConvolutionDescriptor(&convolution_descriptor));
    CUDNN_SAFE_CALL(cudnnSetConvolution2dDescriptor(
            convolution_descriptor,
            h_padding_arg.getValue(),
            w_padding_arg.getValue(),
            h_stride_arg.getValue(),
            w_stride_arg.getValue(),
            1,
            1,
            CUDNN_CROSS_CORRELATION,
            CUDNN_DATA_FLOAT
    ));
    int max_algo_count = 0;
    CUDNN_SAFE_CALL(cudnnGetConvolutionForwardAlgorithmMaxCount(handle, &max_algo_count));
    std::vector<cudnnConvolutionFwdAlgoPerf_t> algorithms(max_algo_count);
    CUDNN_SAFE_CALL(cudnnFindConvolutionForwardAlgorithm(
            handle,
            input_descriptor,
            filter_descriptor,
            convolution_descriptor,
            output_descriptor,
            max_algo_count,
            &max_algo_count,
            algorithms.data()
    ));
    convolution_algorithm = algorithms.front().algo;

    // allocate memory
    CUDNN_SAFE_CALL(cudnnGetConvolutionForwardWorkspaceSize(
            handle,
            input_descriptor,
            filter_descriptor,
            convolution_descriptor,
            output_descriptor,
            convolution_algorithm,
            &workspace_bytes
    ));
    CUDA_SAFE_CALL(cudaMalloc(&d_workspace, workspace_bytes));

    CUDA_SAFE_CALL(cudaMalloc(&d_filter, filter.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMemcpy(d_filter, filter.data(), filter.size() * sizeof(float), cudaMemcpyHostToDevice));

    CUDA_SAFE_CALL(cudaMalloc(&d_input, images.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMemcpy(d_input, images.data(), images.size() * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL(cudaMalloc(&d_output, n_arg.getValue() * p_arg.getValue() * q_arg.getValue() * k_arg.getValue() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMemset(d_output, 0, n_arg.getValue() * p_arg.getValue() * q_arg.getValue() * k_arg.getValue() * sizeof(float)));

    CUDA_SAFE_CALL(cudaDeviceSynchronize());
}

void exec_api_call(cudnnHandle_t &handle) {
    const float alpha = 1.0f, beta = 0.0f;
    CUDNN_SAFE_CALL(cudnnConvolutionForward(
            handle,
            &alpha,
            input_descriptor,
            d_input,
            filter_descriptor,
            d_filter,
            convolution_descriptor,
            convolution_algorithm,
            d_workspace,
            workspace_bytes,
            &beta,
            output_descriptor,
            d_output
    ));
}

bool result_correct(const std::string &gold_file) {
    std::ifstream is(gold_file);
    std::istream_iterator<float> start(is), end;
    std::vector<float> out_gold(start, end);

    CUDA_SAFE_CALL(cudaMemcpy(out.data(), d_output, out.size() * sizeof(float), cudaMemcpyDeviceToHost));

    CUDA_SAFE_CALL(cudaDeviceSynchronize());

    return !memcmp(out.data(), out_gold.data(), out_gold.size() * sizeof(float));
}

void free_buffers() {
    CUDA_SAFE_CALL(cudaFree(d_input));
    CUDA_SAFE_CALL(cudaFree(d_filter));
    CUDA_SAFE_CALL(cudaFree(d_output));
}