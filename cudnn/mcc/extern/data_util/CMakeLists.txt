project(data_util)
cmake_minimum_required(VERSION 3.3)

include_directories(include)
include_directories(extern)

add_library(data_util STATIC src/data_util.cpp)

add_executable(data_util_tool src/data_util_tool.cpp)
target_link_libraries(data_util_tool data_util)