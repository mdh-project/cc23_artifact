import numpy as np
import tvm
from tvm import te, auto_scheduler


@auto_scheduler.register_workload
def func(M, N, K):
    A = te.placeholder((M, K), name='A', dtype='float32')
    B = te.placeholder((K, N), name='B', dtype='float32')

    k = te.reduce_axis((0, K), name='k')
    C = te.compute(
        (M, N),
        lambda i, j: te.sum(A[i, k] * B[k, j], axis=k),
        name='matmul'
    )
    return [A, B, C]


def tvm_buffer(dev, M, N, K):
    A_np = np.random.uniform(size=(M, K)).astype(np.float32)
    B_np = np.random.uniform(size=(K, N)).astype(np.float32)
    A = tvm.nd.array(A_np, device=dev)
    B = tvm.nd.array(B_np, device=dev)
    C = tvm.nd.empty((M, N), device=dev)
    return A, B, C
