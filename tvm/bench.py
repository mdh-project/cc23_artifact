import os, sys

import numpy as np
import tvm
from tvm import te, auto_scheduler
import importlib

data_dir = sys.argv[1]
target_name = sys.argv[2].lower()
device_type = sys.argv[3].lower()
device_id = int(sys.argv[4])
routine = sys.argv[5]
routine_args = tuple(map(int, sys.argv[6:]))

routine_module = importlib.import_module(f'{routine}')

if target_name == 'opencl':
    dev = tvm.opencl(device_id)
    device_name = dev.device_name
    target = tvm.target.Target('opencl')
    max_shared_memory_per_block = dev.max_shared_memory_per_block
    # There is no explicit local memory limition
    # so we can use INT32_MAX to disalbe the check on local_memory.
    max_local_memory_per_block = 2147483647 # INT32_MAX
    max_threads_per_block = dev.max_threads_per_block
    max_vthread_extent = int(dev.warp_size / 4) if int(dev.warp_size / 4) > 1 else dev.warp_size
    warp_size = dev.warp_size
    hardware_params = auto_scheduler.HardwareParams(-1, 16, 64,
                                                    max_shared_memory_per_block, max_local_memory_per_block,
                                                    max_threads_per_block, max_vthread_extent, warp_size)
elif target_name == 'llvm':
    if device_type != 'cpu':
        raise ValueError('llvm target only supports cpu')
    dev = tvm.cpu(device_id)

    # get cpu name
    def get_processor_name():
        import os, platform, subprocess, re
        if platform.system() == 'Windows':
            return platform.processor()
        elif platform.system() == 'Darwin':
            os.environ['PATH'] = os.environ['PATH'] + os.pathsep + '/usr/sbin'
            command ='sysctl -n machdep.cpu.brand_string'
            return subprocess.check_output(command).strip()
        elif platform.system() == 'Linux':
            command = 'cat /proc/cpuinfo'
            all_info = str(subprocess.check_output(command, shell=True).strip())
            for line in all_info.split('\\n'):
                if 'model name' in line:
                    return re.sub( '.*model name.*:', '', line,1).strip()
        raise ValueError('unable to get cpu name')
    device_name = get_processor_name()

    target_spec = 'llvm'
    if device_name == 'Intel(R) Xeon(R) Gold 6140 CPU @ 2.30GHz':
        target_spec += ' -mcpu=skylake-avx512'
    elif device_name == 'Intel(R) Xeon(R) CPU E5-2683 v4 @ 2.10GHz':
        target_spec += ' -mcpu=core-avx2'
    print(target_spec)

    target = tvm.target.Target(target_spec)
elif target_name == 'cuda':
    if device_type != 'gpu':
        raise ValueError('cuda target only supports gpu')
    dev = tvm.cuda(device_id)
    dev.sync()  # sync device to initialize it and be able to query the device name
    device_name = dev.device_name
    target = tvm.target.Target('cuda')
else:
    raise ValueError('unknown target ' + target_name)

print('Benchmarking for target: ' + target_name)
print('Benchmarking for device: ' + device_name)

logfile = f'{data_dir}/log.json'
schedulefile = f'{data_dir}/schedule'
runtimefile = f'{data_dir}/runtime'
kernelfile = f'{data_dir}/kernel_{{}}'

if target_name == 'opencl':
    task = auto_scheduler.SearchTask(func=routine_module.func, args=routine_args, target=target, hardware_params=hardware_params)
else:
    task = auto_scheduler.SearchTask(func=routine_module.func, args=routine_args, target=target)

# Apply the best schedule
sch, args = task.apply_best(logfile)

func = tvm.build(sch, args, target)

# Evaluate execution time (real measurements done using OpenCL/CUDA events via LD_PRELOAD)
tvm_buffer = routine_module.tvm_buffer(dev, *routine_args)
evaluator = func.time_evaluator(func.entry_name, dev, repeat=610 if target_name == 'llvm' else 1)
min_runtime_ns = int(np.min(evaluator(*tvm_buffer).results) * 1000000000)
print(
    'Execution time of this operator: %f ns'
    % min_runtime_ns
)

# write schedule file
with open(schedulefile, 'w') as f:
    f.write(task.print_best(logfile, print_mode="schedule"))

# write runtime file
with open(runtimefile, 'w') as f:
    f.write(str(min_runtime_ns))

# write kernel file
with open(kernelfile.format(''), 'w') as file:
    file.write(func.get_source())
for i, imported_mod in enumerate(func.imported_modules):
    with open(kernelfile.format(i), 'w') as file:
        file.write(imported_mod.get_source())