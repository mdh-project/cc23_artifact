import os, sys

import numpy as np
import tvm
from tvm import te, auto_scheduler
import importlib

data_dir = sys.argv[1]
target_name = sys.argv[2].lower()
device_type = sys.argv[3].lower()
device_id = int(sys.argv[4])
trials = int(sys.argv[5])
routine = sys.argv[6]
routine_args = tuple(map(int, sys.argv[7:]))

routine_module = importlib.import_module(f'{routine}')

if target_name == 'opencl':
    dev = tvm.opencl(device_id)
    device_name = dev.device_name
    target = tvm.target.Target('opencl')
    max_shared_memory_per_block = dev.max_shared_memory_per_block
    # There is no explicit local memory limition
    # so we can use INT32_MAX to disalbe the check on local_memory.
    max_local_memory_per_block = 2147483647 # INT32_MAX
    max_threads_per_block = dev.max_threads_per_block
    max_vthread_extent = int(dev.warp_size / 4) if int(dev.warp_size / 4) > 1 else dev.warp_size
    warp_size = dev.warp_size
    hardware_params = auto_scheduler.HardwareParams(-1, 16, 64,
                                                    max_shared_memory_per_block, max_local_memory_per_block,
                                                    max_threads_per_block, max_vthread_extent, warp_size)
elif target_name == 'llvm':
    if device_type != 'cpu':
        raise ValueError('llvm target only supports cpu')
    dev = tvm.cpu(device_id)

    # get cpu name
    def get_processor_name():
        import os, platform, subprocess, re
        if platform.system() == 'Windows':
            return platform.processor()
        elif platform.system() == 'Darwin':
            os.environ['PATH'] = os.environ['PATH'] + os.pathsep + '/usr/sbin'
            command ='sysctl -n machdep.cpu.brand_string'
            return subprocess.check_output(command).strip()
        elif platform.system() == 'Linux':
            command = 'cat /proc/cpuinfo'
            all_info = str(subprocess.check_output(command, shell=True).strip())
            for line in all_info.split('\\n'):
                if 'model name' in line:
                    return re.sub( '.*model name.*:', '', line,1).strip()
        raise ValueError('unable to get cpu name')
    device_name = get_processor_name()

    target_spec = 'llvm'
    if device_name == 'Intel(R) Xeon(R) Gold 6140 CPU @ 2.30GHz':
        target_spec += ' -mcpu=skylake-avx512'
    elif device_name == 'Intel(R) Xeon(R) CPU E5-2683 v4 @ 2.10GHz':
        target_spec += ' -mcpu=core-avx2'
    print(target_spec)

    target = tvm.target.Target(target_spec)
elif target_name == 'cuda':
    if device_type != 'gpu':
        raise ValueError('cuda target only supports gpu')
    dev = tvm.cuda(device_id)
    dev.sync()  # sync device to initialize it and be able to query the device name
    device_name = dev.device_name
    target = tvm.target.Target('cuda')
else:
    raise ValueError('unknown target ' + target_name)

print('Tuning for target: ' + target_name)
print('Tuning for device: ' + device_name)

os.makedirs(data_dir, exist_ok=True)
logfile = f'{data_dir}/log.json'

if target_name == 'opencl':
    task = auto_scheduler.SearchTask(func=routine_module.func, args=routine_args, target=target, hardware_params=hardware_params)
else:
    task = auto_scheduler.SearchTask(func=routine_module.func, args=routine_args, target=target)
measure_ctx = auto_scheduler.LocalRPCMeasureContext(min_repeat_ms=300, timeout=600)
tune_option = auto_scheduler.TuningOptions(
    num_measure_trials=trials,
    measure_callbacks=[auto_scheduler.RecordToFile(logfile)],
    verbose=1,
    runner=measure_ctx.runner
)

# Run auto-tuning (search)
if os.path.isfile(logfile):
    print('resume tuning')
    cost_model = auto_scheduler.XGBModel()
    cost_model.update_from_file(logfile)
    search_policy = auto_scheduler.SketchPolicy(
        task, cost_model, init_search_callbacks=[auto_scheduler.PreloadMeasuredStates(logfile)]
    )
    tune_option = auto_scheduler.TuningOptions(
        num_measure_trials=trials, measure_callbacks=[auto_scheduler.RecordToFile(logfile)]
    )
    task.tune(tune_option, search_policy=search_policy)
else:
    # Run auto-tuning (search)
    task.tune(tune_option)

# Apply the best schedule
sch, args = task.apply_best(logfile)

# Kill the measurement process
del measure_ctx