import numpy as np
import tvm
from tvm import te, auto_scheduler


@auto_scheduler.register_workload
def func(N, K, C, H, W, R, S, P, Q, stride):
    data = te.placeholder((N, H, W, C), name='data', dtype='float32')
    filter = te.placeholder((K, R, S, C), name='filter', dtype='float32')

    r = te.reduce_axis((0, R), name='r')
    s = te.reduce_axis((0, S), name='s')
    c = te.reduce_axis((0, C), name='c')
    mcc = te.compute(
        (N, P, Q, K),
        lambda n, p, q, k: te.sum(
            data[n, p * stride + r, q * stride + s, c] * filter[k, r, s, c], axis=[r, s, c]
        ),
        name='mcc_nhwc_krsc_npqk',
    )
    return [data, filter, mcc]


def tvm_buffer(dev, N, K, C, H, W, R, S, P, Q, stride):
    data_np = np.random.uniform(size=(N, H, W, C)).astype(np.float32)
    filter_np = np.random.uniform(size=(K, R, S, C)).astype(np.float32)
    data = tvm.nd.array(data_np, device=dev)
    filter = tvm.nd.array(filter_np, device=dev)
    out = tvm.nd.empty((N, P, Q, K), device=dev)
    return data, filter, out
