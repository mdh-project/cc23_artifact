#include <cuda.h>
#include <nvrtc.h>
#include <dlfcn.h>
#include <iostream>
#include <array>
#include <algorithm>
#include <cstring>

constexpr unsigned short int _warm_ups = 10;
constexpr unsigned short int _evaluations = 600;

extern "C" CUresult cuLaunchKernel(CUfunction   f,
                                   unsigned int gridDimX,
                                   unsigned int gridDimY,
                                   unsigned int gridDimZ,
                                   unsigned int blockDimX,
                                   unsigned int blockDimY,
                                   unsigned int blockDimZ,
                                   unsigned int sharedMemBytes,
                                   CUstream     hStream,
                                   void**       kernelParams,
                                   void**       extra) {
    typedef CUresult (*signature)(CUfunction,
                                  unsigned int,
                                  unsigned int,
                                  unsigned int,
                                  unsigned int,
                                  unsigned int,
                                  unsigned int,
                                  unsigned int,
                                  CUstream,
                                  void**,
                                  void**);
    static signature original_method = nullptr;
    if (original_method == nullptr) {
        void *tmpPtr = dlsym(RTLD_NEXT, "cuLaunchKernel");
        memcpy(&original_method, &tmpPtr, sizeof(void*));
    }

    std::array<float, _evaluations> runtimes;
    for (int i = 0; i < _warm_ups + _evaluations; ++i) {
        CUevent start, end;
        cuEventCreate(&start, 0);
        cuEventCreate(&end, 0);
        cuEventRecord(start, hStream);
        (*original_method)(f, gridDimX, gridDimY, gridDimZ, blockDimX, blockDimY, blockDimZ, sharedMemBytes, hStream, kernelParams, extra);
        cuEventRecord(end, hStream);
        cuEventSynchronize(end);
        if (i >= _warm_ups) {
            cuEventElapsedTime(&(runtimes[i - _warm_ups]), start, end);
        }
    }
    std::cout << "min runtime: " << *std::min_element(runtimes.begin(), runtimes.end()) << " ms (" << _evaluations << " evaluations)" << std::endl;

    return (*original_method)(f, gridDimX, gridDimY, gridDimZ, blockDimX, blockDimY, blockDimZ, sharedMemBytes, hStream, kernelParams, extra);
}

extern "C" nvrtcResult nvrtcCompileProgram(nvrtcProgram prog, int numOptions, const char* const* options) {
    typedef nvrtcResult (*signature)(nvrtcProgram,
                                     int,
                                     const char* const*);
    static signature original_method = nullptr;
    if (original_method == nullptr) {
        void *tmpPtr = dlsym(RTLD_NEXT, "nvrtcCompileProgram");
        memcpy(&original_method, &tmpPtr, sizeof(void*));
    }
    std::cout << "compile options:" << std::endl;
    for (int i = 0; i < numOptions; ++i) {
        std::cout << " " << options[i];
    }
    std::cout << std::endl;
    return (*original_method)(prog, numOptions, options);
}
