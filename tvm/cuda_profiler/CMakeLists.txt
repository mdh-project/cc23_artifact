cmake_minimum_required(VERSION 2.8.11)
project(cuda_profiler)

set(CMAKE_CXX_STANDARD 14)

include_directories(include)

find_package(CUDA REQUIRED)

add_library(cuda_profiler SHARED src/cuda_profiler.cpp)
target_link_libraries(cuda_profiler dl)