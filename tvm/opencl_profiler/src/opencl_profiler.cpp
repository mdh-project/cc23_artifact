#include <CL/cl.h>
#include <dlfcn.h>
#include <iostream>
#include <array>
#include <algorithm>
#include <cstring>

constexpr unsigned short int _warm_ups = 10;
constexpr unsigned short int _evaluations = 600;

extern "C" cl_command_queue clCreateCommandQueue(cl_context context,
                                                 cl_device_id device,
                                                 cl_command_queue_properties properties,
                                                 cl_int* errcode_ret) {
    typedef cl_command_queue (*signature)(cl_context,
                                          cl_device_id,
                                          cl_command_queue_properties,
                                          cl_int*);
    static signature original_method = nullptr;
    if (original_method == nullptr) {
        void *tmpPtr = dlsym(RTLD_NEXT, "clCreateCommandQueue");
        memcpy(&original_method, &tmpPtr, sizeof(void*));
    }
    return original_method(context, device, properties | CL_QUEUE_PROFILING_ENABLE, errcode_ret);
}

extern "C" cl_int clEnqueueNDRangeKernel(cl_command_queue command_queue,
                                         cl_kernel        kernel,
                                         cl_uint          work_dim,
                                         const size_t *   global_work_offset,
                                         const size_t *   global_work_size,
                                         const size_t *   local_work_size,
                                         cl_uint          num_events_in_wait_list,
                                         const cl_event * event_wait_list,
                                         cl_event *       event) {
    typedef cl_int (*signature)(cl_command_queue,
                                cl_kernel,
                                cl_uint,
                                const size_t *,
                                const size_t *,
                                const size_t *,
                                cl_uint,
                                const cl_event *,
                                cl_event *);
    static signature original_method = nullptr;
    if (original_method == nullptr) {
        void *tmpPtr = dlsym(RTLD_NEXT, "clEnqueueNDRangeKernel");
        memcpy(&original_method, &tmpPtr, sizeof(void*));
    }

    std::array<size_t, _evaluations> runtimes;
    auto timing_event = event;
    if (event == nullptr) {
        timing_event = new cl_event;
    }
    for (int i = 0; i < _warm_ups + _evaluations; ++i) {
        (*original_method)(command_queue,
                           kernel,
                           work_dim,
                           global_work_offset,
                           global_work_size,
                           local_work_size,
                           num_events_in_wait_list,
                           event_wait_list, timing_event);
        clWaitForEvents(1, timing_event);
        cl_ulong start, end;
        clGetEventProfilingInfo(*timing_event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, nullptr);
        clGetEventProfilingInfo(*timing_event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, nullptr);

        if (i >= _warm_ups) {
            runtimes[i - _warm_ups] = end - start;
        }
    }
    if (event == nullptr) {
        clReleaseEvent(*timing_event);
        delete timing_event;
    }
    std::cout << "min runtime: " << *std::min_element(runtimes.begin(), runtimes.end()) << " ns (" << _evaluations << " evaluations)" << std::endl;

    return (*original_method)(command_queue,
                              kernel,
                              work_dim,
                              global_work_offset,
                              global_work_size,
                              local_work_size,
                              num_events_in_wait_list,
                              event_wait_list, event);
}