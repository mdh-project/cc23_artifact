#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }

if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
    if [[ -n "$ENABLE_MDH" && "$ENABLE_MDH" -eq 1 ]]; then
        # MDH (ResNet-50 Training MCC)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 0 ]] && (
          echo "Benchmarking MDH ResNet-50 Training MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical --path $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 7 --input-size-r-3 7 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/mdh/tuned_configuration
        )
        # MDH (ResNet-50 Training MatMul)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 1 ]] && (
          echo "Benchmarking MDH ResNet-50 Training MatMul for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 1000 --input-size-r-1 2048 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/mdh/tuned_configuration
        )
        # MDH (ResNet-50 Inference MCC)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 2 ]] && (
          echo "Benchmarking MDH ResNet-50 Inference MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical --path $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 7 --input-size-r-3 7 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/mdh/tuned_configuration
        )
        # MDH (ResNet-50 Inference MatMul)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 3 ]] && (
          echo "Benchmarking MDH ResNet-50 Inference MatMul for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 1000 --input-size-r-1 2048 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/mdh/tuned_configuration
        )
        # MDH (VGG-16 Training MCC)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 4 ]] && (
          echo "Benchmarking MDH VGG-16 Training MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 --path $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 224 --input-size-l-3 224 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/mdh/tuned_configuration
        )
        # MDH (VGG-16 Training MatMul)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 5 ]] && (
          echo "Benchmarking MDH VGG-16 Training MatMul for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 4096 --input-size-r-1 25088 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/mdh/tuned_configuration
        )
        # MDH (VGG-16 Inference MCC)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 6 ]] && (
          echo "Benchmarking MDH VGG-16 Inference MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 --path $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 224 --input-size-l-3 224 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/mdh/tuned_configuration
        )
        # MDH (VGG-16 Inference MatMul)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 7 ]] && (
          echo "Benchmarking MDH VGG-16 Inference MatMul for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 4096 --input-size-r-1 25088 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/mdh/tuned_configuration
        )
        # MDH (MobileNet Training MCC)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 8 ]] && (
          echo "Benchmarking MDH MobileNet Training MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 --path $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/ -d 0 --input-size-l-1 16 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 32 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/mdh/tuned_configuration
        )
        # MDH (MobileNet Inference MCC)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 9 ]] && (
          echo "Benchmarking MDH MobileNet Inference MCC for GPU"
          cd $ARTIFACT_ROOT/mdh/gpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 --path $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/ -d 0 --input-size-l-1 1 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 32 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/kernel_1.cuda --kernel-file-2 $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/kernel_2.cuda -c $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/mdh/tuned_configuration
        )
    fi

    if [[ -n "$ENABLE_TVM" && "$ENABLE_TVM" -eq 1 ]]; then
        # TVM (ResNet-50 Training MCC)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 10 ]] && (
          echo "Benchmarking TVM ResNet-50 Training MCC for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/tvm/ cuda gpu 0 mcc_nhwc_krsc_npqk 16 64 3 230 230 7 7 112 112 2 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/tvm/runtimes_min
        )
        # TVM (ResNet-50 Training MatMul)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 11 ]] && (
          echo "Benchmarking TVM ResNet-50 Training MatMul for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/tvm/ cuda gpu 0 matmul 16 1000 2048 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/tvm/runtimes_min
        )
        # TVM (ResNet-50 Inference MCC)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 12 ]] && (
          echo "Benchmarking TVM ResNet-50 Inference MCC for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/tvm/ cuda gpu 0 mcc_nhwc_krsc_npqk 1 64 3 230 230 7 7 112 112 2 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/tvm/runtimes_min
        )
        # TVM (ResNet-50 Inference MatMul)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 13 ]] && (
          echo "Benchmarking TVM ResNet-50 Inference MatMul for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/tvm/ cuda gpu 0 matmul 1 1000 2048 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/tvm/runtimes_min
        )
        # TVM (VGG-16 Training MCC)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 14 ]] && (
          echo "Benchmarking TVM VGG-16 Training MCC for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/tvm/ cuda gpu 0 mcc_nhwc_krsc_npqk 16 64 3 226 226 3 3 224 224 1 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/tvm/runtimes_min
        )
        # TVM (VGG-16 Training MatMul)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 15 ]] && (
          echo "Benchmarking TVM VGG-16 Training MatMul for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/tvm/ cuda gpu 0 matmul 16 4096 25088 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/tvm/runtimes_min
        )
        # TVM (VGG-16 Inference MCC)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 16 ]] && (
          echo "Benchmarking TVM VGG-16 Inference MCC for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/tvm/ cuda gpu 0 mcc_nhwc_krsc_npqk 1 64 3 226 226 3 3 224 224 1 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/tvm/runtimes_min
        )
        # TVM (VGG-16 Inference MatMul)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 17 ]] && (
          echo "Benchmarking TVM VGG-16 Inference MatMul for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/tvm/ cuda gpu 0 matmul 1 4096 25088 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/tvm/runtimes_min
        )
        # TVM (MobileNet Training MCC)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 18 ]] && (
          echo "Benchmarking TVM MobileNet Training MCC for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/tvm/ cuda gpu 0 mcc_nhwc_krsc_npqk 16 32 3 225 225 3 3 112 112 2 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/tvm/runtimes_min
        )
        # TVM (MobileNet Inference MCC)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 19 ]] && (
          echo "Benchmarking TVM MobileNet Inference MCC for GPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/cuda_profiler/build/libcuda_profiler.so
          python bench.py $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/tvm/ cuda gpu 0 mcc_nhwc_krsc_npqk 1 32 3 225 225 3 3 112 112 2 | grep -oP "(?<=min runtime: )(\\d+\\.\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/tvm/runtimes_min
        )
    fi
fi


if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
    if [[ -n "$ENABLE_MDH" && "$ENABLE_MDH" -eq 1 ]]; then
        # MDH (ResNet-50 Training MCC)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 20 ]] && (
          echo "Benchmarking MDH ResNet-50 Training MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical --path $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 7 --input-size-r-3 7 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/mdh/tuned_configuration
        )
        # MDH (ResNet-50 Training MatMul)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 21 ]] && (
          echo "Benchmarking MDH ResNet-50 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 1000 --input-size-r-1 2048 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/mdh/tuned_configuration
        )
        # MDH (ResNet-50 Inference MCC)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 22 ]] && (
          echo "Benchmarking MDH ResNet-50 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2_asymmetrical --path $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 7 --input-size-r-3 7 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/mdh/tuned_configuration
        )
        # MDH (ResNet-50 Inference MatMul)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 23 ]] && (
          echo "Benchmarking MDH ResNet-50 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 1000 --input-size-r-1 2048 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/mdh/tuned_configuration
        )
        # MDH (VGG-16 Training MCC)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 24 ]] && (
          echo "Benchmarking MDH VGG-16 Training MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 --path $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 224 --input-size-l-3 224 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/mdh/tuned_configuration
        )
        # MDH (VGG-16 Training MatMul)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 25 ]] && (
          echo "Benchmarking MDH VGG-16 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 4096 --input-size-r-1 25088 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/mdh/tuned_configuration
        )
        # MDH (VGG-16 Inference MCC)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 26 ]] && (
          echo "Benchmarking MDH VGG-16 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_1 --path $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 224 --input-size-l-3 224 --input-size-l-4 64 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/mdh/tuned_configuration
        )
        # MDH (VGG-16 Inference MatMul)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 27 ]] && (
          echo "Benchmarking MDH VGG-16 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_matmul --path $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 4096 --input-size-r-1 25088 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/mdh/tuned_configuration
        )
        # MDH (MobileNet Training MCC)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 28 ]] && (
          echo "Benchmarking MDH MobileNet Training MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 --path $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/ -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 32 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/mdh/tuned_configuration
        )
        # MDH (MobileNet Inference MCC)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 29 ]] && (
          echo "Benchmarking MDH MobileNet Inference MCC for CPU"
          cd $ARTIFACT_ROOT/mdh/cpu/build &&
          ./bench_md_hom_blocked_v3_mcc_nhwc_krsc_npqk_stride_2 --path $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/ -p 0 -d 0 --input-size-l-1 1 --input-size-l-2 112 --input-size-l-3 112 --input-size-l-4 32 --input-size-r-1 3 --input-size-r-2 3 --input-size-r-3 3 -e 600 --kernel-file-1 $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/kernel_1.cl --kernel-file-2 $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/kernel_2.cl -c $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/mdh/tuned_configuration
        )
    fi

    if [[ -n "$ENABLE_TVM" && "$ENABLE_TVM" -eq 1 ]]; then
        # TVM (ResNet-50 Training MCC, OpenCL)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 30 ]] && (
          echo "Benchmarking TVM (OpenCL) ResNet-50 Training MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/opencl/ opencl cpu 0 mcc_nhwc_krsc_npqk 16 64 3 230 230 7 7 112 112 2 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/opencl/runtimes_min
        )
        # TVM (ResNet-50 Training MatMul, OpenCL)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 31 ]] && (
          echo "Benchmarking TVM (OpenCL) ResNet-50 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/opencl/ opencl cpu 0 matmul 16 1000 2048 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/opencl/runtimes_min
        )
        # TVM (ResNet-50 Inference MCC, OpenCL)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 32 ]] && (
          echo "Benchmarking TVM (OpenCL) ResNet-50 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/opencl/ opencl cpu 0 mcc_nhwc_krsc_npqk 1 64 3 230 230 7 7 112 112 2 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/opencl/runtimes_min
        )
        # TVM (ResNet-50 Inference MatMul, OpenCL)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 33 ]] && (
          echo "Benchmarking TVM (OpenCL) ResNet-50 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/opencl/ opencl cpu 0 matmul 1 1000 2048 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/opencl/runtimes_min
        )
        # TVM (VGG-16 Training MCC, OpenCL)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 34 ]] && (
          echo "Benchmarking TVM (OpenCL) VGG-16 Training MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/opencl/ opencl cpu 0 mcc_nhwc_krsc_npqk 16 64 3 226 226 3 3 224 224 1 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/opencl/runtimes_min
        )
        # TVM (VGG-16 Training MatMul, OpenCL)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 35 ]] && (
          echo "Benchmarking TVM (OpenCL) VGG-16 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/opencl/ opencl cpu 0 matmul 16 4096 25088 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/opencl/runtimes_min
        )
        # TVM (VGG-16 Inference MCC, OpenCL)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 36 ]] && (
          echo "Benchmarking TVM (OpenCL) VGG-16 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/opencl/ opencl cpu 0 mcc_nhwc_krsc_npqk 1 64 3 226 226 3 3 224 224 1 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/opencl/runtimes_min
        )
        # TVM (VGG-16 Inference MatMul, OpenCL)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 37 ]] && (
          echo "Benchmarking TVM (OpenCL) VGG-16 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/opencl/ opencl cpu 0 matmul 1 4096 25088 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/opencl/runtimes_min
        )
        # TVM (MobileNet Training MCC, OpenCL)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 38 ]] && (
          echo "Benchmarking TVM (OpenCL) MobileNet Training MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/opencl/ opencl cpu 0 mcc_nhwc_krsc_npqk 16 32 3 225 225 3 3 112 112 2 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/opencl/runtimes_min
        )
        # TVM (MobileNet Inference MCC, OpenCL)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 39 ]] && (
          echo "Benchmarking TVM (OpenCL) MobileNet Inference MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          export LD_PRELOAD=`pwd`/opencl_profiler/build/libopencl_profiler.so
          python bench.py $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/opencl/ opencl cpu 0 mcc_nhwc_krsc_npqk 1 32 3 225 225 3 3 112 112 2 | grep -oP "(?<=min runtime: )(\\d+)" | head -n 1 > $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/opencl/runtimes_min
        )

        # TVM (ResNet-50 Training MCC, LLVM)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 40 ]] && (
          echo "Benchmarking TVM (LLVM) ResNet-50 Training MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/llvm/ llvm cpu 0 mcc_nhwc_krsc_npqk 16 64 3 230 230 7 7 112 112 2 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/tvm/llvm/runtimes_min
        )
        # TVM (ResNet-50 Training MatMul, LLVM)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 41 ]] && (
          echo "Benchmarking TVM (LLVM) ResNet-50 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/llvm/ llvm cpu 0 matmul 16 1000 2048 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/tvm/llvm/runtimes_min
        )
        # TVM (ResNet-50 Inference MCC, LLVM)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 42 ]] && (
          echo "Benchmarking TVM (LLVM) ResNet-50 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/llvm/ llvm cpu 0 mcc_nhwc_krsc_npqk 1 64 3 230 230 7 7 112 112 2 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/tvm/llvm/runtimes_min
        )
        # TVM (ResNet-50 Inference MatMul, LLVM)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 43 ]] && (
          echo "Benchmarking TVM (LLVM) ResNet-50 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/llvm/ llvm cpu 0 matmul 1 1000 2048 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/tvm/llvm/runtimes_min
        )
        # TVM (VGG-16 Training MCC, LLVM)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 44 ]] && (
          echo "Benchmarking TVM (LLVM) VGG-16 Training MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/llvm/ llvm cpu 0 mcc_nhwc_krsc_npqk 16 64 3 226 226 3 3 224 224 1 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/tvm/llvm/runtimes_min
        )
        # TVM (VGG-16 Training MatMul, LLVM)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 45 ]] && (
          echo "Benchmarking TVM (LLVM) VGG-16 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/llvm/ llvm cpu 0 matmul 16 4096 25088 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/tvm/llvm/runtimes_min
        )
        # TVM (VGG-16 Inference MCC, LLVM)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 46 ]] && (
          echo "Benchmarking TVM (LLVM) VGG-16 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/llvm/ llvm cpu 0 mcc_nhwc_krsc_npqk 1 64 3 226 226 3 3 224 224 1 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/tvm/llvm/runtimes_min
        )
        # TVM (VGG-16 Inference MatMul, LLVM)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 47 ]] && (
          echo "Benchmarking TVM (LLVM) VGG-16 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/llvm/ llvm cpu 0 matmul 1 4096 25088 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/tvm/llvm/runtimes_min
        )
        # TVM (MobileNet Training MCC, LLVM)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 48 ]] && (
          echo "Benchmarking TVM (LLVM) MobileNet Training MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/llvm/ llvm cpu 0 mcc_nhwc_krsc_npqk 16 32 3 225 225 3 3 112 112 2 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/tvm/llvm/runtimes_min
        )
        # TVM (MobileNet Inference MCC, LLVM)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 49 ]] && (
          echo "Benchmarking TVM (LLVM) MobileNet Inference MCC for CPU"
          cd $ARTIFACT_ROOT/tvm/ &&
          python bench.py $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/llvm/ llvm cpu 0 mcc_nhwc_krsc_npqk 1 32 3 225 225 3 3 112 112 2 &> /dev/null
          mv $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/llvm/runtime $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/tvm/llvm/runtimes_min
        )
    fi
fi

if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
    if [[ -n "$ENABLE_CUDNN" && "$ENABLE_CUDNN" -eq 1 ]]; then
        # cuDNN (ResNet-50 Training MCC)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 50 ]] && (
          echo "Benchmarking cuDNN ResNet-50 Training MCC for GPU"
          cd $ARTIFACT_ROOT/cudnn/mcc/build &&
          ./bench_cuDNN_nhwc_krsc_npqk_v8 --path $ARTIFACT_ROOT/results/gpu/resnet-50_training_mcc/cudnn/ -n 16 -k 64 -h 230 -w 230 -p 112 -q 112 -c 3 -r 7 -s 7 --h-stride 2 --w-stride 2 --evaluations 600
        )
        # cuDNN (ResNet-50 Inference MCC)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 52 ]] && (
          echo "Benchmarking cuDNN ResNet-50 Inference MCC for GPU"
          cd $ARTIFACT_ROOT/cudnn/mcc/build &&
          ./bench_cuDNN_nhwc_krsc_npqk_v8 --path $ARTIFACT_ROOT/results/gpu/resnet-50_inference_mcc/cudnn/ -n 1 -k 64 -h 230 -w 230 -p 112 -q 112 -c 3 -r 7 -s 7 --h-stride 2 --w-stride 2 --evaluations 600
        )
        # cuDNN (VGG-16 Training MCC)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 54 ]] && (
          echo "Benchmarking cuDNN VGG-16 Training MCC for GPU"
          cd $ARTIFACT_ROOT/cudnn/mcc/build &&
          ./bench_cuDNN_nhwc_krsc_npqk_v8 --path $ARTIFACT_ROOT/results/gpu/vgg-16_training_mcc/cudnn/ -n 16 -k 64 -h 226 -w 226 -p 224 -q 224 -c 3 -r 3 -s 3 --h-stride 1 --w-stride 1 --evaluations 600
        )
        # cuDNN (VGG-16 Inference MCC)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 56 ]] && (
          echo "Benchmarking cuDNN VGG-16 Inference MCC for GPU"
          cd $ARTIFACT_ROOT/cudnn/mcc/build &&
          ./bench_cuDNN_nhwc_krsc_npqk_v8 --path $ARTIFACT_ROOT/results/gpu/vgg-16_inference_mcc/cudnn/ -n 1 -k 64 -h 226 -w 226 -p 224 -q 224 -c 3 -r 3 -s 3 --h-stride 1 --w-stride 1 --evaluations 600
        )
        # cuDNN (MobileNet Training MCC)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 58 ]] && (
          echo "Benchmarking cuDNN MobileNet Training MCC for GPU"
          cd $ARTIFACT_ROOT/cudnn/mcc/build &&
          ./bench_cuDNN_nhwc_krsc_npqk_v8 --path $ARTIFACT_ROOT/results/gpu/mobilenet_training_mcc/cudnn/ -n 16 -k 32 -h 225 -w 225 -p 112 -q 112 -c 3 -r 3 -s 3 --h-stride 2 --w-stride 2 --evaluations 600
        )
        # cuDNN (MobileNet Inference MCC)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 59 ]] && (
          echo "Benchmarking cuDNN MobileNet Inference MCC for GPU"
          cd $ARTIFACT_ROOT/cudnn/mcc/build &&
          ./bench_cuDNN_nhwc_krsc_npqk_v8 --path $ARTIFACT_ROOT/results/gpu/mobilenet_inference_mcc/cudnn/ -n 1 -k 32 -h 225 -w 225 -p 112 -q 112 -c 3 -r 3 -s 3 --h-stride 2 --w-stride 2 --evaluations 600
        )
    fi
    if [[ -n "$ENABLE_CUBLAS" && "$ENABLE_CUBLAS" -eq 1 ]]; then
        # cuBLAS (ResNet-50 Training MatMul)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 51 ]] && (
          echo "Benchmarking cuBLAS ResNet-50 Training MatMul for GPU"
          cd $ARTIFACT_ROOT/cublas/matmul/build &&
          ./bench_cuBLAS_sgemm_row_major --path $ARTIFACT_ROOT/results/gpu/resnet-50_training_matmul/cublas/ -m 16 -n 1000 -k 2048 --evaluations 600
        )
        # cuBLAS (ResNet-50 Inference MatMul)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 53 ]] && (
          echo "Benchmarking cuBLAS ResNet-50 Inference MatMul for GPU"
          cd $ARTIFACT_ROOT/cublas/matmul/build &&
          ./bench_cuBLAS_sgemm_row_major --path $ARTIFACT_ROOT/results/gpu/resnet-50_inference_matmul/cublas/ -m 1 -n 1000 -k 2048 --evaluations 600
        )
        # cuBLAS (VGG-16 Training MatMul)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 55 ]] && (
          echo "Benchmarking cuBLAS VGG-16 Training MatMul for GPU"
          cd $ARTIFACT_ROOT/cublas/matmul/build &&
          ./bench_cuBLAS_sgemm_row_major --path $ARTIFACT_ROOT/results/gpu/vgg-16_training_matmul/cublas/ -m 16 -n 4096 -k 25088 --evaluations 600
        )
        # cuBLAS (VGG-16 Inference MatMul)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 57 ]] && (
          echo "Benchmarking cuBLAS VGG-16 Inference MatMul for GPU"
          cd $ARTIFACT_ROOT/cublas/matmul/build &&
          ./bench_cuBLAS_sgemm_row_major --path $ARTIFACT_ROOT/results/gpu/vgg-16_inference_matmul/cublas/ -m 1 -n 4096 -k 25088 --evaluations 600
        )
    fi
fi

if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
    if [[ -n "$ENABLE_ONEDNN" && "$ENABLE_ONEDNN" -eq 1 ]]; then
        # oneDNN (ResNet-50 Training MCC)
        [[ -n "$ENABLE_RESNET50_TRAINING_MCC" && "$ENABLE_RESNET50_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 60 ]] && (
          echo "Benchmarking oneDNN ResNet-50 Training MCC for CPU"
          cd $ARTIFACT_ROOT/onednn/mcc/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/dnnl/latest/env/vars.sh --dnnl-configuration=cpu_iomp &&
          ./bench_oneDNN_nhwc_krsc_npqk --path $ARTIFACT_ROOT/results/cpu/resnet-50_training_mcc/onednn/ -n 16 -k 64 -h 230 -w 230 -p 112 -q 112 -c 3 -r 7 -s 7 --h-stride 2 --w-stride 2 --evaluations 600
        )
        # oneDNN (ResNet-50 Inference MCC)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MCC" && "$ENABLE_RESNET50_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 62 ]] && (
          echo "Benchmarking oneDNN ResNet-50 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/onednn/mcc/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/dnnl/latest/env/vars.sh --dnnl-configuration=cpu_iomp &&
          ./bench_oneDNN_nhwc_krsc_npqk --path $ARTIFACT_ROOT/results/cpu/resnet-50_inference_mcc/onednn/ -n 1 -k 64 -h 230 -w 230 -p 112 -q 112 -c 3 -r 7 -s 7 --h-stride 2 --w-stride 2 --evaluations 600
        )
        # oneDNN (VGG-16 Training MCC)
        [[ -n "$ENABLE_VGG16_TRAINING_MCC" && "$ENABLE_VGG16_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 64 ]] && (
          echo "Benchmarking oneDNN VGG-16 Training MCC for CPU"
          cd $ARTIFACT_ROOT/onednn/mcc/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/dnnl/latest/env/vars.sh --dnnl-configuration=cpu_iomp &&
          ./bench_oneDNN_nhwc_krsc_npqk --path $ARTIFACT_ROOT/results/cpu/vgg-16_training_mcc/onednn/ -n 16 -k 64 -h 226 -w 226 -p 224 -q 224 -c 3 -r 3 -s 3 --h-stride 1 --w-stride 1 --evaluations 600
        )
        # oneDNN (VGG-16 Inference MCC)
        [[ -n "$ENABLE_VGG16_INFERENCE_MCC" && "$ENABLE_VGG16_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 66 ]] && (
          echo "Benchmarking oneDNN VGG-16 Inference MCC for CPU"
          cd $ARTIFACT_ROOT/onednn/mcc/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/dnnl/latest/env/vars.sh --dnnl-configuration=cpu_iomp &&
          ./bench_oneDNN_nhwc_krsc_npqk --path $ARTIFACT_ROOT/results/cpu/vgg-16_inference_mcc/onednn/ -n 1 -k 64 -h 226 -w 226 -p 224 -q 224 -c 3 -r 3 -s 3 --h-stride 1 --w-stride 1 --evaluations 600
        )
        # oneDNN (MobileNet Training MCC)
        [[ -n "$ENABLE_MOBILENET_TRAINING_MCC" && "$ENABLE_MOBILENET_TRAINING_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 68 ]] && (
          echo "Benchmarking oneDNN MobileNet Training MCC for CPU"
          cd $ARTIFACT_ROOT/onednn/mcc/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/dnnl/latest/env/vars.sh --dnnl-configuration=cpu_iomp &&
          ./bench_oneDNN_nhwc_krsc_npqk --path $ARTIFACT_ROOT/results/cpu/mobilenet_training_mcc/onednn/ -n 16 -k 32 -h 225 -w 225 -p 112 -q 112 -c 3 -r 3 -s 3 --h-stride 2 --w-stride 2 --evaluations 600
        )
        # oneDNN (MobileNet Inference MCC)
        [[ -n "$ENABLE_MOBILENET_INFERENCE_MCC" && "$ENABLE_MOBILENET_INFERENCE_MCC" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 69 ]] && (
          echo "Benchmarking oneDNN MobileNet Inference MCC for CPU"
          cd $ARTIFACT_ROOT/onednn/mcc/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/dnnl/latest/env/vars.sh --dnnl-configuration=cpu_iomp &&
          ./bench_oneDNN_nhwc_krsc_npqk --path $ARTIFACT_ROOT/results/cpu/mobilenet_inference_mcc/onednn/ -n 1 -k 32 -h 225 -w 225 -p 112 -q 112 -c 3 -r 3 -s 3 --h-stride 2 --w-stride 2 --evaluations 600
        )
    fi
    if [[ -n "$ENABLE_ONEMKL" && "$ENABLE_ONEMKL" -eq 1 ]]; then
        # oneMKL (ResNet-50 Training MatMul)
        [[ -n "$ENABLE_RESNET50_TRAINING_MATMUL" && "$ENABLE_RESNET50_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 61 ]] && (
          echo "Benchmarking oneMKL ResNet-50 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/onemkl/matmul/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/mkl/latest/env/vars.sh &&
          ./bench_oneMKL_sgemm --path $ARTIFACT_ROOT/results/cpu/resnet-50_training_matmul/onemkl/ -m 16 -n 1000 -k 2048 --evaluations 600
        )
        # oneMKL (ResNet-50 Inference MatMul)
        [[ -n "$ENABLE_RESNET50_INFERENCE_MATMUL" && "$ENABLE_RESNET50_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 63 ]] && (
          echo "Benchmarking oneMKL ResNet-50 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/onemkl/matmul/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/mkl/latest/env/vars.sh &&
          ./bench_oneMKL_sgemm --path $ARTIFACT_ROOT/results/cpu/resnet-50_inference_matmul/onemkl/ -m 1 -n 1000 -k 2048 --evaluations 600
        )
        # oneMKL (VGG-16 Training MatMul)
        [[ -n "$ENABLE_VGG16_TRAINING_MATMUL" && "$ENABLE_VGG16_TRAINING_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 65 ]] && (
          echo "Benchmarking oneMKL VGG-16 Training MatMul for CPU"
          cd $ARTIFACT_ROOT/onemkl/matmul/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/mkl/latest/env/vars.sh &&
          ./bench_oneMKL_sgemm --path $ARTIFACT_ROOT/results/cpu/vgg-16_training_matmul/onemkl/ -m 16 -n 4096 -k 25088 --evaluations 600
        )
        # oneMKL (VGG-16 Inference MatMul)
        [[ -n "$ENABLE_VGG16_INFERENCE_MATMUL" && "$ENABLE_VGG16_INFERENCE_MATMUL" -eq 1 ]] && [[ -z "$SLURM_ARRAY_TASK_ID" || $SLURM_ARRAY_TASK_ID == 67 ]] && (
          echo "Benchmarking oneMKL VGG-16 Inference MatMul for CPU"
          cd $ARTIFACT_ROOT/onemkl/matmul/build &&
          source ${ONEAPI_DIR}/compiler/latest/env/vars.sh &&
          source ${ONEAPI_DIR}/mkl/latest/env/vars.sh &&
          ./bench_oneMKL_sgemm --path $ARTIFACT_ROOT/results/cpu/vgg-16_inference_matmul/onemkl/ -m 1 -n 4096 -k 25088 --evaluations 600
        )
    fi
fi


# print results
python collect_results.py | tee results/summary.txt