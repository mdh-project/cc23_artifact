__kernel void matmul_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[8 * 1]
                                 [1 * 1];
    const size_t wg_1 = get_group_id(0) / 20; {
    const size_t wg_2 = get_group_id(0) % 20; {
    {
    {
    const size_t wi_2 = get_local_id(0); {
    {
    {
    {
    {
    for (size_t lcl_1 = 0; lcl_1 < 8; ++lcl_1) {
    {
    {
    {
        c_prv[lcl_1 * 1 + 0][0 * 1 + 0]
            = 0.0f;
    }}}}
    }
    for (size_t glb_3 = 0; glb_3 < 2048; ++glb_3) {
        for (size_t lcl_1 = 0; lcl_1 < 8; ++lcl_1) {
        {
        {
            {
            {
            {
            c_prv[lcl_1 * 1 + 0][0 * 1 + 0]
                +=
            a_glb[(0 * 2 * 8 * 1 * 1 + wg_1 * 8 * 1 * 1 + lcl_1 * 1 * 1 + 0 * 1 + 0) * (2048) + (glb_3 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
            *
            b_glb[(glb_3 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (1000) + (0 * 20 * 1 * 50 * 1 + wg_2 * 1 * 50 * 1 + 0 * 50 * 1 + wi_2 * 1 + 0) ]
            ;
            }}}
        }}}
    }
    {
    for (size_t lcl_1 = 0; lcl_1 < 8; ++lcl_1) {
    {
    {
    {
        c_glb[(0 * 2 * 8 * 1 * 1 + wg_1 * 8 * 1 * 1 + lcl_1 * 1 * 1 + 0 * 1 + 0) * (1000) + (0 * 20 * 1 * 50 * 1 + wg_2 * 1 * 50 * 1 + 0 * 50 * 1 + wi_2 * 1 + 0) ]
                                               =
        c_prv[lcl_1 * 1 + 0][0 * 1 + 0]
                           ;
    }}}}
    }
    }}
    }}}
    }}}
}
