__kernel void matmul_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[1 * 8]
                                 [1 * 1];
    __private float a_prv[8]
                                 [2];
    __private float b_prv[2]
                                 [1];
    {
    const size_t wg_2 = get_group_id(0); {
    {
    {
    const size_t wi_2 = get_local_id(0); {
    {
    for (size_t glb_1 = 0; glb_1 < 2; ++glb_1) {
    {
    {
    {
    {
    for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
    {
        c_prv[0 * 8 + prv_1][0 * 1 + 0]
            = 0.0f;
    }}}}
    }
    for (size_t glb_3 = 0; glb_3 < 512; ++glb_3) {
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
            {
                for (size_t prv_1 = 0; prv_1 < 8; ++prv_1)
                for (size_t prv_3 = 0; prv_3 < 2; ++prv_3)
                    a_prv[prv_1][prv_3]
                                 =
                    a_glb[(glb_1 * 1 * 1 * 1 * 8 + 0 * 1 * 1 * 8 + 0 * 1 * 8 + 0 * 8 + prv_1) * (2048) + (glb_3 * 1 * 2 * 1 * 2 + 0 * 2 * 1 * 2 + lcl_3 * 1 * 2 + 0 * 2 + prv_3) ]
                        ;
            }
            {
               
                for (size_t prv_3 = 0; prv_3 < 2; ++prv_3)
                    b_prv[prv_3][0]
                                 =
                    b_glb[(glb_3 * 1 * 2 * 1 * 2 + 0 * 2 * 1 * 2 + lcl_3 * 1 * 2 + 0 * 2 + prv_3) * (1000) + (0 * 125 * 1 * 8 * 1 + wg_2 * 1 * 8 * 1 + 0 * 8 * 1 + wi_2 * 1 + 0) ]
                        ;
            }
            for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
            {
            for (size_t prv_3 = 0; prv_3 < 2; ++prv_3) {
            c_prv[0 * 8 + prv_1][0 * 1 + 0]
                +=
            a_prv[prv_1][prv_3]
            *
            b_prv[prv_3][0]
            ;
            }}}
        }}}
    }
    {
    {
    {
    for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
    {
        c_glb[(glb_1 * 1 * 1 * 1 * 8 + 0 * 1 * 1 * 8 + 0 * 1 * 8 + 0 * 8 + prv_1) * (1000) + (0 * 125 * 1 * 8 * 1 + wg_2 * 1 * 8 * 1 + 0 * 8 * 1 + wi_2 * 1 + 0) ]
                                               =
        c_prv[0 * 8 + prv_1][0 * 1 + 0]
                           ;
    }}}}
    }
    }}
    }}}
    }}}
}
