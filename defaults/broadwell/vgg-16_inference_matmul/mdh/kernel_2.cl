__kernel void matmul_static_2(__global float const * const restrict int_res, __global float * const restrict res_g, __global float * const restrict c) {
  const size_t i_wg_l_1 = 0;
  const size_t i_wi_l_1 = 0;
  const size_t i_wg_l_2 = 0;
  const size_t i_wi_l_2 = 0;
  const size_t i_wg_r_1 = 0;
  const size_t i_wi_r_1 = get_local_id(0);
  __private float res_p[1][((1024 / 1) / (64)) + (((1 * (1024 / 1)) % (1 * (64)) / 1) > 0) + (((1 * (1024 / 1)) % (1 * (64)) % 1) > 0)][64][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1];
  __local float l_reduction_mem[4][1024][1];
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((1 / (1 * 1)) / (1 / 1)); ++l_step_l_1) {
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((4096 / (1 * 1)) / (1024 / 1)); ++l_step_l_2) {
      size_t l_step_r_1 = 0;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
        for (size_t p_step_l_2 = 0; p_step_l_2 < ((1024 / 1) / (64)); ++p_step_l_2) {
          size_t p_step_r_1 = 0;
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
            for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (64); ++p_iteration_l_2) {
              size_t p_iteration_r_1 = 0;
              res_p[(0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)] = int_res[(((l_step_r_1 * (4 / 4) + (((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 4 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) / 4) * 4 + i_wg_r_1 * 4 + ((((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 4 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) % 4))) * 4096 * 1 + (((l_step_l_2 * (1024 / 1) + (((p_step_l_2 * (64) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_2 * 1 + ((((p_step_l_2 * (64) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) % 1))) * 1 + (((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) % 1)))];
            }
          }
        }
      }
      for (l_step_r_1 = 1; l_step_r_1 < ((32 / 4) / (4 / 4)); ++l_step_r_1) {
        for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
          for (size_t p_step_l_2 = 0; p_step_l_2 < ((1024 / 1) / (64)); ++p_step_l_2) {
            size_t p_step_r_1 = 0;
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (64); ++p_iteration_l_2) {
                for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
                  res_p[(0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)] += int_res[(((l_step_r_1 * (4 / 4) + (((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 4 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) / 4) * 4 + i_wg_r_1 * 4 + ((((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 4 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) % 4))) * 4096 * 1 + (((l_step_l_2 * (1024 / 1) + (((p_step_l_2 * (64) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_2 * 1 + ((((p_step_l_2 * (64) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) % 1))) * 1 + (((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) % 1)))];
                }
              }
            }
          }
        }
      }
      {
        for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
          for (size_t p_step_l_2 = 0; p_step_l_2 < ((1024 / 1) / (64)); ++p_step_l_2) {
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (64); ++p_iteration_l_2) {
                l_reduction_mem[(i_wi_r_1)][((p_step_l_2 * (64) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))][((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))] = res_p[(0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)];
              }
            }
          }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        size_t stride = 4 / 2;
        for (; stride > 0; stride /= 2) {
          if (i_wi_r_1 < stride) {
            for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
              for (size_t p_step_l_2 = 0; p_step_l_2 < ((1024 / 1) / (64)); ++p_step_l_2) {
                for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                  for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (64); ++p_iteration_l_2) {
                    l_reduction_mem[(i_wi_r_1)][((p_step_l_2 * (64) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))][((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))] += l_reduction_mem[(i_wi_r_1 + stride)][((p_step_l_2 * (64) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))][((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))];
                  }
                }
              }
            }
          }
          barrier(CLK_LOCAL_MEM_FENCE);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
      }
      {
        for (size_t step = 0; step < (((((1 / 1) / (1)) * (1) + ((1 * (1 / 1)) % (1 * (1)) / 1)) * 1 + ((1 * (1 / 1)) % (1 * (1)) % 1)) * ((((1024 / 1) / (64)) * (64) + ((1 * (1024 / 1)) % (1 * (64)) / 1)) * 1 + ((1 * (1024 / 1)) % (1 * (64)) % 1))) / (1 * 1 * 4); ++step) {
          const size_t flat_index = get_local_id(0) + step * (1 * 1 * 4);
          const size_t l_index_l_1 = flat_index / (((((1024 / 1) / (64)) * (64) + ((1 * (1024 / 1)) % (1 * (64)) / 1)) * 1 + ((1 * (1024 / 1)) % (1 * (64)) % 1)));
          const size_t l_index_l_2 = flat_index % ((((1024 / 1) / (64)) * (64) + ((1 * (1024 / 1)) % (1 * (64)) / 1)) * 1 + ((1 * (1024 / 1)) % (1 * (64)) % 1));
          c[(((l_step_l_1 * (1 / 1) + (l_index_l_1) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((l_index_l_1) % 1))) * 4096 + (((l_step_l_2 * (1024 / 1) + (l_index_l_2) / 1) * (1 * 1) + i_wg_l_2 * 1 + ((l_index_l_2) % 1)))] =
            l_reduction_mem[(0)][l_index_l_2][l_index_l_1];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
      }
    }
  }
}
