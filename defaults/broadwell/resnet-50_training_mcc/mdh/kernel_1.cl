__kernel void mcc_nhwc_krsc_npqk_stride_2_asymmetrical_static_1(
        __global float const * const __restrict__ images_glb,
        __global float const * const __restrict__ filter_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ out_glb) {
    __private float out_prv[(1 * 1) *
                                    (1 * 7) *
                                    (2 * 1) *
                                    (1 * 1)];
    __private float filter_prv[(1) *
                                       (1) *
                                       (1) *
                                       (1)];
    const size_t wg_1 = get_group_id(0) / (8 * 14 * 2) % (16); {
    const size_t wg_2 = get_group_id(0) / (8 * 14) % (2); {
    const size_t wg_3 = get_group_id(0) / (8) % (14); {
    const size_t wg_4 = get_group_id(0) % (8); {
    {
    {
    {
    {
    {
    {
    const size_t wi_4 = get_local_id(0) % (8); {
    {
    {
    {
    {
    for (size_t glb_2 = 0; glb_2 < 8; ++glb_2) {
    for (size_t glb_3 = 0; glb_3 < 4; ++glb_3) {
    {
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
    {
    {
    for (size_t prv_2 = 0; prv_2 < 7; ++prv_2) {
    {
    {
        out_prv[(0 * 1 + 0) * (1 * 7) * (2 * 1) * (1 * 1) + (0 * 7 + prv_2) * (2 * 1) * (1 * 1) + (lcl_3 * 1 + 0) * (1 * 1) + (0 * 1 + 0) ]
            = 0.0f;
    }}}}}}}}
    }
    for (size_t glb_5 = 0; glb_5 < 3; ++glb_5) {
    for (size_t glb_6 = 0; glb_6 < 7; ++glb_6) {
    for (size_t glb_7 = 0; glb_7 < 7; ++glb_7) {
        {
        {
        {
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
        {
            {
               
               
               
               
                    filter_prv[(0) * (1) * (1) * (1) + (0) * (1) * (1) + (0) * (1) + (0) ]
                                      =
                    filter_glb[(wg_4 * 1 * 8 * 1 * 1 + 0 * 8 * 1 * 1 + wi_4 * 1 * 1 + 0 * 1 + 0) * (7) * (7) * (3) + (0 * 7 * 1 * 1 * 1 + glb_6 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (7) * (3) + (0 * 7 * 1 * 1 * 1 + glb_7 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (3) + (0 * 3 * 1 * 1 * 1 + glb_5 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            {
            {
            for (size_t prv_2 = 0; prv_2 < 7; ++prv_2) {
            {
            {
            out_prv[(0 * 1 + 0) * (1 * 7) * (2 * 1) * (1 * 1) + (0 * 7 + prv_2) * (2 * 1) * (1 * 1) + (lcl_3 * 1 + 0) * (1 * 1) + (0 * 1 + 0) ]
                +=
            images_glb[(wg_1 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * ((2 * 112 + 7 - 1)) * ((2 * 112 + 7 - 1)) * (3) + ((wg_2 * 8 * 1 * 1 * 7 + glb_2 * 1 * 1 * 7 + 0 * 1 * 7 + 0 * 7 + prv_2) * 2 + (0 * 7 * 1 * 1 * 1 + glb_6 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * ((2 * 112 + 7 - 1)) * (3) + ((wg_3 * 4 * 1 * 2 * 1 + glb_3 * 1 * 2 * 1 + 0 * 2 * 1 + lcl_3 * 1 + 0) * 2 + (0 * 7 * 1 * 1 * 1 + glb_7 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * (3) + (0 * 3 * 1 * 1 * 1 + glb_5 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
            *
            filter_prv[(0) * (1) * (1) * (1) + (0) * (1) * (1) + (0) * (1) + (0) ]
            ;
            }}}}}}}
        }}}}}}}
    }}}
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
    {
    {
    for (size_t prv_2 = 0; prv_2 < 7; ++prv_2) {
    {
    {
        out_glb[(wg_1 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (112) * (112) * (64) + (wg_2 * 8 * 1 * 1 * 7 + glb_2 * 1 * 1 * 7 + 0 * 1 * 7 + 0 * 7 + prv_2) * (112) * (64) + (wg_3 * 4 * 1 * 2 * 1 + glb_3 * 1 * 2 * 1 + 0 * 2 * 1 + lcl_3 * 1 + 0) * (64) + (wg_4 * 1 * 8 * 1 * 1 + 0 * 8 * 1 * 1 + wi_4 * 1 * 1 + 0 * 1 + 0) ]
                                                 =
        out_prv[(0 * 1 + 0) * (1 * 7) * (2 * 1) * (1 * 1) + (0 * 7 + prv_2) * (2 * 1) * (1 * 1) + (lcl_3 * 1 + 0) * (1 * 1) + (0 * 1 + 0) ]
                             ;
    }}}}}}}}
    }
    }}}}
    }}}}}}}
    }}}}}}}
}
