import os

import tabulate as tabulate

devices = ['cpu', 'gpu']
nets = ['ResNet-50', 'VGG-16', 'MobileNet']
phases = ['Training', 'Inference']
routines = ['MCC', 'MatMul']


for device in devices:
    if 'ENABLE_' + device.upper() not in os.environ or os.environ['ENABLE_' + device.upper()] != '1':
        continue

    if device == 'cpu':
        competitor_folders = ['tvm/opencl', 'onednn', 'onemkl', 'tvm/llvm']
        competitor_names = {competitor_folders[0]: 'TVM+Ansor',
                            competitor_folders[1]: 'Intel oneDNN',
                            competitor_folders[2]: 'Intel oneMKL',
                            competitor_folders[3]: 'TVM+Ansor (LLVM)'}
    else:
        competitor_folders = ['tvm', 'cudnn', 'cublas']
        competitor_names = {competitor_folders[0]: 'TVM+Ansor',
                            competitor_folders[1]: 'NVIDIA cuDNN',
                            competitor_folders[2]: 'NVIDIA cuBLAS'}

    table = []

    # header
    table.append([device.upper()])
    table.append([None])
    table.append([None])
    for net in nets:
        for phase in phases:
            for routine in routines:
                if net == 'MobileNet' and routine == 'MatMul':
                    continue
                table[-3].append(net.lower())
                table[-2].append(phase.lower())
                table[-1].append(routine.lower())

    # get MDH runtimes
    mdh_runtimes = dict()
    for net in nets:
        for phase in phases:
            for routine in routines:
                if net == 'MobileNet' and routine == 'MatMul':
                    continue
                input_size_id = '_'.join([net.lower(), phase.lower(), routine.lower()])
                runtime_file_name = 'results/{}/{}/mdh/runtimes_min'.format(device, input_size_id)
                if os.path.isfile(runtime_file_name):
                    with open(runtime_file_name, 'r') as runtime_file:
                        try:
                            mdh_runtimes[input_size_id] = float(runtime_file.read())
                        except:
                            pass

    # competitor rows
    for competitor_folder in competitor_folders:
        competitor_name = competitor_names[competitor_folder]
        table.append([competitor_name])
        for net in nets:
            for phase in phases:
                for routine in routines:
                    if net == 'MobileNet' and routine == 'MatMul':
                        continue
                    input_size_id = '_'.join([net.lower(), phase.lower(), routine.lower()])
                    mdh_runtime = mdh_runtimes[input_size_id] if input_size_id in mdh_runtimes.keys() else None
                    competitor_runtime = None
                    runtime_file_name = 'results/{}/{}/{}/runtimes_min'.format(device, input_size_id, competitor_folder)
                    if os.path.isfile(runtime_file_name):
                        with open(runtime_file_name, 'r') as runtime_file:
                            try:
                                competitor_runtime = float(runtime_file.read())
                                if device == 'gpu' and competitor_folder == 'tvm':
                                    competitor_runtime *= 1000000  # convert ms to ns
                            except:
                                pass
                    if mdh_runtime is not None and competitor_runtime is not None:
                        table[-1].append(round(competitor_runtime / mdh_runtime, 2))
                    else:
                        table[-1].append(None)

    print(tabulate.tabulate(table))